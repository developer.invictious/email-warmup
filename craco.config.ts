const path = require(`path`);

const config = {
  webpack: {
    alias: {
      "@sms": path.resolve(__dirname, "src/")
    },
    configure: {
      stats: "errors-only"
    }
  }
};

export default config;
