import datepicker from "./datepicker.svg";
import filterList from "./filterList.svg";
import incomingMail from "./incomingMail.svg";
import mailSent from "./mailSent.svg";
import replyMail from "./replyMail.svg";
import spamMail from "./spamMail.svg";
export const images = {
  datepicker,
  incomingMail,
  mailSent,
  replyMail,
  filterList,
  spamMail
};
