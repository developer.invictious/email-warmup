export const DigiHubColor = {
  smoke: "#F7F8FA",
  blue: "#024B8D",
  blue_2: "#4F539E",
  whiteGray: "#D8D6DE",
  lightGray: "#585858",
  lightSmoke: "#F4F3F3",
  lightBlue: "#1DABF2",
  ghostWhite: "#FAF8FC",
  lightBlack: "#4D4D4D",
  black: "#262626",
  success: "#308D44"
};
