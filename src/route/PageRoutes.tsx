const pageRoutes = {
  login: "/",
  register: "/register",
  forgetPassword: "forgetPassword",

  dashboard: "/dashboard",

  user: "/user-management",
  add_user: "/user-management/user-form",
  edit_user: "/user-management/edit/:userId",
  view_user: "/user-management/view/:userId",

  plans: "/payment",

  email_template: "/email-template",
  add_email_template: "/email-template/template-form",
  edit_email_template: "/email-template/edit/:templateId",
  view_email_template: "/email-template/view/:templateId",

  google_token: "/google/token",
  yahoo_token: "/yahoo/token",

  report: "/report/:statsId",

  billing: "/billing"
};

export default pageRoutes;
