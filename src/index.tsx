import { createRoot } from "react-dom/client";

import App from "@sms/pages/App";

const container = document.getElementById("digihub-app");
// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const root = createRoot(container!);

root.render(<App />);
// reportWebVitals(console.log);
