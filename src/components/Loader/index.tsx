import CircularProgress from "@mui/material/CircularProgress";
import logo from "@sms/assets/images/logo.png";

const Loader = () => {
  return (
    <div
      style={{
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <CircularProgress />
      {/* <img src={logo} alt="Nepal government logo" /> */}
    </div>
  );
};

export default Loader;
