import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import Pagination from "@mui/material/Pagination";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableHead from "@mui/material/TableHead";
import { Fragment, ReactNode, useMemo } from "react";
import {
  Column,
  HeaderGroup,
  Row,
  useExpanded,
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable
} from "react-table";

import { Card, Grid, Typography } from "@mui/material";
import {
  GoToPageDropDown,
  StyledTableCell,
  StyledTableContainer,
  StyledTableFooter,
  StyledTableRow,
  StyledTableSearchWrapper
} from "./Datatable.styles";
import { GlobalFilter } from "./GlobalFilter";

interface DatatableProps {
  showTableHeader?: boolean;
  showLimit?: boolean;
  columns: Column<any>[];
  data: Record<string, any>[];
  loading?: boolean;
  HeaderChildren?: ReactNode;
  searchable?: boolean;
  renderExpanded?: (row: Row<any>) => ReactNode;
  pageSize?: number;
  onPageSizeChange?: (page: number) => void;
  isBackendPagination?: boolean;
  paginateParams?: {
    gotoPage: (page: number) => void;
    pageIndex: number;
    canNextPage: boolean;
    canPreviousPage: boolean;
  };
}

const Datatable = ({
  showTableHeader,
  showLimit,
  columns,
  data,
  loading,
  HeaderChildren,
  searchable,
  renderExpanded,
  pageSize: shownPageSize,
  onPageSizeChange,
  isBackendPagination,
  paginateParams
}: DatatableProps) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    pageOptions,
    pageCount,
    canPreviousPage,
    canNextPage,
    gotoPage,
    setPageSize,
    setGlobalFilter,

    state: { globalFilter, pageIndex, pageSize },
    allColumns
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
      autoResetPage: false,
      onPageSizeChange
    },
    useGlobalFilter,
    useSortBy,
    useExpanded,
    usePagination
  );

  const paginationSettings = useMemo(() => {
    const isBackendPaginated = isBackendPagination && paginateParams;

    return {
      pageSize: isBackendPaginated ? shownPageSize : pageSize,
      setPageSize: isBackendPaginated ? onPageSizeChange : setPageSize,
      gotoPage: isBackendPaginated ? paginateParams.gotoPage : gotoPage,
      pageIndex: isBackendPaginated ? paginateParams.pageIndex : pageIndex,
      canPreviousPage: isBackendPaginated
        ? paginateParams.canPreviousPage
        : canPreviousPage,
      canNextPage: isBackendPaginated ? paginateParams.canNextPage : canNextPage
    };
  }, [
    isBackendPagination,
    paginateParams,
    pageIndex,
    canNextPage,
    canPreviousPage,
    onPageSizeChange,
    setPageSize,
    gotoPage
  ]);

  return (
    <Card
      elevation={0}
      sx={{ p: 3.5, borderRadius: "8px", bgcolor: "transparent" }}
    >
      <GoToPageDropDown>
        {showLimit ? (
          <div>
            <Typography>
              Show
              <select
                style={{ background: "white" }}
                onChange={(e) => {
                  onPageSizeChange?.(Number(e.target.value));
                }}
              >
                {[5, 10, 20, 25, 50].map((itemSize: number) => (
                  <option key={itemSize} value={itemSize}>
                    {itemSize}
                  </option>
                ))}
              </select>
            </Typography>
          </div>
        ) : (
          <div>
            <Typography>
              Show
              <select
                value={paginationSettings.pageSize}
                onChange={(e) => {
                  paginationSettings.setPageSize?.(Number(e.target.value));
                }}
              >
                {[5, 10, 20, 25, 50].map((itemSize: number) => (
                  <option key={itemSize} value={itemSize}>
                    {itemSize}
                  </option>
                ))}
              </select>
            </Typography>
          </div>
        )}

        <Box display={"flex"}>
          {searchable && (
            <StyledTableSearchWrapper>
              <GlobalFilter
                globalFilter={globalFilter}
                setGlobalFilter={setGlobalFilter}
              />
            </StyledTableSearchWrapper>
          )}
          {HeaderChildren}
        </Box>
      </GoToPageDropDown>

      <StyledTableContainer component={Paper}>
        {loading && (
          <Box sx={{ width: "100%" }}>
            <LinearProgress color="info" />
          </Box>
        )}

        <Table
          aria-label="Table"
          {...getTableProps()}
          sx={{
            "&.MuiTableContainer-root": {
              boxShadow: "none"
            },

            "& .MuiTable-root": {
              borderCollapse: "separate",
              borderSpacing: "0 10px",
              border: "transparent"
            },
            "& .MuiTable-root th, .MuiTable-root td": {
              borderTop: "1px solid black",
              borderBottom: " 1px solid black"
            }
          }}
        >
          {showTableHeader ? (
            <>
              {" "}
              <TableHead
                sx={{
                  backgroundColor: "#006ECB",
                  color: "white",
                  border: `1px solid #F3F2F7`,
                  borderTopLeftRadius: "8px",
                  borderTopRightRadius: "8px"
                }}
              >
                {headerGroups.map((headerGroup: HeaderGroup, idx: number) => (
                  <StyledTableRow
                    {...headerGroup.getHeaderGroupProps()}
                    key={idx}
                  >
                    {headerGroup.headers.map((column) => {
                      return (
                        <StyledTableCell
                          component={"th"}
                          {...column.getHeaderProps([
                            column.getSortByToggleProps(),
                            {
                              style: {
                                width: column?.width && column.width
                              }
                            }
                          ])}
                          key={column.id}
                        >
                          {column.render("Header")}
                          <Fragment>
                            {column.isSorted &&
                              (column.isSortedDesc ? (
                                <KeyboardArrowDownIcon
                                  sx={{
                                    position: "absolute",
                                    fontSize: 20,
                                    top: 18,
                                    right: 15
                                  }}
                                />
                              ) : (
                                <KeyboardArrowUpIcon
                                  sx={{
                                    position: "absolute",
                                    fontSize: 20,
                                    top: 18,
                                    right: 15
                                  }}
                                />
                              ))}
                          </Fragment>
                        </StyledTableCell>
                      );
                    })}
                  </StyledTableRow>
                ))}
              </TableHead>
            </>
          ) : (
            <></>
          )}

          <TableBody {...getTableBodyProps()}>
            {page.length ? (
              page.map((row, index) => {
                prepareRow(row);
                return (
                  <>
                    <StyledTableRow {...row.getRowProps()} key={index}>
                      {row.cells.map((cell, i) => {
                        return (
                          <StyledTableCell {...cell.getCellProps()} key={i}>
                            {/* <StyledTableRow key={1}> */}
                            {cell.render("Cell")}
                            {/* </StyledTableRow> */}
                          </StyledTableCell>
                        );
                      })}
                    </StyledTableRow>

                    {/* Show this view on expand */}
                    {row.isExpanded && (
                      <StyledTableRow>
                        {/* <StyledStyledTableRow> */}
                        <StyledTableCell colSpan={columns.length} p={0}>
                          {renderExpanded?.(row)}
                        </StyledTableCell>
                        {/* </StyledStyledTableRow> */}
                      </StyledTableRow>
                    )}
                  </>
                );
              })
            ) : (
              <StyledTableRow>
                <StyledTableCell align="center" colSpan={allColumns.length}>
                  No records found
                </StyledTableCell>
              </StyledTableRow>
            )}
          </TableBody>
        </Table>
      </StyledTableContainer>

      <StyledTableFooter>
        {/* <Typography component={"span"}>
          Showing
          <strong>
            {page.length} of {data.length} entities
          </strong>
        </Typography> */}
      </StyledTableFooter>
      {
        <Grid container sx={{ display: "flex", justifyContent: "end" }}>
          {showLimit ? (
            <Pagination
              count={shownPageSize}
              page={paginationSettings.pageIndex + 1}
              onChange={(_, newPage: number) => {
                paginationSettings.gotoPage(newPage - 1);
              }}
              hidePrevButton={false}
              hideNextButton={false}
              //   hidePrevButton={!paginationSettings.canNextPage}
              //   hideNextButton={!paginationSettings.canPreviousPage}
              showFirstButton={paginationSettings.canPreviousPage}
              showLastButton={paginationSettings.canNextPage}
              color="primary"
            />
          ) : (
            <>
              {" "}
              {pageCount > 1 && (
                <Pagination
                  count={pageOptions.length}
                  page={paginationSettings.pageIndex + 1}
                  onChange={(_, newPage: number) => {
                    paginationSettings.gotoPage(newPage - 1);
                  }}
                  hidePrevButton={false}
                  hideNextButton={false}
                  //   hidePrevButton={!paginationSettings.canNextPage}
                  //   hideNextButton={!paginationSettings.canPreviousPage}
                  showFirstButton={paginationSettings.canPreviousPage}
                  showLastButton={paginationSettings.canNextPage}
                  color="primary"
                />
              )}
            </>
          )}
        </Grid>
      }
    </Card>
  );
};

export default Datatable;
