import { TableRow } from "@mui/material";
import Paper from "@mui/material/Paper";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer, {
  TableContainerProps
} from "@mui/material/TableContainer";
import styled from "@mui/system/styled";
import { DigiHubColor } from "@sms/shared/colors";

const StyledTableCell = styled(TableCell)<{ p?: number }>(({ theme, p }) => ({
  [`&.${tableCellClasses.head}`]: {
    padding: "20px",
    backgroundColor: "#4F539E",
    color: "#FCFCFC",
    position: "relative",
    lineHeight: "1rem"
  },
  [`&.${tableCellClasses.body}`]: {
    border: "none",
    padding: p ?? "16px 20px",
    fontSize: 14,
    color: "#585858",
    columnSpan: "10"
  }
  //   "&:hover": {
  //     boxShadow: "0 0 25px #ddd"
  //   }
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(even)": {
    backgroundColor: "#fff",
    borderBottom: `20px solid ${DigiHubColor.ghostWhite}`
  },
  "&:nth-of-type(odd)": {
    borderBottom: `20px solid ${DigiHubColor.ghostWhite}`
  },
  "&:last-child td, &:last-child th": {
    border: 0
  }
}));

const StyledTableContainer = styled(TableContainer)<
  TableContainerProps<typeof Paper, { component: typeof Paper }>
>({
  "&.MuiPaper-root": {
    boxShadow: "none",
    border: "none",
    borderRadius: "11px",
    padding: "0px !important"
  }
});

const GoToPageDropDown = styled("div")({
  display: "flex",
  justifyContent: "space-between",
  color: "#6E6B7B",
  fontSize: 14,
  marginBottom: 18,
  select: {
    border: `1px solid #E9ECEF`,
    background: "transparent",
    padding: "10px 15px",
    borderRadius: 6,
    color: "#6E6B7B",
    marginLeft: 10
  }
});

const StyledTableFooter = styled("div")({
  marginTop: 20,
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  span: {
    color: "#5E5873",
    fontSize: 14,
    strong: {
      marginLeft: 5
    }
  }
});

const StyledTableSearchWrapper = styled("div")(({ theme }) => ({
  position: "relative",
  svg: {
    position: "absolute",
    right: 8,
    top: 8,
    fontSize: 20
  },
  input: {
    border: `1px solid #D8D6DE`,
    padding: 10,
    borderRadius: 4,
    paddingRight: 32,
    marginLeft: 10,
    "&:hover": {
      borderColor: theme.palette.primary.main
    },
    "&:focus-visible": {
      borderColor: theme.palette.primary.main,
      outline: "none"
    }
  }
}));

export {
  StyledTableCell,
  StyledTableRow,
  StyledTableContainer,
  GoToPageDropDown,
  StyledTableFooter,
  StyledTableSearchWrapper
};
