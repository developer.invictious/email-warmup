import React from "react";
import { Toaster } from "react-hot-toast";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { BrowserRouter } from "react-router-dom";

import { LocalizationProvider } from "@mui/lab";
import ThemeProvider from "@mui/system/ThemeProvider";
import { AdapterLuxon } from "@mui/x-date-pickers/AdapterLuxon";
import { GlobalStyles } from "@sms/theme/Global";

import { theme } from "../theme/Theme";
import Loader from "./Loader";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
      refetchOnWindowFocus: false,
      refetchOnReconnect: true,
      staleTime: 30 * 1000
      //   staleTime: 1000 * 60 * 5 // 5 minutes
    }
  }
});
interface IProvider {
  children: React.ReactNode;
}
const Provider: React.FC<IProvider> = ({ children }) => {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <React.Suspense fallback={<Loader />}>
          <QueryClientProvider client={queryClient}>
            <ThemeProvider theme={theme}>
              <Toaster position="bottom-right" />

              <LocalizationProvider dateAdapter={AdapterLuxon}>
                {children}
              </LocalizationProvider>
              <ReactQueryDevtools initialIsOpen={false} />
            </ThemeProvider>

            {GlobalStyles}
          </QueryClientProvider>
        </React.Suspense>
      </BrowserRouter>
    </React.StrictMode>
  );
};

export default Provider;
