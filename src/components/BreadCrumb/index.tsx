import HomeIcon from "@mui/icons-material/Home";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Card from "@mui/material/Card";
import Link from "@mui/material/Link";
import Typography from "@mui/material/Typography";
import { useLocation, useNavigate } from "react-router-dom";

const BreadcrumbsComponent = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const pathnames = pathname.split("/").filter((x: string) => x);

  return (
    <Card
      sx={{
        backgroundColor: "#fff",
        boxShadow: "none",
        px: 2,
        py: 1.5,
        my: 2,
        borderRadius: "8px"
      }}
    >
      <Breadcrumbs
        aria-label="breadcrumb"
        separator={<NavigateNextIcon fontSize="small" />}
      >
        {pathnames.length > 0 ? (
          <Link
            sx={{
              textDecoration: "none",
              color: "#6E6B7B",
              textTransform: "capitalize",
              cursor: "pointer"
            }}
            onClick={() => navigate("/")}
          >
            <HomeIcon sx={{ fontSize: 22, position: "relative", top: 2 }} />
          </Link>
        ) : (
          <HomeIcon
            sx={{
              fontSize: 22,
              position: "relative",
              top: 2,
              color: "#01579B"
            }}
          />
        )}
        {pathnames.map((name: string, index: number) => {
          const routeTo = `/${pathnames.slice(0, index + 1).join("/")}`;
          const isLast = index === pathnames.length - 1;
          return isLast ? (
            <Typography
              sx={{ color: "#01579B", textTransform: "capitalize" }}
              key={name}
            >
              {name}
            </Typography>
          ) : (
            <Link
              sx={{
                textDecoration: "none",
                color: "#6E6B7B",
                textTransform: "capitalize"
              }}
              key={name}
              onClick={() => navigate(routeTo)}
            >
              {name}
            </Link>
          );
        })}
      </Breadcrumbs>
    </Card>
  );
};

export default BreadcrumbsComponent;
