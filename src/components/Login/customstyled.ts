import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import FormControlLabel from "@mui/material/FormControlLabel";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";

const CustomGrid = styled(Grid)({
  width: "100%",
  position: "relative",
  "& .MuiGrid-container": {
    position: "absolute",
    top: "50%",
    width: "100%",
    left: "15%",
    transform: "translate(0,-50%)"
  }
});
const CustomLink = styled(Link)({
  position: "absolute",
  left: "68px",
  top: "52px",
  width: "185px"
});
const HeadingTypography = styled(Typography)({
  fontWeight: "600",
  fontSize: "28px",
  lineHeight: "36px",
  color: "#5E5873"
});
const CustomTypography = styled(Typography)({
  color: "#6E6B7b",
  fontSize: "16px"
});
const CustomTextField = styled(TextField)({
  width: "100%"
});
const CustomButton = styled(Button)({
  width: "100%",
  margin: "5px 0",
  backgroundColor: "#024B8D",
  color: "#ffffff"
});
const CustomFormLabel = styled(FormControlLabel)({
  "& span.MuiTypography-root": {
    fontWeight: "400",
    fontSize: "14px",
    color: "#6E6B7B"
  }
});

export {
  CustomButton,
  CustomGrid,
  CustomFormLabel,
  CustomTypography,
  CustomTextField,
  HeadingTypography,
  CustomLink
};
