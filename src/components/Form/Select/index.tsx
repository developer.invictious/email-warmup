import Chip from "@mui/material/Chip";
import FormControl from "@mui/material/FormControl";
import FormHelperText from "@mui/material/FormHelperText";
import InputLabel, { InputLabelProps } from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { Control, Controller } from "react-hook-form";
// import { theme } from "@sms/theme";
import CancelIcon from "@mui/icons-material/Cancel";
import SearchIcon from "@mui/icons-material/Search";
import {
  Checkbox,
  FormControlLabel,
  Grid,
  InputAdornment,
  ListSubheader,
  TextField
} from "@mui/material";
import { SelectInputProps } from "@mui/material/Select/SelectInput";
import { useState } from "react";

interface Option {
  value: string | number;
  label: string;
}

interface SelectProps<T = any> extends Partial<SelectInputProps> {
  options: Option[];
  label?: string;
  name: string;
  error?: boolean;
  helperText?: string;
  control: Control<T>;
  fullWidth?: boolean;
  multiple?: boolean;
  disabled?: boolean;
  required?: boolean;
  searchable?: boolean;
  sx?: object;
  inputLabelProps?: InputLabelProps["sx"];
  size?: "small" | "medium";
}

const SelectComponent = ({
  searchable,
  name,
  control,
  label,
  options,
  fullWidth,
  multiple,
  required,
  disabled,
  sx,
  size,
  inputLabelProps,
  ...extraProps
}: SelectProps) => {
  const [searchOption, setSearchOption] = useState(options);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <FormControl fullWidth>
            <InputLabel
              sx={inputLabelProps}
              required={required}
              id={`simple-${name}-label`}
            >
              {label}
            </InputLabel>
            <Select
              disabled={disabled}
              name={name}
              multiple={multiple}
              sx={sx}
              size={size}
              error={!!error}
              renderValue={(selected) => {
                const items = options.filter((item: Option) => {
                  return multiple
                    ? selected.includes(item.value)
                    : selected === item.value;
                });

                return (
                  <>
                    {items?.map(({ value, label }: any) => {
                      return (
                        <Chip
                          sx={{ height: 20 }}
                          key={value}
                          label={label}
                          onDelete={() => {
                            multiple
                              ? onChange(
                                  selected.filter(
                                    (s: string | number) => s !== value
                                  )
                                )
                              : onChange();
                          }}
                          deleteIcon={
                            <CancelIcon
                              onMouseDown={(event: any) =>
                                event.stopPropagation()
                              }
                            />
                          }
                        />
                      );
                    })}
                  </>
                );
              }}
              labelId={`simple-${name}-label`}
              id={name}
              onChange={(e) => {
                onChange();
              }}
              value={value ?? ""}
              label={label}
              variant="outlined"
              fullWidth={fullWidth}
              {...extraProps}
            >
              {searchable ? (
                <>
                  <ListSubheader>
                    <TextField
                      size="small"
                      autoFocus
                      placeholder="Type to search..."
                      fullWidth
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <SearchIcon />
                          </InputAdornment>
                        )
                      }}
                      onChange={(e: any) => {
                        if (e.target.value == "") {
                          setSearchOption(options);
                        } else {
                          const data: any = options.filter(
                            (val) =>
                              val.label
                                .toLowerCase()
                                .indexOf(e.target.value.toLowerCase()) > -1
                          );
                          setSearchOption(data);
                        }
                      }}
                      onKeyDown={(e: any) => {
                        if (e.key !== "Escape") {
                          e.stopPropagation();
                        }
                      }}
                    />
                  </ListSubheader>
                  {searchOption?.length ? (
                    searchOption.map((option: Option) => (
                      <Grid container key={option.value}>
                        <Grid
                          item
                          xs={12}
                          sx={{
                            display: "flex",
                            flexDirection: "column",
                            px: "20px",
                            py: "5px",
                            "&:hover": {
                              backgroundColor: "#E6E7F1"
                            }
                          }}
                        >
                          <FormControlLabel
                            label={option.label}
                            name={name}
                            control={
                              <>
                                <Checkbox
                                  sx={{
                                    "&.MuiCheckbox-root": {
                                      position: "absolute",
                                      right: 0,
                                      pr: "20px"
                                    }
                                  }}
                                  checked={value.includes(option.value)}
                                  onChange={() => {
                                    if (multiple) {
                                      if (value.includes(option.value)) {
                                        onChange(
                                          value.filter(
                                            (v: string | number) =>
                                              v !== option.value
                                          )
                                        );
                                      } else {
                                        onChange([...value, option.value]);
                                      }
                                    } else {
                                      if (value === option.value) onChange();
                                      else onChange(option.value);
                                    }
                                  }}
                                />
                              </>
                            }
                          />
                        </Grid>
                      </Grid>
                    ))
                  ) : (
                    <MenuItem value="">No Items</MenuItem>
                  )}
                </>
              ) : (
                <>
                  {" "}
                  {options?.length ? (
                    options.map((option: Option) => (
                      <Grid container key={option.value}>
                        <Grid
                          item
                          xs={12}
                          sx={{
                            display: "flex",
                            flexDirection: "column",
                            px: "20px",
                            py: "5px",
                            "&:hover": {
                              backgroundColor: "#E6E7F1"
                            }
                          }}
                        >
                          <FormControlLabel
                            label={option.label}
                            name={name}
                            control={
                              <Checkbox
                                sx={{
                                  "&.MuiCheckbox-root": {
                                    position: "absolute",
                                    right: 0,
                                    pr: "20px"
                                  }
                                }}
                                checked={value.includes(option.value)}
                                onChange={() => {
                                  if (multiple) {
                                    if (value.includes(option.value)) {
                                      onChange(
                                        value.filter(
                                          (v: string | number) =>
                                            v !== option.value
                                        )
                                      );
                                    } else {
                                      onChange([...value, option.value]);
                                    }
                                  } else {
                                    if (value === option.value) onChange();
                                    else onChange(option.value);
                                  }
                                }}
                              />
                            }
                          />
                        </Grid>
                      </Grid>
                    ))
                  ) : (
                    <MenuItem value="">No Items</MenuItem>
                  )}
                </>
              )}
            </Select>
            <FormHelperText sx={{ color: "#d32f2f", marginLeft: 0 }}>
              {error?.message || ""}
            </FormHelperText>
          </FormControl>
        );
      }}
    />
  );
};

export default SelectComponent;
