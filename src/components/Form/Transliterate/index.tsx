import { Box } from "@mui/material";
import { DigiHubColor } from "@sms/shared/colors";
import { ReactNode, Ref } from "react";
import { ReactTransliterate } from "react-transliterate";

type RenderComponentProps = {
  "data-testid": string;
  onBlur: () => void;
  onChange: () => void;
  onKeyDown: () => void;
  ref: Ref<HTMLInputElement | HTMLTextAreaElement>;
  value: string;
};

type Props = {
  text: string;
  onChange: (text: string) => void;
  renderComponent: (props: RenderComponentProps) => ReactNode;
  enabled?: boolean;
};

function Transliterate({
  text,
  onChange,
  renderComponent,
  enabled = true
}: Props) {
  return (
    <Box
      sx={{
        "& ul": {
          top: "45px !important",
          borderRadius: 1.5,
          "& li": { borderRadius: 1.5 }
        }
      }}
    >
      <ReactTransliterate
        enabled={enabled}
        renderComponent={renderComponent}
        value={text}
        onChangeText={onChange}
        lang="ne"
        activeItemStyles={{ backgroundColor: DigiHubColor.blue }}
      />
    </Box>
  );
}

export default Transliterate;
