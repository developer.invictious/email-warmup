import React from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

interface IEditor {
  data?: string;
  onChange?: (data: string) => void;
  onBlur?: (data: string | undefined) => void;
  onInit?: (editor: ClassicEditor) => void;
}

const Editor = ({ data, onInit, onChange, onBlur }: IEditor) => {
  return (
    <CKEditor
      editor={ClassicEditor}
      data={data}
      config={{
        removePlugins: ["EasyImage", "ImageUpload", "MediaEmbed"]
      }}
      onReady={(editor) => {
        // You can store the "editor" and use when it is needed.
        // console.log("Editor is ready to use!", editor);
        editor?.editing.view.change((writer) => {
          writer.setStyle(
            "height",
            "200px",
            editor.editing.view.document.getRoot()
          );
        });
        onInit && onInit(editor);
      }}
      onChange={(event, editor) => {
        const data = editor.getData();
        onChange && onChange(data);
      }}
      onBlur={(event, editor) => {
        onBlur && onBlur(data);
      }}
    />
  );
};

export default Editor;
