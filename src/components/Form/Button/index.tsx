import LoadingButton from "@mui/lab/LoadingButton";
import { Button, ButtonGroup, Grid } from "@mui/material";

interface IFormButtons {
  onBackButtonClick: () => void;
  isLoading: boolean;
  submitButtonText: string;
  backButtonText: string;
}

const FormButtons = ({
  onBackButtonClick,
  isLoading,
  submitButtonText,
  backButtonText
}: IFormButtons) => {
  return (
    <Grid container sx={{ pb: 4 }} columnSpacing={3}>
      <ButtonGroup
        aria-label="outlined button group"
        sx={{
          pl: 3,
          pt: 5,
          width: "100%",
          justifyContent: "space-between"
        }}
      >
        <Button
          variant="outlined"
          onClick={onBackButtonClick}
          sx={{
            width: 182,
            height: 46,
            borderRightColor: "currentColor !important",
            borderTopRightRadius: "4px !important",
            borderBottomRightRadius: "4px !important"
          }}
        >
          {backButtonText}
        </Button>
        <LoadingButton
          type="submit"
          loading={isLoading}
          variant="contained"
          sx={{
            width: 182,
            height: 46,
            borderRadius: "4px !important"
          }}
        >
          {submitButtonText}
        </LoadingButton>
      </ButtonGroup>
    </Grid>
  );
};

export default FormButtons;
