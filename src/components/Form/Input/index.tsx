import { TextFieldProps } from "@mui/material/TextField";
import React from "react";
import { Control, Controller } from "react-hook-form";
import { useTranslation } from "react-i18next";
// import { theme } from "@sms/theme";
import { Input } from "./Input.style";

interface TextInputProps<T = any> {
  name: string;
  label?: string;
  control: Control<T>;
  borderColor?: string;
  color?: string;
  type: React.InputHTMLAttributes<unknown>["type"];
  testId?: string;
}

const TextInput: React.FC<TextInputProps & Omit<TextFieldProps, "color">> = ({
  name,
  control,
  label,
  type,
  borderColor,
  color,
  testId,
  ...extraProps
}) => {
  const { t } = useTranslation();

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <Input
            error={!!error}
            onChange={onChange}
            value={value}
            data-testid={testId}
            InputLabelProps={{
              style: {
                color: color || "gray",
                fontSize: 15
              }
            }}
            FormHelperTextProps={{ style: { marginLeft: 4 } }}
            helperText={t(error?.message || "")}
            label={label}
            variant="outlined"
            fullWidth
            type={type}
            {...extraProps}
          />
        );
      }}
    />
  );
};

export default TextInput;
