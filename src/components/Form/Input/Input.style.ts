import TextField from "@mui/material/TextField";
import styled from "@mui/system/styled";
import { DigiHubColor } from "@sms/shared/colors";

export const Input = styled(TextField)(({ theme }) => {
  return {
    "& input": {
      fontSize: 14,
      color: DigiHubColor.black
    },
    "& input ~ fieldset": {
      border: `1px solid ${"#D8D6DE" || theme.palette.grey["50"]} !important`,

      "&: focus-visible": {
        outline: "none"
      }
    }
  };
});
