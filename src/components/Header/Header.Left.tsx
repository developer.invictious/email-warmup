import MenuIcon from "@mui/icons-material/Menu";
import IconButton from "@mui/material/IconButton";
import Box from "@mui/system/Box";

export const LeftHeader = ({
  isDrawerOpen,
  handleDrawerToggle
}: {
  isDrawerOpen: boolean;
  handleDrawerToggle: () => void;
}) => {
  return (
    <Box sx={{ pl: isDrawerOpen ? 0 : 11.25 }}>
      <IconButton
        size="large"
        aria-label="Show notifications"
        color="inherit"
        onClick={handleDrawerToggle}
      >
        <MenuIcon />
      </IconButton>
    </Box>
  );
};
