import React, { Fragment } from "react";
import { IoIosLogOut } from "react-icons/io";

import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {
  amber,
  blue,
  blueGrey,
  cyan,
  deepOrange,
  deepPurple,
  grey,
  indigo,
  lightBlue,
  lightGreen,
  lime,
  pink,
  purple,
  teal
} from "@mui/material/colors";
import Grid from "@mui/material/Grid";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";

import { Chip } from "@mui/material";
import { toastFail } from "@sms/services/service-toast";
import {
  useLoginTokenDetailQuery,
  useLogoutMutation
} from "@sms/services/sms-auth";
import { useFetchUser } from "@sms/services/sms-user";
import { DigiHubColor } from "@sms/shared/colors";
import { useNavigate } from "react-router-dom";
import { HeaderAnchor } from "./";

type TMenuOptions = {
  label: string;
  icon: JSX.Element;
  type?: any[];
  clickHandler: () => void;
}[];

interface IRightHeader {
  handleProfileMenuOpen: (event: any) => void;
  handleMenuClose: (event: any) => void;
  anchorEl: HeaderAnchor;

  mobileMoreAnchorEl: HeaderAnchor;
  isMobileMenuOpen: boolean;
  mobileMenuId: string;
  handleMobileMenuClose: () => void;
}

const avatarColors = [
  pink,
  purple,
  deepPurple,
  indigo,
  blue,
  lightBlue,
  cyan,
  teal,
  lightGreen,
  lime,
  amber,
  deepOrange,
  blueGrey
];

const generateColorIndex = (name: string) => name.length % avatarColors.length;
const stringAvatar = (name: string) => {
  return {
    sx: {
      width: 50,
      height: 50,
      bgcolor: avatarColors[generateColorIndex(name)][500]
    },
    children: `${name.split(" ")[0]?.[0] ?? ""}${name.split(" ")[1]?.[0] ?? ""}`
  };
};

export const RightHeader: React.FC<IRightHeader> = ({
  handleProfileMenuOpen,
  handleMenuClose,
  anchorEl,

  mobileMoreAnchorEl,
  isMobileMenuOpen,
  mobileMenuId,
  handleMobileMenuClose
}) => {
  const { data: tokenDetail } = useLoginTokenDetailQuery();

  const navigate = useNavigate();
  const { mutateAsync: initLogout } = useLogoutMutation();
  const { data: user } = useFetchUser(tokenDetail?.id?.toString() ?? "");

  const logoutHandler = async () => {
    try {
      await initLogout();
      localStorage.removeItem("auth");
      navigate("/");
      window.location.reload();
    } catch (e) {
      toastFail("Some Error Occured!");
    }
  };

  const menuOptions: TMenuOptions = [
    // {
    //   label: "Personal Information",
    //   icon: (
    //     <HiOutlineUserCircle
    //       stroke="#5E5873"
    //       size={24}
    //       style={{ marginRight: 10 }}
    //     />
    //   ),
    //   clickHandler: () => console.log()
    // },
    // {
    //   label: "Change Password",
    //     type: [
    //       Authorities.SUPERADMIN,
    //       Authorities.USER,
    //       Authorities.SUPPORT,
    //       Authorities.AGENT
    //     ],
    //   icon: <Lock width={24} height={24} style={{ marginRight: 10 }} />,
    //   clickHandler: () => console.log()
    // },
    {
      label: "Logout",
      icon: <IoIosLogOut size={24} style={{ marginRight: 10 }} />,
      clickHandler: logoutHandler
    }
  ];

  return (
    <React.Fragment>
      <Box sx={{ display: { xs: "none", md: "flex" } }}>
        {/* <IconButton
          size="large"
          aria-label="Show notifications"
          color="inherit"
        >
          <Badge badgeContent={100} color="error" max={9}>
            <NotificationsIcon sx={{ stroke: grey[900], fill: "white" }} />
          </Badge>
        </IconButton> */}

        <Tooltip title="Open Menu">
          <Button
            onClick={handleProfileMenuOpen}
            sx={{ textTransform: "none" }}
          >
            <Grid sx={{ mx: 1 }}>
              <Grid item xs={12}>
                <Typography
                  variant="body1"
                  gutterBottom
                  component="div"
                  fontWeight={600}
                  fontSize={14}
                  sx={{
                    m: 0,
                    color: "#6E6B7B",
                    lineHeight: 1
                  }}
                >
                  {user?.fullname ||
                    user?.username ||
                    user?.firstName ||
                    "User"}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography
                  fontSize={12}
                  variant="body2"
                  gutterBottom
                  component="div"
                  sx={{ m: 0, color: grey[600], lineHeight: 1 }}
                >
                  {user?.email}
                </Typography>
              </Grid>
            </Grid>

            <Chip
              size="medium"
              sx={{
                color: DigiHubColor.blue,
                borderColor: DigiHubColor.blue,
                "& .MuiChip-avatar": { color: "white" }
              }}
              avatar={
                <Avatar
                  {...stringAvatar(
                    user?.fullname ||
                      user?.username ||
                      user?.firstName ||
                      "User"
                  )}
                />
              }
              label={user?.roles?.[0]?.name}
              variant="outlined"
            />
          </Button>
        </Tooltip>

        {/* Desktop Menus */}
        <Menu
          sx={{ mt: "60px" }}
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          keepMounted
          transformOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={Boolean(anchorEl)}
          onClose={handleMenuClose}
        >
          <MenuOptions menuOptions={menuOptions} />
        </Menu>

        {/* Mobile Menu */}
        <Menu
          sx={{ mt: "60px" }}
          anchorEl={mobileMoreAnchorEl}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          id={mobileMenuId}
          keepMounted
          transformOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={isMobileMenuOpen}
          onClose={handleMobileMenuClose}
        >
          <MenuOptions menuOptions={menuOptions} />
        </Menu>
      </Box>
    </React.Fragment>
  );
};

interface MenuOptionsProps {
  menuOptions: TMenuOptions;
}

const MenuOptions = ({ menuOptions }: MenuOptionsProps) => {
  return (
    <Fragment>
      {menuOptions.map((option) => (
        <MenuItem key={option.label} onClick={option.clickHandler}>
          <Typography
            textAlign="center"
            sx={{ display: "flex", alignItems: "center" }}
          >
            {option.icon} {option.label}
          </Typography>
        </MenuItem>
      ))}
    </Fragment>
  );
};
