import { useState } from "react";

import MoreIcon from "@mui/icons-material/MoreVert";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";

import { LeftHeader } from "./Header.Left";
import { RightHeader } from "./Header.Right";
import { HeaderWrapper } from "./Styled";

interface HeaderProps {
  isDrawerOpen: boolean;
  handleDrawerToggle: () => void;
}

export type HeaderAnchor = null | Element | ((element: Element) => Element);

const mobileMenuId = "primary-search-account-menu-mobile";

const Header = ({ isDrawerOpen, handleDrawerToggle }: HeaderProps) => {
  const [anchorEl, setAnchorEl] = useState<HeaderAnchor>(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    useState<HeaderAnchor>(null);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = (event: any) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  return (
    <HeaderWrapper>
      <LeftHeader
        handleDrawerToggle={handleDrawerToggle}
        isDrawerOpen={isDrawerOpen}
      />
      <Box sx={{ flexGrow: 1 }} />
      <RightHeader
        handleProfileMenuOpen={handleProfileMenuOpen}
        handleMenuClose={handleMenuClose}
        anchorEl={anchorEl}
        mobileMoreAnchorEl={mobileMoreAnchorEl}
        isMobileMenuOpen={isMobileMenuOpen}
        mobileMenuId={mobileMenuId}
        handleMobileMenuClose={handleMobileMenuClose}
      />

      <Box sx={{ display: { xs: "flex", md: "none" } }}>
        <IconButton
          size="large"
          aria-label="show more"
          aria-controls={mobileMenuId}
          aria-haspopup="true"
          onClick={handleMobileMenuOpen}
          color="inherit"
        >
          <MoreIcon />
        </IconButton>
      </Box>

      {/* <RightHeader>
        <Badge badgeContent={2} color="primary">
          <NotificationsIcon />
        </Badge>
        <Badge>
          <SettingsIcon />
        </Badge>

        <Custombadge>
          <img src={Avatar} alt="Avatar" />
        </Custombadge>
      </RightHeader> */}
    </HeaderWrapper>
  );
};

export default Header;
