import InputBase from "@mui/material/InputBase";

import Badge from "@mui/material/Badge";
import Box from "@mui/material/Box";
import { alpha, styled } from "@mui/material/styles";

export const Search = styled("div")(({ theme }) => ({
  position: "relative",
  display: "flex",
  alignItems: "center",
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25)
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    width: "auto"
  }
}));

export const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 1, 0, 0),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
}));

export const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),

    paddingLeft: `calc(1em + ${theme.spacing(2)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch"
    }
  }
}));

export const HeaderWrapper = styled(Box)({
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  width: "100%",
  height: 80,
  padding: "0 1rem",
  "& .MuiGrid-container": {
    textAlign: "center"
  }
});

export const RightHeader = styled(Box)({
  display: "flex",
  justifyContent: "flex-end",
  alignItems: "center",

  "& span": {
    margin: "0 10px",
    crusor: "pointer"
  }
});
export const Custombadge = styled(Badge)({
  "& img": {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    objectFit: "cover"
  }
});
