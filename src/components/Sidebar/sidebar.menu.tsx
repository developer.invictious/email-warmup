import CircleIcon from "@mui/icons-material/Circle";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import { SxProps } from "@mui/material";
import { ReactComponent as Billing } from "@sms/assets/svg/billing.svg";
import { ReactComponent as UserIcon } from "@sms/assets/svg/contacts.svg";
import { ReactComponent as DashboardIcon } from "@sms/assets/svg/dashboard.svg";
import pageRoutes from "@sms/route/PageRoutes";
import { DigiHubColor } from "@sms/shared/colors";

import { Authorities } from "../../services/service-token";

const WhiteCircleIcon = ({
  sx,
  isActive
}: {
  sx: SxProps;
  isActive: boolean;
}) => (
  <CircleIcon
    sx={{
      width: 12,
      ml: 1,
      stroke: isActive ? "white" : DigiHubColor.lightGray,
      fill: isActive ? DigiHubColor.blue : "white"
      //   ...sx
    }}
  />
);

const dashboardItems = {
  key: "dashboard",
  feature: "Dashboard",
  icon: (isActive: boolean) => (
    <DashboardIcon
      style={{
        fill: isActive ? "white" : DigiHubColor.lightGray,
        margin: "auto"
      }}
    />
  ),
  accessor: [
    Authorities.SUPERADMIN,
    Authorities.ADMIN,
    Authorities.RESELLER,
    Authorities.MERCHANT,
    Authorities.RESELLER_MERCHANT,
    Authorities.MERCHANT_USER,
    Authorities.re_MERCHANT
  ],
  path: pageRoutes.dashboard
};

const menuItems = [
  //   {
  //     key: "mailbox",
  //     feature: "Mailbox Config",
  //     accessor: [Authorities.SUPERADMIN],
  //     icon: (isActive: boolean) => (
  //       <MailboxIcon
  //         style={{ fill: isActive ? "white" : DigiHubColor.lightGray }}
  //       />
  //     ),
  //     path: pageRoutes.mail_box
  //   },
  //   {
  //     key: "mailbox",
  //     feature: "Mailbox Config",
  //     accessor: [Authorities.SUPERADMIN],
  //     icon: (isActive: boolean) => (
  //       <MailboxIcon
  //         style={{ fill: isActive ? "white" : DigiHubColor.lightGray }}
  //       />
  //     ),
  //     path: pageRoutes.mail_box_form
  //   },
  //   {
  //     key: "mailing",
  //     feature: "Mailing Config",
  //     accessor: [Authorities.SUPERADMIN],
  //     icon: (isActive: boolean) => (
  //       <MailConfigIcon
  //         style={{ fill: isActive ? "white" : DigiHubColor.lightGray }}
  //       />
  //     ),
  //     path: pageRoutes.mailConfig
  //   },
  {
    key: "email",
    feature: "Email Template",
    accessor: [Authorities.SUPERADMIN, Authorities.RESELLER],
    icon: (isActive: boolean) => (
      <MailOutlineIcon
        style={{
          fill: isActive ? "white" : DigiHubColor.lightGray,
          margin: "auto"
        }}
      />
    ),
    path: pageRoutes.email_template
  },
  {
    key: "user",
    feature: "User Management",
    accessor: [Authorities.SUPERADMIN],
    icon: (isActive: boolean) => (
      <UserIcon
        style={{
          fill: isActive ? "white" : DigiHubColor.lightGray,
          margin: "auto"
        }}
      />
    ),
    path: pageRoutes.user
  },
  {
    key: "billing",
    feature: "Billing",
    accessor: [Authorities.SUPERADMIN],
    icon: (isActive: boolean) => (
      <Billing
        style={{
          fill: isActive ? "white" : DigiHubColor.lightGray,
          margin: "auto"
        }}
      />
    ),
    path: pageRoutes.billing
  }

  //   {
  //     key: "report",
  //     feature: "Report",
  //     accessor: [Authorities.SUPERADMIN],
  //     icon: (isActive: boolean) => (
  //       <ReportIcon
  //         style={{ fill: isActive ? "white" : DigiHubColor.lightGray }}
  //       />
  //     ),
  //     child: [
  //       {
  //         key: "report",
  //         feature: "Report 1",
  //         accessor: [Authorities.SUPERADMIN],
  //         icon: (isActive: boolean) => (
  //           <UserIcon
  //             style={{ fill: isActive ? "white" : DigiHubColor.lightGray }}
  //           />
  //         ),
  //         path: pageRoutes.report
  //       }
  //     ]
  //   }
];

export { menuItems, dashboardItems };
