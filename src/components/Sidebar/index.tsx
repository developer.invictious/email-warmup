import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import { Button, Divider } from "@mui/material";
import MuiAccordion, { AccordionProps } from "@mui/material/Accordion";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import MuiAccordionSummary, {
  AccordionSummaryProps
} from "@mui/material/AccordionSummary";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { styled } from "@mui/material/styles";
import Tooltip, { tooltipClasses, TooltipProps } from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import Zoom from "@mui/material/Zoom";
import { ReactComponent as PremiumIcon } from "@sms/assets/svg/premium.svg";
import pageRoutes from "@sms/route/PageRoutes";
import { useLoginTokenDetailQuery } from "@sms/services/sms-auth";
import { DigiHubColor } from "@sms/shared/colors";
import { Fragment, ReactNode, useEffect, useState } from "react";
import {
  NavLink,
  NavLinkProps,
  useLocation,
  useNavigate
} from "react-router-dom";
import { dashboardItems, menuItems } from "./sidebar.menu";

const BlueTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    borderRadius: 8,
    backgroundColor: DigiHubColor.blue_2,
    color: "white",
    boxShadow: theme.shadows[1],
    fontSize: 13,
    padding: "10px 14px"
  },
  [`& .${tooltipClasses.arrow}`]: {
    color: DigiHubColor.blue_2
  }
}));

const SidebarList = styled(List)({
  //   marginTop: 70,
  padding: "0 4px",
  "& .MuiButtonBase-root": {
    borderRadius: "10px"
  }
});

const StyledNavLink = styled(NavLink)<NavLinkProps>(() => ({
  padding: "10px 12px",
  width: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  borderRadius: 8,
  textDecoration: "none"
}));

const Accordion = styled((props: AccordionProps) => (
  <MuiAccordion disableGutters elevation={0} {...props} />
))(() => ({
  width: "100%",
  "&:before": {
    display: "none"
  }
}));

const AccordionSummary = styled((props: AccordionSummaryProps) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.65rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  padding: "0 4px",
  width: "100%",
  "&.Mui-expanded": {
    background: DigiHubColor.lightSmoke
  },
  "& .MuiAccordionSummary-expandIconWrapper": {
    marginRight: 8
  },
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)"
  },
  "& .MuiAccordionSummary-content": {
    margin: "4px 0",
    marginLeft: theme.spacing(1)
  }
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: "8px 0"
}));

const Sidebar = ({ isDrawerOpen }: { isDrawerOpen: boolean }) => {
  const location = useLocation();
  const navigate = useNavigate();
  const { data: tokenDetails } = useLoginTokenDetailQuery();

  const [openMenus, setOpenMenus] = useState<string[]>([]);

  const handleChange = (panel: string) => () => {
    setOpenMenus((oldState) =>
      oldState.includes(panel)
        ? oldState.filter((p) => p !== panel)
        : [...oldState, panel]
    );
  };

  //   const filterMenuItems = menuItems.filter((menu) =>
  //     menu.accessor.includes(tokenDetails?.roles[0] as Authorities)
  //   );

  useEffect(() => {
    const activeMenu = menuItems
      //   .flatMap((item) => item.child?.map((c) => ({ ...c, key: item.key })))
      .find((item) => (item?.path ? location.pathname === item.path : false));

    if (activeMenu?.key) {
      setOpenMenus([activeMenu.key]);
    }
  }, [location.pathname]);

  return (
    <Fragment>
      <Box
        sx={{
          flex: 1
        }}
      >
        <SidebarList>
          <ListItem key={dashboardItems.feature}>
            <RenderLink
              {...dashboardItems}
              isDrawerOpen={isDrawerOpen}
              key={dashboardItems.key}
            />
          </ListItem>
          <Typography sx={{ pl: 2, fontWeight: "bold" }}>Menus</Typography>
          {menuItems.map((item) => (
            // item.child ? (
            //   <ListItem key={item.feature}>
            //     <Accordion
            //       expanded={openMenus.includes(item.key)}
            //       onChange={handleChange(item.key)}
            //       sx={{
            //         "&:hover": {
            //           backgroundColor: DigiHubColor.lightSmoke,
            //           color: ` ${DigiHubColor.lightGray} !important`
            //         }
            //       }}
            //     >
            //       <BlueTooltip
            //         arrow
            //         title={item.feature}
            //         placement="right"
            //         TransitionComponent={Zoom}
            //         sx={{ display: isDrawerOpen ? "none" : "block" }}
            //       >
            //         <AccordionSummary
            //           aria-controls="panel1d-content"
            //           id="panel1d-header"
            //         >
            //           <ListItemIcon
            //             sx={{
            //               alignItems: "center",
            //               minWidth: isDrawerOpen ? 35 : 24
            //             }}
            //           >
            //             {item.icon(false)}
            //           </ListItemIcon>
            //           <ListItemText
            //             sx={{
            //               opacity: isDrawerOpen ? 1 : 0,
            //               width: isDrawerOpen ? "auto" : 0
            //             }}
            //           >
            //             <Typography fontSize={14}>{item.feature}</Typography>
            //           </ListItemText>
            //         </AccordionSummary>
            //       </BlueTooltip>

            //       <AccordionDetails>
            //         {item.child?.map((subnav) => (
            //           //   subnav.path === pageRoutes.apimanual ? (
            //           //     <a href={DGHUBManual} download>
            //           //       <ListItemText sx={{ py: 1.5, pl: 6 }}>
            //           //         {subnav.feature}
            //           //       </ListItemText>
            //           //     </a>
            //           //   ) :
            //           <RenderLink
            //             {...subnav}
            //             isDrawerOpen={isDrawerOpen}
            //             key={subnav.key}
            //           />
            //         ))}
            //       </AccordionDetails>
            //     </Accordion>
            //   </ListItem>
            // ) :
            <ListItem key={item.feature}>
              <RenderLink
                {...item}
                isDrawerOpen={isDrawerOpen}
                key={item.key}
              />
            </ListItem>
          ))}
        </SidebarList>
      </Box>
      {isDrawerOpen ? (
        <>
          <Divider />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              height: "210px",
              alignItems: "center",
              justifyContent: "center",
              color: DigiHubColor.lightGray
            }}
          >
            <PremiumIcon />
            <Typography fontSize={14} sx={{ pt: 1.5 }}>
              You are currrently in free trial
            </Typography>
            <Typography fontSize={18} fontWeight="bold" sx={{ py: 1.5 }}>
              Upgrade to premium
            </Typography>
            <Button
              variant="contained"
              color="primary"
              onClick={() => navigate(pageRoutes.plans)}
            >
              Upgrade Now
            </Button>
          </Box>
        </>
      ) : null}
    </Fragment>
  );
};

export default Sidebar;

interface RenderLinkProps {
  feature: string;
  path: string;
  icon: (isActive: boolean) => ReactNode;
  isDrawerOpen: boolean;
}
const RenderLink = ({ feature, path, icon, isDrawerOpen }: RenderLinkProps) => {
  return (
    <BlueTooltip
      arrow
      title={feature}
      placement="right"
      TransitionComponent={Zoom}
      sx={{ display: isDrawerOpen ? "none" : "block" }}
    >
      <StyledNavLink
        to={path}
        style={({ isActive }) => ({
          color: isActive ? "white" : DigiHubColor.lightGray,
          background: isActive ? DigiHubColor.blue_2 : "transparent"
        })}
        sx={{
          "&:hover": {
            background: ` ${DigiHubColor.lightSmoke} !important`,
            color: ` ${DigiHubColor.lightGray} !important`
          }
        }}
      >
        {({ isActive }) => (
          <Fragment>
            <ListItemIcon
              sx={{
                alignItems: "center",
                minWidth: isDrawerOpen ? 35 : 24
              }}
            >
              {/* {icon ? icon(isActive) : null} */}
              {icon(isActive)}
            </ListItemIcon>
            <ListItemText
              sx={{
                opacity: isDrawerOpen ? 1 : 0,
                width: isDrawerOpen ? "auto" : 0
              }}
            >
              <Typography fontSize={14}>{feature}</Typography>
            </ListItemText>
          </Fragment>
        )}
      </StyledNavLink>
    </BlueTooltip>
  );
};
