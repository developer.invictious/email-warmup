import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import PaymentForm from "./PaymentForm";

interface IStripeProps {
  handleClose: () => void;
  open: boolean;
  //   currency: string;
  //   description: string;
}

const PUBLIC_KEY = "pk_test_rgWMA3zxjAtwaB6iV8b5W40x";

const stripeTestPromise = loadStripe(PUBLIC_KEY);

export default function StripeContainer(props: IStripeProps) {
  return (
    <Elements stripe={stripeTestPromise}>
      <PaymentForm open={props.open} handleClose={props.handleClose} />
    </Elements>
  );
}
