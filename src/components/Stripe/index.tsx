import { yupResolver } from "@hookform/resolvers/yup";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import {
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  InputAdornment,
  Link,
  Stack,
  Typography
} from "@mui/material";
import { user } from "@sms/hooks/useIsFirstLogin";
import {
  CustomPrimaryButton,
  CustomTextField,
  CustomTypography
} from "@sms/pages/PopupForm/mailbox.style";
import {
  getLoggedDetail,
  useAddCard,
  useGetToken2,
  usePayPlan
} from "@sms/services/warmup-stripe";
import moment from "moment";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

interface IPaymentProps {
  handleClose: () => void;
  open: boolean;
  price: string;
  description: string;
}

const defaultValues = {
  number: "",
  exp_month: "",
  exp_year: "",
  cvc: ""
};
const newDate: number = parseInt(moment(new Date()).format("YYYY"));
const futureDate: number = parseInt(
  moment(new Date()).add(5, "years").format("YYYY")
);

const StripePayment = (props: IPaymentProps) => {
  const { mutateAsync } = getLoggedDetail();
  const { mutateAsync: tokenDetail, isLoading: tokenLoading } = useGetToken2();
  const { mutateAsync: addCard, isLoading: addingCard } = useAddCard();
  const { mutateAsync: payPlan, isLoading } = usePayPlan();

  const schema = Yup.object().shape({
    number: Yup.string()
      .required("Card number is required")
      .min(16, "invalid card number.")
      .max(16, "invalid card number."),
    exp_month: Yup.number()
      .required("Expiry month is required")
      .typeError("you must specify a number")
      .min(1, "invalid month.")
      .max(12, "invalid month."),

    exp_year: Yup.number()
      .required("Expiry year is required")
      .typeError("you must specify a number")
      .min(newDate, "invalid year")
      .max(futureDate, "invalid year"),

    cvc: Yup.string().required("CVC is required")
  });

  const { control, handleSubmit } = useForm({
    defaultValues,
    resolver: yupResolver(schema)
  });

  const onPaymentHandler = async (paymentDetails: typeof defaultValues) => {
    tokenDetail(paymentDetails).then((result: any) => {
      mutateAsync(user.id).then(async (x: any) => {
        if (result.data.id && x.data.stripeId) {
          const response = await addCard({
            id: x.data.stripeId,
            token: result.data.id
          });
          if (response.status === 200) {
            const res = await payPlan({
              amount: props.price,
              currency: "usd",
              description: props.description,
              id: x.data.stripeId
            });
            if (res.status === 200) {
              props.handleClose();
            }
          }
        }
      });
    });
  };

  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      sx={{
        ".MuiPaper-root": {
          borderRadius: "12px",
          width: "50%"
        }
      }}
    >
      <DialogTitle
        display="flex"
        sx={{
          background: "#E6E7F1",
          justifyContent: "space-between",
          alignItems: "center",
          py: 3,
          px: 5
        }}
      >
        <Typography sx={{ fontSize: 18, fontWeight: 600, color: "#4D4D4D;" }}>
          Stripe Pay
        </Typography>
        <Link
          onClick={() => {
            props.handleClose();
          }}
          sx={{
            textDecoration: "none",
            cursor: "pointer",
            fontSize: 16,
            fontWeight: 600
          }}
        >
          X
        </Link>
      </DialogTitle>
      <DialogContent
        sx={{
          px: 5,
          mt: 7,
          "&::-webkit-scrollbar": {
            width: "7px"
          },
          "&::-webkit-scrollbar-track": {
            background: "#f1f1f1"
          },

          /* Handle */
          "&::-webkit-scrollbar-thumb": {
            background: "#888"
          },

          /* Handle on hover */
          "&::-webkit-scrollbar-thumb:hover": {
            background: "#F1F1F1"
          }
        }}
      >
        <Grid container>
          <form
            style={{ width: "100%" }}
            onSubmit={handleSubmit(onPaymentHandler)}
          >
            <Grid container columnSpacing={6} alignItems="center">
              <Grid item xs={12}>
                <CustomTypography>Card Number</CustomTypography>
                <CustomTextField
                  control={control}
                  name="number"
                  inputProps={{ maxLength: 16 }}
                  type="text"
                  placeholder="Card Number"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton size="small">
                          <CreditCardIcon />
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <CustomTypography>Expiry Month</CustomTypography>
                <CustomTextField
                  control={control}
                  name="exp_month"
                  inputProps={{ maxLength: 2 }}
                  type="text"
                  placeholder="Expiry Month"
                />
              </Grid>
              <Grid item xs={12}>
                <CustomTypography>Expiry Year</CustomTypography>
                <CustomTextField
                  control={control}
                  inputProps={{ maxLength: 4 }}
                  name="exp_year"
                  type="text"
                  placeholder="Expiry Year"
                />
              </Grid>
              <Grid item xs={12}>
                <CustomTypography>CVC</CustomTypography>
                <CustomTextField
                  control={control}
                  name="cvc"
                  type="text"
                  placeholder="CVC"
                />
              </Grid>

              <Grid item xs={8}></Grid>
              <Grid item xs={4}>
                <Stack flexDirection={"row"} sx={{ mt: 4, mb: 2 }}>
                  <CustomPrimaryButton
                    variant="contained"
                    disableElevation
                    type="submit"
                    loading={tokenLoading || isLoading || addingCard}
                    sx={{ marginRight: "20px" }}
                    // onClick={props.handleClose}
                  >
                    Pay
                  </CustomPrimaryButton>
                  <Button
                    variant="contained"
                    disableElevation
                    color="error"
                    sx={{
                      borderRadius: "6px",
                      height: "42px",
                      padding: "12px 24px",
                      fontWeight: "600",
                      textTransform: "none"
                    }}
                    onClick={props.handleClose}
                  >
                    Cancel
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </DialogContent>
    </Dialog>
  );
};
export default StripePayment;
