import {
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Link,
  Stack,
  Typography
} from "@mui/material";
import { useLoginTokenDetailQuery } from "@sms/services/sms-auth";
import { useFetchUser } from "@sms/services/sms-user";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import { useState } from "react";

interface IPaymentProps {
  handleClose: () => void;
  open: boolean;
  //   currency: string;
  //   description: string;
}

// const defaultValues = {
//   number: "",
//   exp_month: "",
//   exp_year: "",
//   cvc: ""
// };

const CARD_OPTIONS: any = {
  iconStyle: "solid",
  style: {
    base: {
      iconColor: "#c4f0ff",
      color: "blue",
      fontWeight: 500,
      fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
      fontSize: "16px",
      fontSmoothing: "antialiased",
      ":-webkit-autofill": { color: "#fce883" },
      "::placeholder": { color: "#87bbfd" }
    },
    invalid: {
      iconColor: "black",
      color: "black"
    }
  }
};

export default function PaymentForm(props: IPaymentProps) {
  const { data: tokenDetail } = useLoginTokenDetailQuery();
  const { data: user } = useFetchUser(tokenDetail?.id?.toString() ?? "");

  const [success, setSuccess] = useState(false);
  const stripe: any = useStripe();
  const elements: any = useElements();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    await stripe.createToken(CardElement).then(function (result: any) {
      console.log(result.token);
    });
  };
  return (
    <>
      {!success ? (
        <Dialog
          open={props.open}
          onClose={props.handleClose}
          sx={{
            ".MuiPaper-root": {
              borderRadius: "12px",
              width: "50%"
            }
          }}
        >
          <DialogTitle
            display="flex"
            sx={{
              background: "#E6E7F1",
              justifyContent: "space-between",
              alignItems: "center",
              py: 3,
              px: 5
            }}
          >
            <Typography
              sx={{ fontSize: 18, fontWeight: 600, color: "#4D4D4D;" }}
            >
              Stripe Pay
            </Typography>
            <Link
              onClick={() => {
                props.handleClose();
              }}
              sx={{
                textDecoration: "none",
                cursor: "pointer",
                fontSize: 16,
                fontWeight: 600
              }}
            >
              X
            </Link>
          </DialogTitle>
          <DialogContent
            sx={{
              mt: 7,
              "&::-webkit-scrollbar": {
                width: "7px"
              },
              "&::-webkit-scrollbar-track": {
                background: "#f1f1f1"
              },

              /* Handle */
              "&::-webkit-scrollbar-thumb": {
                background: "#888"
              },

              /* Handle on hover */
              "&::-webkit-scrollbar-thumb:hover": {
                background: "#F1F1F1"
              }
            }}
          >
            <form onSubmit={handleSubmit}>
              <fieldset className="FormGroup">
                <div className="FormRow">
                  <CardElement options={CARD_OPTIONS} />
                </div>
              </fieldset>
              <Stack flexDirection="row" sx={{ mt: 4, mb: 2 }}>
                <Button
                  variant="contained"
                  type="submit"
                  sx={{ marginRight: "10px" }}
                >
                  Pay
                </Button>
                <Button
                  variant="contained"
                  color="error"
                  onClick={props.handleClose}
                >
                  Cancel
                </Button>
              </Stack>
            </form>
          </DialogContent>
        </Dialog>
      ) : (
        <>
          {/* <div>
            <h2>
             Sucessfully payed
            </h2>
          </div> */}
        </>
      )}
    </>
  );
}
