import styled from "@mui/system/styled";
import ClearIcon from "@mui/icons-material/Clear";

interface IFileUploadProps {
  uiStyles?: React.CSSProperties;
}

const FileUploadContainer = styled("div", {
  shouldForwardProp: (prop) => prop !== "uiStyles"
})<IFileUploadProps>(({ theme, uiStyles }) => ({
  flex: "1",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  padding: "40px",
  borderWidth: " 2px",
  borderRadius: "10px",
  borderColor: theme.palette.primary.main,
  //   borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "black",
  fontWeight: "bold",
  fontSize: "1.4rem",
  outline: "none",
  transition: "border 0.24s ease-in-out",
  position: "relative",
  zIndex: "1",
  ...uiStyles
}));

const ClearIconStyle = styled(ClearIcon)({
  position: "absolute",
  top: "10px",
  right: "10px",
  cursor: "pointer",
  zIndex: "2"
});

export { FileUploadContainer, ClearIconStyle };
