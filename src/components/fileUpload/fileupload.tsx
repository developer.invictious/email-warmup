import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDropzone } from "react-dropzone";
import * as XLSX from "xlsx";

import Typography from "@mui/material/Typography";

import { ReactComponent as Upload } from "../../assets/svg/upload.svg";
import { toastFail } from "../Toast";
import { ClearIconStyle, FileUploadContainer } from "./fileuploadStyle";

interface IFileUploadProps {
  onDropHandler?: (file: string | File) => void;
  onChangeFile?: (data: any) => void;
  image?: string;
  customStyles?: React.CSSProperties;
  textName?: string;
  imageUpload?: any;
  accept?: any;
  isBase64?: boolean;
  isFile?: boolean;
}

export default function Fileupload(props: IFileUploadProps) {
  const {
    customStyles,
    image: defaultImage,
    textName,
    imageUpload,
    isBase64,
    accept,
    onChangeFile,
    isFile,
    ...restprops
  } = props;
  const [image, setImage] = useState("");
  const [fileName, setFileName] = useState(textName);

  const onDrop = useCallback((acceptedFiles: any) => {
    acceptedFiles.map((file: File) => {
      if (!isBase64 && !isFile) {
        const reader = new FileReader();

        reader.onload = async (e) => {
          const fileBuffer = e.target?.result as ArrayBuffer;

          const byteArrayData = new Uint8Array(fileBuffer);

          const workbook = XLSX.read(byteArrayData, {
            type: "array",
            // For UTF-8
            codepage: 65001
          });
          const wsname = workbook.SheetNames[0];
          const worksheet = workbook.Sheets[wsname];
          const fileData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

          onChangeFile?.(fileData);
          setFileName(file.name);
        };

        reader.readAsArrayBuffer(file);
        return file;
      } else if (isFile) {
        setFileName(file.name);
        props.onDropHandler?.(file);
      } else {
        const reader = new FileReader();

        reader.onload = function (e) {
          const imagesrc = e.target?.result as string;
          setImage(imagesrc);
        };

        reader.readAsDataURL(file);
        return file;
      }
    });
  }, []);

  const onDropRejected = () => {
    toastFail("Please upload images with size less than 1MB");
  };

  const clearImage = (e: React.MouseEvent<SVGSVGElement>) => {
    e.preventDefault();
    e.stopPropagation();
    setImage("");
  };

  const { getRootProps, getInputProps } = useDropzone({
    // accept: "image/*",
    accept: {
      "image/*": accept ?? []
    },
    onDrop,
    noKeyboard: true,
    maxSize: 1000 * 1024,
    onDropRejected
  });

  useEffect(() => {
    restprops.onDropHandler?.(image);
  }, [image]);

  useEffect(() => {
    if (defaultImage) {
      setImage("data:image/jpeg;base64," + defaultImage);
    } else {
      setImage("");
    }
  }, [defaultImage]);

  return (
    <FileUploadContainer {...getRootProps()} uiStyles={customStyles}>
      <input {...getInputProps()} />
      {image ? (
        <ClearIconStyle onClick={clearImage} />
      ) : (
        <>
          {imageUpload ? imageUpload : <Upload />}
          <Typography
            fontSize={12}
            fontWeight={500}
            fontStyle="normal"
            fontFamily="nunito"
          >
            <span>{fileName}</span>
          </Typography>
        </>
      )}
      {image ? (
        <img
          src={image}
          height={200}
          width={200}
          style={{
            objectFit: "contain"
          }}
        />
      ) : null}
    </FileUploadContainer>
  );
}

Fileupload.defaultProps = {
  textName: "upload your file"
};
