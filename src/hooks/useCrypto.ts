import CryptoJS from "crypto-js";

const useHash = () => {
  // Encrypt
  return (data: string) =>
    CryptoJS.AES.encrypt(JSON.stringify(data), "CAL")
      .toString()
      .replace(/[+]/g, "xMl3Jk")
      .replace(/[/]/g, "Por21Ld")
      .replace(/[=]/g, "Ml32");
};

const useDehash = () => {
  // Decrypt
  return (hashedText: string) => {
    try {
      const bytes = CryptoJS.AES.decrypt(
        hashedText
          .replace(/xMl3Jk/g, "+")
          .replace(/Por21Ld/g, "/")
          .replace(/Ml32/g, "="),
        "CAL"
      );
      return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    } catch (err) {
      return null;
    }
  };
};

export { useHash, useDehash };
