import { toastFail } from "@sms/components/Toast";
import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";
import { toastSuccess } from "./service-toast";
import { UserRole } from "./sms-role";
export interface IUser {
  id: number | string;
  email: string;
  firstName: string;
  lastName: string;
  roles: UserRole[];
  createdAt: string;
  result: Record<string, any>[];
  pages: number;
}

interface IUserSave {
  id?: number | string;
  email: string;
  firstName: string;
  lastName: string;
  roles: number[];
}

interface IUserEdit {
  id: number | string;
  email: string;
  firstName: string;
  lastName: string;
  roles: string[];
}

interface IUserRoles {
  id: number;
  name: string;
  description: string;
}

interface IPageLimit {
  page: number;
  limit: number;
}

const fetchRoles = () => {
  return httpClient.get<IUserRoles[]>(api.user_management.fetch.roles);
};

const fetchUsers =
  ({ page, limit }: IPageLimit) =>
  () => {
    return httpClient.get<IUser>(api.user_management.fetch.users, {
      params: { page, limit }
    });
  };

const fetchUserById = (id: string) => () => {
  return httpClient.get<IUser>(
    api.user_management.fetch.userById.replace("{userId}", id)
  );
};

const createUser = (userDetail: IUserSave) => {
  return httpClient.post<IUser>(api.user_management.post.user, userDetail);
};

const editUser = (userDetail: IUserEdit) => {
  return httpClient.patch(
    api.user_management.put.user.replace("{userId}", userDetail.id.toString()),
    userDetail
  );
};

const deleteUser = (id: string) => {
  return httpClient.delete(
    api.user_management.delete.user.replace("{userId}", id)
  );
};

export const useFetchRole = () => {
  return useQuery(api.user_management.fetch.roles, fetchRoles, {
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useFetchAllUsers = ({ page, limit }: IPageLimit) => {
  return useQuery(
    [api.user_management.fetch.users, page, limit],
    fetchUsers({ page, limit } || ""),
    {
      enabled: !!page,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const useFetchUserById = (userId: string) => {
  return useQuery(
    [api.user_management.fetch.userById, userId],
    fetchUserById(userId || ""),
    {
      enabled: !!userId,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const useCreateUser = () => {
  const queryClient = useQueryClient();

  return useMutation(createUser, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.user_management.fetch.users);
      toastSuccess("Sucessfully Added");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useUpdateUser = () => {
  const queryClient = useQueryClient();

  return useMutation(editUser, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.user_management.fetch.users);
      toastSuccess("Successfully Updated");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};
export const useDeleteUser = () => {
  const queryClient = useQueryClient();
  return useMutation(deleteUser, {
    onSuccess(data) {
      queryClient.refetchQueries(api.user_management.fetch.users);
      queryClient.invalidateQueries(api.user_management.fetch.users);
      toastSuccess("Deleted Sucessfully");
    },
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};
