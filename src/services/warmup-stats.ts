import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";

interface IStats {
  total_message_sent: number;
  total_inbox_read: number;
  total_message_replied: number;
  total_spam_converted: number;
}

interface IStatsDetail {
  id: string;
  type: string;
  value: string;
}

const fetchStatsById = (id: string) => () => {
  return httpClient.get<any>(api.stats.fetchById.replace("{statsId}", id));
};

const fetchStatsId = (id: string) => () => {
  return httpClient.get<IStats>(api.stats.fetch.replace("{statsId}", id));
};

export const useFetchStatsId = (statsId: string) => {
  return useQuery([api.stats.fetch, statsId], fetchStatsId(statsId || ""), {
    enabled: !!statsId,
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useFetchStatsById = (statsId: string) => {
  return useQuery(
    [api.stats.fetchById, statsId],
    fetchStatsById(statsId || ""),
    {
      enabled: !!statsId,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

const fetchByDate =
  ({ type, value, id }: IStatsDetail) =>
  () => {
    return httpClient.get<IStatsDetail>(
      api.stats.fetchByDate.replace("{statsId}", id),
      {
        params: { type, value }
      }
    );
  };

export const useFetchByDate = ({ type, value, id }: IStatsDetail) => {
  return useQuery(
    [api.stats.fetchByDate, type, value, id],
    fetchByDate({ type, value, id } || ""),
    {
      enabled: !!id,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

const getPaymentDetail = (customer: string) => () => {
  return httpClient.get<[]>(api.stats.getPaymentDetail, {
    params: { customer }
  });
};

export const useGetPaymentDetail = (customer: string) => {
  return useQuery(
    [api.stats.getPaymentDetail, customer],
    getPaymentDetail(customer || ""),
    {
      enabled: !!customer,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};
