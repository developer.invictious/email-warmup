import { toastFail } from "@sms/components/Toast";
import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";
import { toastSuccess } from "./service-toast";

export interface ISmtpAdd {
  id?: number;
  server: string;
  name: string;
  smtp_host: string;
  imap_host: string;
  password: string;
  smtp_port: number;
  imap_port: number;
  email: string;
  encryption: string;
}

export interface ISmtpEdit {
  id: string;

  smtp_host: string;
  imap_host: string;

  smtp_port: string;
  imap_port: string;
  email: string;
  encryption: string;
}

export interface ISmtpDetail {
  id: number;
  server: string;
  name: string;
  smtp_host: string;
  imap_host: string;
  password: string;
  smtp_port: string;
  imap_port: string;
  email: string;
  encryption: string;
}

export interface IMailbox {
  id?: number;
  email: string;
  server: string;
  status: boolean;
  createdAt: string;
  editable: boolean;
  result: Record<string, any>[];
  page: number;
}

interface IPageLimit {
  page: number;
  limit: number;
  status: string;
}

const googleToken = (token: string) => () => {
  return httpClient.get<{ email: string }>(api.mailbox.token.google, {
    params: { code: token }
  });
};

const yahooToken = (token: string) => () => {
  return httpClient.get<{ email: string }>(api.mailbox.token.yahoo, {
    params: { code: token }
  });
};

const toggleById = (id: string) => {
  return httpClient.get(api.mailbox.toggle.replace("{toggleId}", id));
};

const handleGoogle = () => {
  return httpClient.get<string>(api.mailbox.form.google);
};
const handleYahoo = () => {
  return httpClient.get<string>(api.mailbox.form.yahoo);
};
export const useHandleGoogle = () => {
  return useQuery(api.mailbox.form.google, handleGoogle);
};

export const useHandleYahoo = () => {
  return useQuery(api.mailbox.form.yahoo, handleYahoo);
};

const fetchMailboxName = () => {
  return httpClient.get<[]>(api.mailbox.fetch.mailbox);
};

const fetchMailboxNameById = (id: string) => () => {
  return httpClient.get(
    api.mailbox.fetch.mailboxById.replace("{mailboxId}", id)
  );
};

const deleteMailbox = (id: string) => {
  return httpClient.delete(
    api.mailbox.delete.mailbox.replace("{mailboxId}", id)
  );
};

const deleteSmt = (id: string) => {
  return httpClient.delete(api.mailbox.delete.smtp.replace("{smtpId}", id));
};

export const useDeleteMailbox = () => {
  const queryClient = useQueryClient();
  return useMutation(deleteMailbox, {
    onSuccess(data) {
      queryClient.refetchQueries(api.mailbox.fetch.get);
      queryClient.invalidateQueries(api.mailbox.fetch.get);
      toastSuccess("Delete Sucessfully");
    },
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useFetchMailboxById = (mailboxId: string) => {
  return useQuery(
    [api.mailbox.fetch.mailboxById, mailboxId],
    fetchMailboxNameById(mailboxId || ""),
    {
      enabled: !!mailboxId,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};
//here
const fetchMailbox =
  ({ page, limit, status }: IPageLimit) =>
  () => {
    return httpClient.get<IMailbox>(api.mailbox.fetch.get, {
      params: { page, limit, status }
    });
  };

const fetchMailBoxById = (id: string) => () => {
  return httpClient.get<ISmtpDetail>(
    api.mailbox.fetch.getById.replace("{mailboxId}", id)
  );
};

const createMailbox = (smtpDetail: ISmtpAdd) => {
  return httpClient.post<{ email: string }>(api.mailbox.post.post, smtpDetail);
};

const createSmtp = (smtpDetail: ISmtpAdd) => {
  return httpClient.post<{ domain: string }>(api.mailbox.post.smtp, smtpDetail);
};

const editMailbox = (smtpDetail: ISmtpEdit) => {
  return httpClient.patch(
    api.mailbox.put.put.replace("{mailboxId}", smtpDetail.id),
    smtpDetail
  );
};

const editSmtp = (smtpDetail: ISmtpEdit) => {
  return httpClient.patch(
    api.mailbox.put.smtp.replace("{smtpId}", smtpDetail.id),
    smtpDetail
  );
};

export const useFetchMailBoxById = (mailboxId: string) => {
  return useQuery(
    [api.mailbox.fetch.getById, mailboxId],
    fetchMailBoxById(mailboxId || ""),
    {
      enabled: !!mailboxId,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const useUpdateMailbox = () => {
  const queryClient = useQueryClient();
  return useMutation(editMailbox, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.mailbox.fetch.get);
      toastSuccess("Successfully Updated");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useUpdateSmtp = () => {
  const queryClient = useQueryClient();
  return useMutation(editSmtp, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.mailbox.fetch.get);
      toastSuccess("Successfully Updated");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useFetchMailboxName = () => {
  return useQuery(api.mailbox.fetch.mailbox, fetchMailboxName, {
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};
//here
export const useFetchMailbox = ({ page, limit, status }: IPageLimit) => {
  return useQuery(
    [api.mailbox.fetch.get, page, limit, status],
    fetchMailbox({ page, limit, status } || ""),
    {
      enabled: !!page,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const useCreateMailbox = () => {
  const queryClient = useQueryClient();

  return useMutation(createMailbox, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.mailbox.fetch.get);
      toastSuccess("Sucessfully Added");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useCreateSmtp = () => {
  const queryClient = useQueryClient();

  return useMutation(createSmtp, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.mailbox.fetch.get);
      toastSuccess("Sucessfully Added");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useGoogleToken = (token: string) => {
  return useQuery([api.mailbox.token.google, token], googleToken(token || ""), {
    enabled: !!token,
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useYahooToken = (token: string) => {
  return useQuery([api.mailbox.token.yahoo, token], yahooToken(token || ""), {
    enabled: !!token,
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useToggle = () => {
  const queryClient = useQueryClient();
  return useMutation(toggleById, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.mailbox.fetch.get);
      toastSuccess("Successfully toggled");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};
