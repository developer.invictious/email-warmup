import { toastFail } from "@sms/components/Toast";
import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { api, SMSResponse } from "./service-api";
import { httpClient } from "./service-axios";
import { toastSuccess } from "./service-toast";
import { UserRole } from "./sms-role";
interface IUser {
  id: number;
  firstName: string;
  stripeId: string;
  username: string;
  email: string;
  fullname: string;
  address: string;
  roles: UserRole[];
  status: boolean;
  activeStatus: boolean;
  isActive: boolean;
}
export interface IAdminUser {
  createdDate: string;
  updatedDate: string;
  isActive: boolean;
  id: number;
  username: string;
  email: string;
  fullName: string;
  address: string;
  contactNumber: null | string;
  companyName: null | string;
  status: boolean;
}

interface IUserSave extends Omit<IUser, "id" | "status" | "roles"> {
  id?: string;
  rePassword: string;
  password: string;
  roleIdList: number[];
  merchantId: number;
}

interface IUserEdit {
  id: number;
  email: string;
  address: string;
}

const fetchUsers = () => {
  return httpClient.get<SMSResponse<IUser[]>>(api.user.fetch.users);
};

const fetchUser = (id: string) => () => {
  return httpClient.get<IUser>(api.user.fetch.user.replace("{userId}", id));
};

const getUserByStatus = (status: string) => () => {
  return httpClient.get<SMSResponse<IUser>>(
    api.user.fetch.getUserByStatus.replace("{status}", status)
  );
};

const createUser = (
  userDetail: Omit<IUserSave, "isActive" | "activeStatus">
) => {
  return httpClient.post<SMSResponse<IUser>>(api.user.post.user, userDetail);
};

const editUser = (userDetail: IUserEdit) => {
  return httpClient.put(api.user.put.user, userDetail);
};

const fetchAdminUsers = (superAdminId: string) => () => {
  return httpClient.get<SMSResponse<IAdminUser[]>>(
    api.user.fetch.adminUsers.replace("{superAdminId}", superAdminId)
  );
};

const getUserByMerchantId = (id: string) => () => {
  return httpClient.get<SMSResponse<IUser>>(api.user.fetch.userByMerchantId);
};

const getMyProfile = () => {
  return httpClient.get<SMSResponse<IUser>>(api.user.fetch.userProfile);
};

export const useFetchAllUsers = () => {
  const queryClient = useQueryClient();
  return useQuery(api.user.fetch.users, fetchUsers, {
    select: ({ data }) => data?.data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

// export const useFetchAllAdmin = () => {
//   const tokenDetails = useLoginTokenDetailQuery();
//   const userId = tokenDetails.data?.userId?.toString() ?? "";

//   return useQuery(
//     [api.user.fetch.users, userId],
//     fetchAdminUsers(userId ?? ""),
//     {
//       enabled: !!userId,
//       keepPreviousData: true,
//       select: ({ data }) => data?.data,
//       onError: (error: AxiosError) => {
//         console.log(error);
//       }
//     }
//   );
// };

export const useFetchUser = (userId: string) => {
  return useQuery([api.user.fetch.users, userId], fetchUser(userId), {
    enabled: !!userId,
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useFetchMyProfile = () => {
  const queryClient = useQueryClient();
  return useQuery(api.user.fetch.userProfile, getMyProfile, {
    select: ({ data }) => data?.data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useFetchUserByMerchantId = (merchantId: number) => {
  const queryClient = useQueryClient();
  return useQuery(
    [api.user.fetch.userByMerchantId, merchantId],
    getUserByMerchantId(merchantId?.toString() || ""),
    {
      select: ({ data }) => data?.data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const toggleUser = (id: number | null) => {
  return httpClient.put<IUser>(
    api.user.put.toggleUser.replace("{id}", `${id}` || ""),
    {}
  );
};

export const useToggleUser = () => {
  const queryClient = useQueryClient();
  return useMutation(api.user.put.toggleUser, toggleUser, {
    onSuccess: (data) => {
      queryClient.invalidateQueries(api.user.fetch.users);
      toastSuccess("User Status Changed Successfully");
    },
    onError: (error: AxiosError<{ message: string; error: string }>) => {
      console.log(error);
    }
  });
};

export const useGetUserByStatus = (status: boolean) => {
  const queryClient = useQueryClient();
  return useQuery(
    [api.user.fetch.getUserByStatus, status],
    getUserByStatus(status ? "true" : "false"),
    {
      onSuccess: () => {
        console.log("success");
      },
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};
export const useCreateUser = () => {
  const queryClient = useQueryClient();

  return useMutation(createUser, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.user.fetch.users);
      toastSuccess("Successfully Added");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};
export const useUpdateUser = () => {
  const queryClient = useQueryClient();

  return useMutation(editUser, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.user.fetch.adminUsers);
      toastSuccess("Successfully Updated");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};
