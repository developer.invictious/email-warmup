import { AxiosError } from "axios";
import { useQuery } from "react-query";

import { api, SMSResponse } from "./service-api";
import { httpClient } from "./service-axios";

export interface UserRole {
  id: number;
  name: string;
  description: string;
}

const fetchRoles = () => {
  return httpClient.get<SMSResponse<UserRole[]>>(api.role.fetch.roles);
};

const useRolesQuery = () => {
  return useQuery(api.role.fetch.roles, fetchRoles, {
    refetchOnMount: false,
    select: ({ data }) => data?.data || [],
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export { useRolesQuery };
