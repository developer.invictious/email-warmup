import { toastFail } from "@sms/components/Toast";
import { AxiosError } from "axios";
import { useMutation, useQueryClient } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";
import { toastSuccess } from "./service-toast";

export interface IRegister {
  id?: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}

const createRegister = (registerDetail: IRegister) => {
  return httpClient.post<IRegister>(api.register, registerDetail);
};

export const useCreateRegister = () => {
  const queryClient = useQueryClient();

  return useMutation(createRegister, {
    onSuccess: () => {
      //   queryClient.invalidateQueries(api.register);
      toastSuccess("Sucessfully Registered");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};
