import jwt_decode from "jwt-decode";
import { UserRole } from "./sms-role";
export interface TokenDetails {
  access_token: string;
  token_type: string;
  expires_in: number;
  scope: string;
  refresh_token: string;
}

export interface ITokenDetails {
  id: number;
  email: string;

  roles: UserRole[];
}
export enum Authorities {
  SUPERADMIN = "SUPERADMIN",
  RESELLER_MERCHANT = "RESELLER_MERCHANT",
  re_MERCHANT = "re_MERCHANT",
  MERCHANT_USER = "MERCHANT_USER",
  ADMIN = "ADMIN",
  MERCHANT = "MERCHANT",
  RESELLER = "RESELLER"
}

function setToken(token: TokenDetails) {
  try {
    localStorage.setItem("auth", JSON.stringify(token));
  } catch (e) {
    console.error("Error storing token", e);
  }
}

function getToken() {
  try {
    return JSON.parse(localStorage.getItem("auth") || "") as TokenDetails;
  } catch (e) {
    return null;
  }
}

function getTokenDetails(): ITokenDetails | null {
  try {
    const token = getToken();
    return token ? jwt_decode(token.access_token) : null;
  } catch (e) {
    return null;
  }
}

function isAuthenticated() {
  const tokenDetails = getTokenDetails();
  if (tokenDetails) {
    return tokenDetails;
    // .exp * 1000 > Date.now();
  } else {
    return false;
  }
}

function clearToken() {
  localStorage.removeItem("auth");
}

// export const checkAuthority = (authorities: Authorities[]) => {
//   const tokenDetails = getTokenDetails();
//   if (tokenDetails) {
//     return tokenDetails.authorities.find((auth) => authorities.includes(auth));
//   } else return false;
// };

const TokenService = {
  setToken,
  getToken,
  getTokenDetails,
  isAuthenticated,
  clearToken
  //   checkAuthority
};

export default TokenService;
