import axios, { AxiosError } from "axios";
import qs from "qs";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { toastFail } from "./service-toast";
import TokenService, { ITokenDetails, TokenDetails } from "./service-token";
export interface LoginDetails {
  email: string;
  password: string;
}

const initLogout = () => {
  try {
    TokenService.clearToken();
    return Promise.resolve(true);
  } catch (err) {
    return Promise.resolve(false);
  }
};
const initLogin = (loginData: LoginDetails) => {
  const body = qs["stringify"]({ ...loginData, grant_type: "password" });
  return axios.post<TokenDetails>(
    "http://116.202.10.98:8005/api/auth/login",

    loginData
  );
};
const authTokenKey = "authTokenKey";
const authTokenDetails = "authTokenDetails";
const useLoginMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(initLogin, {
    onSuccess: ({ data }) => {
      TokenService.setToken(data);
      queryClient.setQueryData(authTokenKey, () => true);
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error?.response?.data.message || "");
    }
  });
};

const checkAuthentication = () => {
  if (TokenService.isAuthenticated()) {
    return Promise.resolve(true);
  }
  return Promise.reject(false);
};

const useAuthentication = () => {
  const queryClient = useQueryClient();
  return useQuery(authTokenKey, checkAuthentication, {
    onSuccess: () => {
      const tokenDetails = TokenService.getTokenDetails();
      if (tokenDetails) {
        queryClient.setQueryData<ITokenDetails>(authTokenDetails, tokenDetails);
      }
    },
    onError: (error) => {
      console.log(error);
    }
  });
};

const useLoginTokenDetailQuery = () => {
  return useQuery<unknown, unknown, ITokenDetails>(authTokenDetails);
};

const useLogoutMutation = () => {
  const queryClient = useQueryClient();
  return useMutation(initLogout, {
    onSuccess: () => {
      queryClient.setQueryData(authTokenKey, () => false);
      queryClient.removeQueries();
    }
  });
};

export {
  useAuthentication,
  useLoginMutation,
  useLoginTokenDetailQuery,
  useLogoutMutation
};
