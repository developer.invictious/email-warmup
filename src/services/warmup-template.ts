import { toastFail } from "@sms/components/Toast";
import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";
import { toastSuccess } from "./service-toast";
interface ITemplate {
  id: number;
  title: string;
  language: string;
  description: string;
  result: Record<string, any>[];
  page: number;
}
interface ITemplateSave extends Omit<ITemplate, "id" | "result" | "page"> {
  id?: string;
}

interface ITemplateEdit {
  id: number;
  title: string;
  language: string;
  description: string;
}

interface IPageLimit {
  page: number;
  limit: number;
}

const lang = () => {
  return httpClient.get<[]>(api.email_template.language);
};

const fetchTemp =
  ({ page, limit }: IPageLimit) =>
  () => {
    return httpClient.get<ITemplate>(api.email_template.fetch.temp, {
      params: { page, limit }
    });
  };

const fetchTempById = (id: string) => () => {
  return httpClient.get<ITemplate>(
    api.email_template.fetch.tempById.replace("{templateId}", id)
  );
};

const createTemp = (tempDetail: ITemplateSave) => {
  return httpClient.post<ITemplate>(api.email_template.post.temp, tempDetail);
};

const editTemp = (tempDetail: ITemplateEdit) => {
  return httpClient.patch(
    api.email_template.put.temp.replace(
      "{templateId}",
      tempDetail.id.toString()
    ),
    tempDetail
  );
};

const deleteTemp = (id: string) => {
  return httpClient.delete(
    api.email_template.delete.temp.replace("{templateId}", id)
  );
};

export const useLang = () => {
  return useQuery(api.email_template.language, lang, {
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useFetchAllTemps = ({ page, limit }: IPageLimit) => {
  return useQuery(
    [api.email_template.fetch.temp, page, limit],
    fetchTemp({ page, limit } || ""),
    {
      enabled: !!page,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const useFetchTempById = (templateId: string) => {
  return useQuery(
    [api.email_template.fetch.tempById, templateId],
    fetchTempById(templateId || ""),
    {
      enabled: !!templateId,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const useCreateTemp = () => {
  const queryClient = useQueryClient();

  return useMutation(createTemp, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.email_template.fetch.temp);
      toastSuccess("Sucessfully Added");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useUpdateTemp = () => {
  const queryClient = useQueryClient();

  return useMutation(editTemp, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.email_template.fetch.temp);
      toastSuccess("Successfully Updated");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};
export const useDeleteTemp = () => {
  const queryClient = useQueryClient();
  return useMutation(deleteTemp, {
    onSuccess(data) {
      queryClient.refetchQueries(api.email_template.fetch.temp);
      queryClient.invalidateQueries(api.email_template.fetch.temp);
      toastSuccess("Delete Sucessfully");
    },
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};
