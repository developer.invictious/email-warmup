import { toastFail } from "@sms/components/Toast";
import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";
import { toastSuccess } from "./service-toast";

export interface IMailingConfig {
  id: number | string;
  sending_limit: string;
  reply_rate: string;
  reading_time: string;
  mailBox: string;
  ramp_up: string;
  max_limit: string;
  templates: string[];
}

interface IMailingSave {
  id?: number | string;
  sending_limit: number;
  reply_rate: number;
  reading_time: number;
  ramp_up: number;
  max_limit: number;
  templates: string[];
}
interface IMailingEdit {
  id: number | string;
  sending_limit: number;
  reply_rate: number;
  reading_time: number;
  ramp_up: number;
  max_limit: number;
  templates: string[];
}

const mails = () => {
  return httpClient.get<IMailingConfig[]>(api.domain_checker.fetch.get);
};

export const useFetchMails = () => {
  return useQuery(api.domain_checker.fetch.get, mails, {
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

const mailById = (id: string) => () => {
  return httpClient.get<IMailingConfig>(
    api.domain_checker.fetch.getById.replace("{mailId}", id)
  );
};

const editMailConfig = (mailDetail: IMailingEdit) => {
  return httpClient.patch(
    api.domain_checker.put.replace("{mailId}", mailDetail.id.toString()),
    mailDetail
  );
};

const dominChecker = (code: string) => () => {
  return httpClient.get<any>(api.domain_checker.fetch.domain, {
    params: { domain: code }
  });
};

const createMailing = (mailingDetail: IMailingSave) => {
  return httpClient.post<IMailingSave>(api.domain_checker.post, mailingDetail);
};

export const useDomainChecker = (code: string) => {
  return useQuery(
    [api.domain_checker.fetch.domain, code],
    dominChecker(code || ""),
    {
      enabled: !!code,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const useCreateMailing = () => {
  const queryClient = useQueryClient();
  return useMutation(createMailing, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.domain_checker.fetch.get);
      toastSuccess("Sucessfully Added");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useEditMailConfig = () => {
  const queryClient = useQueryClient();
  return useMutation(editMailConfig, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.domain_checker.fetch.get);
      toastSuccess("Successfully Updated");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const useMailById = (mailId: string) => {
  return useQuery(
    [api.domain_checker.fetch.getById, mailId],
    mailById(mailId || ""),
    {
      enabled: !!mailId,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};
