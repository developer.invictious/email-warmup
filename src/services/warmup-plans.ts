import { AxiosError } from "axios";
import { useQuery } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";

const getProduct = () => {
  return httpClient.get<[]>(api.plans.getProduct);
};

const getPrice = () => {
  return httpClient.get<[]>(api.plans.getPrice);
};

export const useGetProduct = () => {
  return useQuery(api.plans.getProduct, getProduct, {
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};

export const useGetPrice = () => {
  return useQuery(api.plans.getPrice, getPrice, {
    select: ({ data }) => data,
    onError: (error: AxiosError) => {
      console.log(error);
    }
  });
};
