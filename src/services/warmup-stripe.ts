import { toastFail } from "@sms/components/Toast";
import { AxiosError } from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { api } from "./service-api";
import { httpClient } from "./service-axios";
import { toastSuccess } from "./service-toast";
interface IStripe {
  number: string;
  exp_month: string;
  exp_year: string;
  cvc: string;
}
interface Itoken {
  id: string;
}

interface ICard {
  id: string;
  token: string;
}

interface IPlan {
  amount: string;
  currency: string;
  description: string;
  id: string;
}
export interface IUser {
  id: number | string;
  email: string;
  firstName: string;
  lastName: string;
  roles: string[];
  createdAt: string;
  result: Record<string, any>[];
  pages: number;
}

const getToken = ({ number, exp_month, exp_year, cvc }: IStripe) => {
  return httpClient.get<Itoken>(api.stripe.getToken, {
    params: { number, exp_month, exp_year, cvc }
  });
};

const fetchUserById = (id: string) => {
  return httpClient.get<IUser>(
    api.user_management.fetch.userById.replace("{userId}", id)
  );
};

const fetchUserById2 = (id: string) => () => {
  return httpClient.get<IUser>(
    api.user_management.fetch.userById.replace("{userId}", id)
  );
};

export const getLoggedDetail2 = (id: string) => {
  return useQuery(
    [api.user_management.fetch.userById, id],
    fetchUserById2(id || ""),
    {
      enabled: !!id,
      select: ({ data }) => data,
      onError: (error: AxiosError) => {
        console.log(error);
      }
    }
  );
};

export const getLoggedDetail = () => {
  const queryClient = useQueryClient();
  return useMutation(fetchUserById, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.user_management.fetch.userById);
      //   toastSuccess("Successfully toggled");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

// export const useGetToken = ({ number, exp_month, exp_year, cvc }: IStripe) => {
//   return useQuery(
//     [api.stripe.getToken],
//     getToken({ number, exp_month, exp_year, cvc } || ""),
//     {
//       enabled: !!number,
//       select: ({ data }) => data,
//       onError: (error: AxiosError) => {
//         console.log(error);
//       }
//     }
//   );
// };

export const useGetToken2 = () => {
  const queryClient = useQueryClient();
  return useMutation(getToken, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.stripe.getToken);
      //   toastSuccess("Successfully toggled");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

const addCard = (cardDetail: ICard) => {
  return httpClient.post<ICard>(api.stripe.addCard, cardDetail);
};

const payPlan = (planDetails: IPlan) => {
  const { amount, currency, description, id } = planDetails;
  return httpClient.post<IPlan>(
    api.stripe.payPlan,
    { amount, currency, description },
    {
      params: { id }
    }
  );
};

export const useAddCard = () => {
  const queryClient = useQueryClient();
  return useMutation(addCard, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.stripe.addCard);
      toastSuccess("Sucessfully Added Card in Stripe");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};

export const usePayPlan = () => {
  const queryClient = useQueryClient();
  return useMutation(payPlan, {
    onSuccess: () => {
      queryClient.invalidateQueries(api.stripe.payPlan);
      toastSuccess("Payment Successful");
    },
    onError: (error: AxiosError<{ message: string }>) => {
      toastFail(error.response?.data.message || "");
    }
  });
};
