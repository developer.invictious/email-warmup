export const api = {
  //for email warmup
  role: {
    fetch: {
      roles: "/api/role",
      role: "/api/role/{id}"
    }
  },

  user: {
    fetch: {
      users: "/api/user",
      user: "/api/user/{userId}",
      adminUsers: "/api/admin/{superAdminId}",
      userProfile: "/api/user/userProfile",
      userByMerchantId: "/api/user/userByMerchantId/{id}",

      getUserByStatus: "/api/user/getByStatus/{status}"
    },
    post: {
      user: "/api/user"
    },
    put: {
      toggleUser: "/api/user/toggle/{id}",
      user: "/api/user/updateUser"
    }
  },

  login: "/api/auth/login",
  register: "/api/auth/register",
  dashboard: {
    fetch: {
      dashboard: "/api/dashboard"
    }
  },
  user_management: {
    fetch: {
      users: "/api/user",
      userById: "/api/user/{userId}",
      roles: "/api/role"
    },
    post: { user: "/api/user" },
    put: { user: "/api/user/{userId}" },
    delete: { user: "/api/user/{userId}" }
  },
  email_template: {
    fetch: {
      temp: "/api/emailTemplate",
      tempById: "/api/emailTemplate/{templateId}"
    },
    post: { temp: "/api/emailTemplate" },
    put: { temp: "/api/emailTemplate/{templateId}" },
    delete: { temp: "/api/emailTemplate/{templateId}" },
    language: "/api/language"
  },
  mailbox: {
    form: { google: "/api/auth/google", yahoo: "/api/auth/yahoo" },
    fetch: {
      get: "/api/mailBox",
      getById: "/api/mailBox/{mailboxId}",
      mailbox: "/api/mailBox/getAllMails",
      mailboxById: "/api/mailBox/{mailboxId}/emails"
    },
    post: { post: "/api/mailBox", smtp: "/api/smtpimap" },
    put: { put: "/api/mailBox/{mailboxId}", smtp: "/api/smtpimap/{smtpId}" },
    token: {
      google: "/api/auth/google/token",
      yahoo: "/api/auth/yahoo/token"
    },
    delete: {
      mailbox: "/api/mailBox/{mailboxId}",
      smtp: "/api/smtpimap/{smtpId}"
    },
    toggle: "/api/mailBox/toggle/{toggleId}"
  },
  domain_checker: {
    fetch: {
      domain: "/api/domain/check",
      getById: "/api/mailBox/{mailId}/config",
      get: "/api/mailConfig"
    },
    put: "/api/mailConfig/{mailId}",
    post: "/api/mailConfig"
  },
  stats: {
    fetch: "/api/stats/{statsId}",
    fetchById: "/api/stats/monthly/{statsId}/80",
    fetchByDate: "/api/stats/detail/{statsId}",
    getPaymentDetail: "/api/stripe/payments"
  },
  stripe: {
    getToken: "/api/stripe/getToken",
    addCard: "/api/stripe/addCard",
    payPlan: "/api/stripe/pay"
  },
  plans: {
    getProduct: "/api/plan/products",
    getPrice: "/api/plan"
  }
};

export interface SMSResponse<T = any> {
  data: T;
  status: 0 | 1;
  message: string;
}
