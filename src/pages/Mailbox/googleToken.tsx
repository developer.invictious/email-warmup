import { Skeleton, Stack } from "@mui/material";
import { Box } from "@mui/system";
import { useGoogleToken } from "@sms/services/warmup-email";
import { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

const GoogleToken = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();

  const { data: googleToken, isLoading: gloading } = useGoogleToken(
    searchParams.get("code") ?? ""
  );

  useEffect(() => {
    if (googleToken) navigate(`/dashboard?mailbox=${googleToken.email}`);
  }, [googleToken]);
  return (
    <Box sx={{ paddingTop: "40px" }}>
      <Stack spacing={3}>
        <Skeleton
          variant="rectangular"
          height={400}
          sx={{ bgcolor: "white" }}
        />
      </Stack>
    </Box>
  );
};

export default GoogleToken;
