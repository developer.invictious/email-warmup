import { Box, Skeleton, Stack } from "@mui/material";
import { useYahooToken } from "@sms/services/warmup-email";
import { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

const YahooToken = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();

  const { data: yahooToken, isLoading: yloading } = useYahooToken(
    searchParams.get("code") ?? ""
  );

  useEffect(() => {
    if (yahooToken) navigate(`/dashboard?mailbox=${yahooToken.email}`);
  }, [yahooToken]);
  return (
    <Box sx={{ paddingTop: "40px" }}>
      <Stack spacing={3}>
        <Skeleton
          variant="rectangular"
          height={400}
          sx={{ bgcolor: "white" }}
        />
      </Stack>
    </Box>
  );
};

export default YahooToken;
