import { Divider, Grid, Link } from "@mui/material";
import { user } from "@sms/hooks/useIsFirstLogin";
import { useHandleGoogle, useHandleYahoo } from "@sms/services/warmup-email";
import { useEffect, useState } from "react";
import Joyride, { CallBackProps, STATUS } from "react-joyride";
import { useNavigate } from "react-router-dom";
import {
  CustomBox,
  CustomHeaderTitle,
  CustomTypography
} from "./mailbox.style";

interface IMailboxProps {
  onClick?: () => void;
}

const Mailbox = (props: IMailboxProps) => {
  const navigate = useNavigate();
  const [run, setRun] = useState(false);
  const { data: google } = useHandleGoogle();
  const { data: yahoo } = useHandleYahoo();

  //react-joyride code
  useEffect(() => {
    setRun(true);
  }, []);

  const handleJoyrideCallback = (data: CallBackProps) => {
    const { status, type } = data;
    const finishedStatuses: string[] = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      setRun(false);
    }
  };

  const steps: any = [
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Mailbox Type
          <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          Here you can select required mailbox type
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "top",
      target: "#second",
      disableBeacon: true
    }
  ];

  return (
    <>
      {user.isFirstTime <= 1 ? (
        <>
          <Joyride
            callback={handleJoyrideCallback}
            // continuous={true}
            // getHelpers={getHelpers}
            disableScrolling={true}
            run={run}
            showProgress={true}
            showSkipButton={true}
            steps={steps}
            styles={{
              options: {
                zIndex: 10000
              },
              buttonNext: {
                background: "#4F539E",
                color: "#fff",
                border: "none !important"
              }
            }}
          />
        </>
      ) : (
        <></>
      )}

      <CustomHeaderTitle>Select Mailbox Type</CustomHeaderTitle>
      <Grid container columnSpacing={5} rowSpacing={5} id="second">
        <Grid item md={6} xs={6}>
          <Link
            onClick={() => {
              navigate("/" + "/" + google?.data.replace(/(^\w+:|^)\/\//, ""));
            }}
            sx={{
              textDecoration: "none",
              cursor: "pointer",
              height: "210px"
            }}
          >
            <CustomBox
              sx={{
                "&:hover": {
                  border: "none",
                  boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;"
                }
              }}
            >
              <img src="/mailbox/gmail.svg" />

              <CustomTypography>Google Configuration</CustomTypography>
            </CustomBox>
          </Link>
        </Grid>

        <Grid item md={6} xs={6}>
          <Link
            onClick={() => {
              navigate("/" + "/" + yahoo?.data.replace(/(^\w+:|^)\/\//, ""));
            }}
            sx={{
              textDecoration: "none",
              cursor: "pointer",
              height: "210px"
            }}
          >
            <CustomBox
              sx={{
                "&:hover": {
                  border: "none",
                  boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;"
                }
              }}
            >
              <img src="/mailbox/yahoo.svg" />

              <CustomTypography>Yahoo Configuration</CustomTypography>
            </CustomBox>
          </Link>
        </Grid>

        <Grid item md={6} xs={6}>
          <Link
            // onClick={}
            sx={{
              textDecoration: "none",
              cursor: "pointer",
              height: "210px"
            }}
          >
            <CustomBox
              sx={{
                "&:hover": {
                  border: "none",
                  boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;"
                }
              }}
            >
              <img src="/mailbox/office365.svg" />

              <CustomTypography>Office 365 Configuration</CustomTypography>
              {/* </Button> */}
            </CustomBox>
          </Link>
        </Grid>

        <Grid item md={6} xs={6}>
          <Link
            onClick={props.onClick}
            // onClick={handleNext}
            sx={{
              textDecoration: "none",
              cursor: "pointer",
              height: "210px"
            }}
          >
            <CustomBox
              sx={{
                "&:hover": {
                  border: "none",
                  boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px "
                }
              }}
            >
              <img src="/mailbox/smtp.svg" />

              <CustomTypography>SMTP/IMAP Configuration</CustomTypography>
            </CustomBox>
          </Link>
        </Grid>
      </Grid>
    </>
  );
};

export default Mailbox;
