import DoneIcon from "@mui/icons-material/Done";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Link,
  Typography
} from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Mailbox from "./Mailbox";
import { Stepper, StepperWrapper } from "./mailbox.style";
import MailConfigForm from "./MailConfigForm";
import SmtpForm from "./SmtpForm";
import Success from "./Success";

interface IMailboxProps {
  open: boolean;
  handleClose: () => void;
  handlePopUp: (popUp: boolean) => void;
  //   handlePopUp: () => void;
}
const steps = [
  "Mailbox Type",
  "Authorization and Details",
  "Mail Configuration",
  "Finish"
];

const MailboxPop = (props: IMailboxProps) => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const [activeStep, setActiveStep] = useState(0);

  useEffect(() => {
    if (searchParams.get("mailbox")) {
      setActiveStep(2);
      props.handlePopUp(true);
    }
  }, [searchParams.get("mailbox")]);

  return (
    <Dialog
      maxWidth="lg"
      open={props.open}
      onClose={props.handleClose}
      //   scroll="body"
      sx={{
        ".MuiPaper-root": {
          borderRadius: "12px",
          width: "100%"
        }
      }}
    >
      <DialogTitle
        display="flex"
        sx={{
          background: "#E6E7F1",
          justifyContent: "space-between",
          alignItems: "center",
          py: 3,
          px: 10
        }}
      >
        <Typography sx={{ fontSize: 18, fontWeight: 600, color: "#4D4D4D;" }}>
          Mailbox Configuration
        </Typography>

        <Link
          onClick={() => {
            navigate("/dashboard");
            setActiveStep(0);
            props.handleClose();
          }}
          sx={{
            textDecoration: "none",
            cursor: "pointer",
            fontSize: 16,
            fontWeight: 600
          }}
        >
          X
        </Link>
      </DialogTitle>
      <DialogContent
        sx={{
          mt: 7,
          "&::-webkit-scrollbar": {
            width: "7px"
          },
          "&::-webkit-scrollbar-track": {
            background: "#f1f1f1"
          },

          /* Handle */
          "&::-webkit-scrollbar-thumb": {
            background: "#888"
          },

          /* Handle on hover */
          "&::-webkit-scrollbar-thumb:hover": {
            background: "#F1F1F1"
          }
        }}
      >
        <ul
          style={{
            display: "flex",
            justifyContent: "space-between",
            padding: "0 60px"
          }}
        >
          {steps.map((item, index) => {
            return (
              <StepperWrapper key={index}>
                <Stepper index={index} activeStep={activeStep}>
                  {activeStep > index || activeStep == 3 ? (
                    <DoneIcon />
                  ) : (
                    index + 1
                  )}
                </Stepper>
                <span style={{ fontFamily: '"Montserrat","Arial",sans-serif' }}>
                  {item}
                </span>
              </StepperWrapper>
            );
          })}
        </ul>
        <Box sx={{ paddingTop: "60px", px: 8 }}>
          {activeStep === 0 ? (
            <Mailbox
              onClick={() => {
                setActiveStep(1);
              }}
            />
          ) : activeStep === 1 ? (
            <SmtpForm
              onSubmit={() => {
                setActiveStep(2);
              }}
            />
          ) : activeStep === 2 ? (
            <MailConfigForm
              // mailbox={searchParams.get("mailbox")}
              onSubmit={() => {
                setActiveStep(3);
              }}
            />
          ) : activeStep === 3 ? (
            <Success
              onClick={() => {
                props.handleClose();
                setActiveStep(0);
              }}
            />
          ) : null}
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default MailboxPop;
