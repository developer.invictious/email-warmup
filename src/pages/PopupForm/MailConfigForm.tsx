import { yupResolver } from "@hookform/resolvers/yup";
import InfoIcon from "@mui/icons-material/Info";
import {
  Box,
  Divider,
  Grid,
  Paper,
  Stack,
  Table,
  TableBody,
  TableContainer,
  Tooltip
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { user } from "@sms/hooks/useIsFirstLogin";
import {
  useCreateMailing,
  useDomainChecker
} from "@sms/services/warmup-domain-checkup";
import { useFetchAllTemps } from "@sms/services/warmup-template";
import moment from "moment";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Joyride, { CallBackProps, STATUS } from "react-joyride";
import { useSearchParams } from "react-router-dom";
import * as Yup from "yup";
import {
  CustomCard,
  CustomHeaderTitle,
  CustomPrimaryButton,
  CustomSecondaryButton,
  CustomSelect,
  CustomTextField,
  CustomTypography,
  StyledTableCell,
  StyledTableRow
} from "./mailbox.style";

const CustomInfoIcon = styled(InfoIcon)({
  fontSize: "24px",
  marginLeft: "5px",
  color: "#E6E7F1",
  "&:hover": {
    cursor: "pointer"
  }
});

interface IMailingProps {
  onSubmit?: () => void;
  //   mailbox: string;
}
const defaultValues = {
  id: "",
  send_limit: "",
  reading_time: "",
  mailBox: "",
  templates: [],
  ramp_up: "",
  reply_rate: "",
  max_limit: ""
};

const MailConfigForm = (props: IMailingProps) => {
  const [run, setRun] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const { mutateAsync: addMailing, isLoading } = useCreateMailing();
  const schema = Yup.object().shape({
    send_limit: Yup.string().required("Sending limit is required"),
    reading_time: Yup.string().required("Reading time is required"),
    ramp_up: Yup.string().required("Ramp_up percentage is required"),
    max_limit: Yup.string().required("Maximum limit is required"),
    reply_rate: Yup.string().required("Replay rate is required"),
    templates: Yup.array().required("Template is required")
  });

  const { control, handleSubmit, reset } = useForm({
    defaultValues: defaultValues,
    resolver: yupResolver(schema)
  });

  const domain = searchParams.get("mailbox")?.split("@")[1];
  const { data: tempList = { result: [], pages: 1 }, isFetching } =
    useFetchAllTemps({ page: 1, limit: 1000 } ?? "");
  const onMailConfigHandler = async (mailingDetails: typeof defaultValues) => {
    const requestBody = {
      sending_limit: parseFloat(mailingDetails.send_limit),
      reply_rate: parseFloat(mailingDetails.reply_rate),
      reading_time: parseFloat(mailingDetails.reading_time),
      mailBox: searchParams.get("mailbox"),
      ramp_up: parseFloat(mailingDetails.ramp_up),
      max_limit: parseFloat(mailingDetails.max_limit),
      templates: mailingDetails.templates
    };

    try {
      await addMailing(requestBody);
      if (props.onSubmit) props.onSubmit();
    } catch (e) {
      console.log(e);
    }
  };

  const { data: domainData, isLoading: domainLoading } = useDomainChecker(
    domain ?? ""
  );

  //convert total days to years, months and days
  const start = moment();
  const end = moment().add(
    moment().diff(domainData?.createdAt, "days"),
    "days"
  );
  const years = end.diff(start, "years");
  start.add(years, "years");
  const months = end.diff(start, "months");
  start.add(months, "months");
  const days = end.diff(start, "days");

  //react-joyride code
  useEffect(() => {
    setRun(true);
  }, []);

  const handleJoyrideCallback = (data: CallBackProps) => {
    const { status, type } = data;
    const finishedStatuses: string[] = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      setRun(false);
    }
  };

  const steps: any = [
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Domain Details <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          Shows the details of the domain
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "bottom",
      target: "#fifth",
      disableBeacon: true
    },
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Mail Sending Configuration <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          Enter the details of the mail sending configuration
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "top",
      target: "#sixth",
      disableBeacon: true
    },
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Save Settings <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          You can save the mail sending configuration
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "top",
      target: "#seventh"
      //   disableBeacon: true
    }
  ];

  return (
    <>
      {user.isFirstTime <= 1 ? (
        <>
          <Joyride
            callback={handleJoyrideCallback}
            continuous={true}
            // getHelpers={getHelpers}
            // disableScrolling={true}
            run={run}
            showProgress={true}
            showSkipButton={true}
            steps={steps}
            styles={{
              options: {
                zIndex: 10000
              },
              buttonNext: {
                background: "#4F539E",
                color: "#fff",
                border: "none !important"
              }
            }}
          />
        </>
      ) : (
        <></>
      )}

      <Box>
        <Grid container spacing={2} id="fifth">
          <Grid item xs={12} md={12}>
            <CustomCard>
              <CustomHeaderTitle>Domain Detail</CustomHeaderTitle>
              <TableContainer
                component={Paper}
                sx={{
                  marginTop: "24px",
                  boxShadow: "none"
                }}
              >
                <Table aria-label="customized table">
                  <TableBody>
                    <StyledTableRow>
                      <StyledTableCell align="left">Domain Url</StyledTableCell>
                      <StyledTableCell align="left">
                        {domainData?.name}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow>
                      <StyledTableCell align="left">
                        Domain created Date
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        {moment(domainData?.createdAt).format("DD MMM, YYYY")}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow
                      sx={{
                        ".MuiTableCell-root": {
                          "&:first-child": { borderTopLeftRadius: 16 },
                          "&:last-child": { borderTopRightRadius: 16 }
                        }
                      }}
                    >
                      <StyledTableCell align="left">Age</StyledTableCell>
                      <StyledTableCell align="left">
                        {/* {domainData?.age} */}
                        {`${years} years, ${months} months and ${days} days`}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow>
                      <StyledTableCell align="left">
                        Domain Expiry date
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        {moment(domainData?.expiresAt).format("DD MMM, YYYY")}
                      </StyledTableCell>
                    </StyledTableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </CustomCard>
          </Grid>
        </Grid>
        {/* for mailconfig form */}
        <CustomCard sx={{ marginTop: "26px" }}>
          <CustomHeaderTitle>Mailing configuration</CustomHeaderTitle>

          <form onSubmit={handleSubmit(onMailConfigHandler)}>
            <Grid container columnSpacing={4} rowSpacing={2} id="sixth">
              <Grid item xs={12} md={6}>
                <CustomTypography
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  Set Sending Limit{" "}
                  <Tooltip title="Sending Limit" placement="top" arrow>
                    <CustomInfoIcon />
                  </Tooltip>
                </CustomTypography>
                <CustomTextField
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="send_limit"
                  placeholder="No. of mails per day"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <CustomTypography
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  Set Reading Time{" "}
                  <Tooltip title="Reading Time" placement="top" arrow>
                    <CustomInfoIcon />
                  </Tooltip>
                </CustomTypography>
                <CustomTextField
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="reading_time"
                  placeholder="Duration to read email"
                  variant="outlined"
                />
              </Grid>
              {/* <Grid item xs={12} md={6}>
              <CustomTypography>Mail Box</CustomTypography>
              <CustomSelect
                multiple={false}
                name="mail_box"
                options={mailboxList.map((x: any) => {
                  return {
                    label: x.email,
                    value: x.id
                  };
                })}
                control={control}
                size="small"
              />
            </Grid> */}
              {/* <Grid item xs={12} md={6}>
              <CustomTypography>Custom Template </CustomTypography>
              <CustomSelect
                multiple={false}
                name="custom_template"
                options={[{ label: "Template", value: "template" }]}
                control={control}
              />
            </Grid> */}
              <Grid item xs={12} md={6}>
                <CustomTypography
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  Set Reply Rate{" "}
                  <Tooltip title="Reply Rate" placement="top" arrow>
                    <CustomInfoIcon />
                  </Tooltip>
                </CustomTypography>
                <CustomTextField
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="reply_rate"
                  placeholder="Reply Rate"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <CustomTypography
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  Set Ramp-up Percentage{" "}
                  <Tooltip title="Ramp-up Percentage" placement="top" arrow>
                    <CustomInfoIcon />
                  </Tooltip>
                </CustomTypography>
                <CustomTextField
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="ramp_up"
                  placeholder="Rampup percentage"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <CustomTypography
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  Set Maximum Limit{" "}
                  <Tooltip title="Maximum Limit" placement="top" arrow>
                    <CustomInfoIcon />
                  </Tooltip>
                </CustomTypography>
                <CustomTextField
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="max_limit"
                  placeholder="Maximum limit"
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid container columnSpacing={4}>
              <Grid item xs={4} md={6}>
                <CustomHeaderTitle sx={{ mt: "20px", mb: "15px" }}>
                  Email Template
                </CustomHeaderTitle>
                <CustomTypography
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  Templates{" "}
                  <Tooltip title="Templates" placement="top" arrow>
                    <CustomInfoIcon />
                  </Tooltip>
                </CustomTypography>
                <CustomSelect
                  searchable={true}
                  multiple={true}
                  name="templates"
                  options={tempList.result.map((x: any) => {
                    return {
                      label: x.title,
                      value: x.id
                    };
                  })}
                  control={control}
                />
              </Grid>
            </Grid>
            <Grid item xs={12} md={12}>
              <Stack
                flexDirection={"row"}
                justifyContent="space-between"
                sx={{ mt: 4, mb: 2 }}
              >
                <CustomSecondaryButton onClick={(_) => reset(defaultValues)}>
                  Discard
                </CustomSecondaryButton>
                <CustomPrimaryButton
                  type="submit"
                  loading={isLoading}
                  id="seventh"
                >
                  Save Setting
                </CustomPrimaryButton>
              </Stack>
            </Grid>
          </form>
        </CustomCard>
      </Box>
    </>
  );
};

export default MailConfigForm;
