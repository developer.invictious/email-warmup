import { yupResolver } from "@hookform/resolvers/yup";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import VisibilityOffOutlinedIcon from "@mui/icons-material/VisibilityOffOutlined";
import {
  Checkbox,
  Divider,
  Grid,
  IconButton,
  InputAdornment,
  Stack,
  Typography
} from "@mui/material";
import { user } from "@sms/hooks/useIsFirstLogin";
import { useCreateMailbox } from "@sms/services/warmup-email";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Joyride, { CallBackProps, STATUS } from "react-joyride";
import { useSearchParams } from "react-router-dom";
import * as Yup from "yup";
import {
  CustomCard,
  CustomFormLabel,
  CustomHeaderTitle,
  CustomPrimaryButton,
  CustomSecondaryButton,
  CustomTextField,
  CustomTypography
} from "./mailbox.style";

interface ISmtpProps {
  onSubmit?: () => void;
}

const defaultValues = {
  server: "",
  name: "",
  smtp_host: "",
  imap_host: "",
  password: "",
  smtp_port: "",
  imap_port: "",
  email: "",
  encryption: ""
};

const SmtpForm = (props: ISmtpProps) => {
  const [showPassword, setShowPassword] = useState(false);
  const [run, setRun] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const schema = Yup.object().shape({
    server: Yup.string().required("Provider name is Required"),
    name: Yup.string().required("Name is required"),
    smtp_host: Yup.string().required("SMTP host is required"),
    imap_host: Yup.string().required("IMAP host is required"),
    password: Yup.string()
      .min(6, "Password should be at least six character")
      .required("Enter password"),
    smtp_port: Yup.string().required("SMTP port is required"),
    imap_port: Yup.string().required("IMAP port is required"),
    email: Yup.string().required("Email is required"),
    encryption: Yup.string()
  });

  const handleCheck = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked === false) {
      setValue("encryption", "unencrypted");
    } else {
      setValue("encryption", "SSL/TLS");
    }
  };

  const { mutateAsync: addMailbox, isLoading } = useCreateMailbox();

  const onSmtpHandler = async (smtpDetails: typeof defaultValues) => {
    const requestBody = {
      email: smtpDetails.email,
      name: smtpDetails.name,
      server: smtpDetails.server,
      smtp_host: smtpDetails.smtp_host,
      imap_host: smtpDetails.imap_host,
      imap_port: parseInt(smtpDetails.imap_port),
      smtp_port: parseInt(smtpDetails.smtp_port),
      password: smtpDetails.password
    };
    try {
      const data = await addMailbox({
        ...requestBody,
        encryption:
          smtpDetails.encryption === "" ? "unencrypted" : smtpDetails.encryption
      });
      setSearchParams({
        mailbox: data.data.email
      });
      if (props.onSubmit) props.onSubmit();
    } catch (e) {
      console.log(e);
    }
  };

  //react-joyride code
  useEffect(() => {
    setRun(true);
  }, []);

  const handleJoyrideCallback = (data: CallBackProps) => {
    const { status, type } = data;
    const finishedStatuses: string[] = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      setRun(false);
    }
  };

  const steps: any = [
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Mailing Configuration <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          Enter the details of the mailing configuration
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "top",
      target: "#third",
      disableBeacon: true
    },
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Save Setting <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          You can save the mailing configuration setting
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "top",
      target: "#forth"
      //   disableBeacon: true
    }
  ];

  const { control, handleSubmit, reset, setValue } = useForm({
    defaultValues: defaultValues,
    resolver: yupResolver(schema)
  });

  return (
    <CustomCard>
      {user.isFirstTime <= 1 ? (
        <>
          <Joyride
            callback={handleJoyrideCallback}
            continuous={true}
            // getHelpers={getHelpers}
            // disableScrolling={true}
            run={run}
            showProgress={true}
            showSkipButton={true}
            steps={steps}
            styles={{
              options: {
                zIndex: 10000
              },
              buttonNext: {
                background: "#4F539E",
                color: "#fff",
                border: "none !important"
              }
            }}
          />
        </>
      ) : (
        <></>
      )}
      <CustomHeaderTitle>Authorization and Details</CustomHeaderTitle>
      <Grid container>
        <form style={{ width: "100%" }} onSubmit={handleSubmit(onSmtpHandler)}>
          <Grid container columnSpacing={6} alignItems="center" id="third">
            <Grid item xs={12} md={6}>
              <CustomTypography>Email Adddress</CustomTypography>
              <CustomTextField
                control={control}
                name="email"
                type="email"
                placeholder="eg. something@gmail.com"
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <CustomTypography>Provider</CustomTypography>
              <CustomTextField
                control={control}
                name="server"
                type="test"
                placeholder="eg. gmail"
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Name</CustomTypography>
              <CustomTextField
                control={control}
                name="name"
                placeholder="eg. John"
                type="text"
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <CustomTypography>Password</CustomTypography>
              <CustomTextField
                control={control}
                name="password"
                placeholder="Password"
                type={showPassword ? "text" : "password"}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        size="small"
                        onClick={() => {
                          setShowPassword((prev) => !prev);
                        }}
                      >
                        {showPassword ? (
                          <VisibilityOffOutlinedIcon />
                        ) : (
                          <RemoveRedEyeOutlinedIcon />
                        )}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <Typography fontWeight="bold" sx={{ fontSize: 15, my: 2 }}>
                SMTP Server
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}></Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Host</CustomTypography>
              <CustomTextField
                control={control}
                name="smtp_host"
                placeholder="eg:smtp.xyz.com"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Port Number</CustomTypography>
              <CustomTextField
                control={control}
                name="smtp_port"
                placeholder="eg:25"
                type="number"
              />
            </Grid>
            {/* <Grid item xs={12} md={12}>
                    <CustomFormLabel
                      control={<Checkbox size="small" />}
                      label="Do you want to use SSL/TSL?"
                    />
                  </Grid> */}
            <Grid item xs={12} md={6}>
              <Typography fontWeight="bold" sx={{ fontSize: 15, my: 2 }}>
                IMAP Server
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}></Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Host</CustomTypography>
              <CustomTextField
                control={control}
                name="imap_host"
                placeholder="eg:imap.xyz.com"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Port Number</CustomTypography>
              <CustomTextField
                control={control}
                name="imap_port"
                placeholder="eg:143"
                type="number"
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <CustomFormLabel
                control={
                  <Checkbox onChange={handleCheck} defaultChecked={false} />
                }
                label="Do you want to use SSL/TLS?"
              />
            </Grid>
          </Grid>
          <Grid container columnSpacing={6} alignItems="center">
            <Grid item xs={12} md={12}>
              <Stack
                flexDirection={"row"}
                justifyContent="space-between"
                sx={{ mt: 3, mb: 2 }}
              >
                <CustomSecondaryButton onClick={(_) => reset(defaultValues)}>
                  Discard
                </CustomSecondaryButton>
                <CustomPrimaryButton
                  type="submit"
                  loading={isLoading}
                  id="forth"
                >
                  Save Setting
                </CustomPrimaryButton>
              </Stack>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </CustomCard>
  );
};

export default SmtpForm;
