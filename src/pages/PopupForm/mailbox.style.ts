import LoadingButton from "@mui/lab/LoadingButton";
import { Button, Card } from "@mui/material";
import FormControlLabel from "@mui/material/FormControlLabel";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import { styled } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/system";
import TextInput from "../../components/Form/Input";

import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import SelectComponent from "@sms/components/Form/Select";
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    border: "none"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    border: "none"
  }
}));
const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: "#E6E7F1"
  },

  "&:last-child td, &:last-child th": {
    border: 0
  }
}));

const CustomGrid = styled(Grid)({
  width: "100%",
  height: "100vh !important",
  position: "relative",
  "& .MuiGrid-container": {
    position: "absolute",
    top: "50%",
    width: "100%",
    left: "15%",
    transform: "translate(0,-50%)"
  }
});
const CustomLink = styled(Link)({
  position: "absolute",
  left: "68px",
  top: "52px",
  width: "185px"
});
const HeadingTypography = styled(Typography)({
  fontWeight: "bold",
  fontSize: "28px",
  color: "#4F539E"
});
const Stepper = styled("div")(
  ({ index, activeStep }: { index: number; activeStep: number }) => ({
    textAlign: "center",
    height: 48,
    width: 48,
    borderRadius: "50%",
    backgroundColor: activeStep >= index ? "#308D44" : "#30358D",
    lineHeight: "48px",
    color: "#fff",
    fontFamily: '"Montserrat","Arial",sans-serif',
    fontWeight: 600,
    fontSize: 14,
    margin: "8px auto",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  })
);
const StepperWrapper = styled("li")({
  position: "relative",
  "&::after": {
    position: "absolute",
    content: "''",
    width: "100%",
    height: 1,
    top: "35%",
    left: "100%",
    background: "#808080"
  },
  "&::before": {
    position: "absolute",
    content: "''",
    width: "100%",
    height: 1,
    top: "35%",
    right: "100%",
    background: "#808080"
  },
  "&:first-child": {
    "&::before": {
      display: "none"
    }
  },
  "&:nth-child(3)": {
    "&::after": {
      width: "125%"
    }
  },
  "&:last-child": {
    "&::before": {
      display: "none"
    },
    "&::after": {
      display: "none"
    }
  }
});
const CustomTypography = styled(Typography)({
  fontWeight: "text.secondary",
  color: "#6E6B7b",
  fontSize: "14px",
  marginBottom: 5,
  display: "block"
});

const CustomTextField = styled(TextInput)({
  width: "100%",
  paddingBottom: "15px",
  margin: 0,
  fontSize: "14px"
});
const CustomPrimaryButton = styled(LoadingButton)({
  background: "#4F539E;",
  borderRadius: "6px;",
  height: "42px",
  //   width: "185px",
  color: "#fff",
  padding: "12px 24px;",
  fontWeight: "600",
  textTransform: "none",

  "&:hover": {
    background: "rgb(98, 101, 162)"
  }
});
const CustomSecondaryButton = styled(Button)({
  background: "#fff;",
  border: "1px solid #024B8D",
  borderRadius: "6px;",
  height: "42px",
  //   width: "94px",
  color: "#024B8D",
  padding: "12px 24px;",
  fontWeight: "600",
  textTransform: "none"
});
const CustomFormLabel = styled(FormControlLabel)({
  "& span.MuiTypography-root": {
    fontWeight: "400",
    fontSize: "14px",
    color: "#6E6B7B"
  }
});
const CustomCard = styled(Card)({
  width: "100%",
  boxShadow: "none"
});

const CustomStatsCard = styled(Card)({
  boxShadow: "0px 10px 12px rgba(0, 0, 0, 0.06)",
  padding: "16px",
  flexDirection: "row",
  display: "flex",
  alignItems: "center"
});

const StatsTitleTypography = styled(Typography)({
  pl: 1,
  fontSize: 14,
  color: "#6D6D6D"
});

const CustomBox = styled(Box)({
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  flexDirection: "column",
  height: "210px",
  backgroundColor: "white",
  borderRadius: "4px",
  border: "1px solid #E6E7F1"
  //   margin: "33px"
});

const CustomHeaderTitle = styled(Typography)({
  marginBottom: 24,
  color: "#4F539E",
  fontSize: 18,
  fontWeight: "600"
});
const CustomTitle = styled(Typography)({
  fontWeight: "400",
  fontSize: "14px"
});

const CustomSelect = styled(SelectComponent)({
  borderColor: "none",
  "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
    border: "1px solid grey"
  },
  width: "100%"
});

export {
  CustomTitle,
  CustomBox,
  CustomSecondaryButton,
  CustomCard,
  CustomPrimaryButton,
  CustomGrid,
  CustomFormLabel,
  CustomTypography,
  CustomTextField,
  HeadingTypography,
  CustomLink,
  StyledTableCell,
  StyledTableRow,
  CustomHeaderTitle,
  Stepper,
  StepperWrapper,
  CustomSelect,
  StatsTitleTypography,
  CustomStatsCard
};
