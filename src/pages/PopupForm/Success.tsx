import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { Button, Divider, Typography } from "@mui/material";
import { user } from "@sms/hooks/useIsFirstLogin";
import { DigiHubColor } from "@sms/shared/colors";
import { useEffect, useState } from "react";
import Joyride, { CallBackProps, STATUS } from "react-joyride";
import { useNavigate } from "react-router-dom";

const Success = (props: ISuccess) => {
  const navigate = useNavigate();
  const [run, setRun] = useState(false);

  //react-joyride code
  useEffect(() => {
    setRun(true);
  }, []);

  const handleJoyrideCallback = (data: CallBackProps) => {
    const { status, type } = data;
    const finishedStatuses: string[] = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      setRun(false);
    }
  };

  const steps: any = [
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Success Message <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          Shown if the configuration is successful
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "top",
      target: "#eighth",
      disableBeacon: true
    }
  ];

  return (
    <div
      style={{ paddingTop: 70, paddingBottom: 145, textAlign: "center" }}
      id="eighth"
    >
      {user.isFirstTime <= 1 ? (
        <>
          <Joyride
            callback={handleJoyrideCallback}
            //   continuous={true}
            // getHelpers={getHelpers}
            disableScrolling={true}
            run={run}
            showProgress={true}
            showSkipButton={true}
            steps={steps}
            styles={{
              options: {
                zIndex: 10000
              },
              buttonNext: {
                background: "#4F539E",
                color: "#fff",
                border: "none !important"
              }
            }}
          />
        </>
      ) : (
        <></>
      )}
      <CheckCircleIcon
        sx={{ color: DigiHubColor.success, fontSize: 120, mb: 3 }}
      />
      <Typography sx={{ margin: 0, padding: 0, color: "#4D4D4D" }}>
        You have succesfully configured your mailbox.
      </Typography>
      <Button
        onClick={() => {
          navigate("/dashboard");
          props.onClick();
        }}
        variant="contained"
        size="large"
        sx={{ backgroundColor: DigiHubColor.success, marginTop: "28px" }}
      >
        Continue
      </Button>
    </div>
  );
};
interface ISuccess {
  onClick: () => void;
}
export default Success;
