import { yupResolver } from "@hookform/resolvers/yup";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import VisibilityOffOutlinedIcon from "@mui/icons-material/VisibilityOffOutlined";
import { IconButton, InputAdornment, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { useCreateRegister } from "@sms/services/warmup-register";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { ReactComponent as Login_pic } from "../../assets/login-page.svg";
import {
  CustomButton,
  CustomGrid,
  CustomTextField,
  CustomTypography,
  HeadingTypography
} from "../Login/customstyled";

const registerDefaultValues = {
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  cpassword: ""
};
const Register = () => {
  const navigate = useNavigate();
  const schema = Yup.object().shape({
    firstName: Yup.string().required("Firstname is required"),
    lastName: Yup.string().required("Lastname is required"),
    email: Yup.string().required("Email is required"),
    password: Yup.string()
      .min(6, "Password should be at least six character")
      .required("Enter password"),
    cpassword: Yup.string()
      .required("Confirm password is required")
      .min(6, "Password should be at least six character ")
      .oneOf([Yup.ref("password"), null], "Passwords must match")
  });
  const { mutateAsync: addRegister, isLoading } = useCreateRegister();
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const { control, handleSubmit } = useForm({
    defaultValues: registerDefaultValues,
    resolver: yupResolver(schema)
  });
  const onRegisterHandler = async (
    registerDetails: typeof registerDefaultValues
  ) => {
    const requestBody = {
      firstName: registerDetails.firstName,
      lastName: registerDetails.lastName,
      email: registerDetails.email,
      password: registerDetails.password,
      cpassword: registerDetails.cpassword
    };

    try {
      await addRegister(requestBody);
      navigate("/");
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <Box
      sx={{
        flexGrow: 1,
        backgroundColor: "white",
        overflow: "hidden"
      }}
    >
      <Grid container columnSpacing={2}>
        <Grid item xs={12} md={7.5} sx={{ backgroundColor: "#F4F3F3" }}>
          <Login_pic width={"100%"} height={"100%"}></Login_pic>
        </Grid>
        <CustomGrid
          item
          xs={12}
          md={3.5}
          sx={{ height: "100vh", backgroundColor: "white" }}
        >
          <Grid container rowSpacing={2}>
            <Box
              sx={{
                width: "388px",
                height: "67px",
                marginBottom: "28px"
              }}
            >
              <HeadingTypography>Register new account</HeadingTypography>
              <Typography
                sx={{
                  marginTop: "6px"
                }}
              >
                Please kindly fill up form *
              </Typography>
            </Box>
            <form
              style={{ width: "100%", margin: "auto" }}
              onSubmit={handleSubmit(onRegisterHandler)}
            >
              <Grid item xs={9} md={12}>
                <CustomTypography>First Name</CustomTypography>
                <CustomTextField
                  size="small"
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="firstName"
                  placeholder="eg. John"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={9} md={12}>
                <CustomTypography>Last Name</CustomTypography>
                <CustomTextField
                  size="small"
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="lastName"
                  placeholder="eg. Doe"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={9} md={12}>
                <CustomTypography>Email</CustomTypography>
                <CustomTextField
                  size="small"
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="email"
                  placeholder="eg. something@gmail.com"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={9} md={12}>
                <CustomTypography>Password</CustomTypography>
                <CustomTextField
                  control={control}
                  size="small"
                  name="password"
                  type={showPassword ? "text" : "password"}
                  placeholder="Password"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          size="small"
                          onClick={() => {
                            setShowPassword((prev) => !prev);
                          }}
                        >
                          {showPassword ? (
                            <VisibilityOffOutlinedIcon />
                          ) : (
                            <RemoveRedEyeOutlinedIcon />
                          )}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </Grid>
              <Grid item xs={9} md={12}>
                <CustomTypography>Confirm Password</CustomTypography>
                <CustomTextField
                  control={control}
                  size="small"
                  name="cpassword"
                  type={showConfirmPassword ? "text" : "password"}
                  placeholder="Confirm Password"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          size="small"
                          onClick={() => {
                            setShowConfirmPassword((prev) => !prev);
                          }}
                        >
                          {showConfirmPassword ? (
                            <VisibilityOffOutlinedIcon />
                          ) : (
                            <RemoveRedEyeOutlinedIcon />
                          )}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </Grid>
              <Grid item xs={9} md={12}>
                <CustomButton
                  loading={isLoading}
                  variant="contained"
                  type="submit"
                >
                  Create an Account
                </CustomButton>
              </Grid>
              <Grid
                item
                xs={8}
                md={12}
                sx={{
                  marginTop: "16px"
                }}
              >
                <CustomTypography
                  sx={{
                    textAlign: "center"
                  }}
                >
                  Already had an account?
                  <Link
                    style={{
                      fontWeight: "bold",
                      marginLeft: "10px"
                    }}
                    to="/"
                  >
                    Login
                  </Link>
                </CustomTypography>
              </Grid>
            </form>
          </Grid>
        </CustomGrid>
      </Grid>
    </Box>
  );
};
export default Register;
