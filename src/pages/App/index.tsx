import { Provider } from "@sms/components";
import AppRoutes from "./App.Routes";

function App() {
  return (
    <Provider>
      <AppRoutes />
    </Provider>
  );
}

export default App;
