import React, { useEffect } from "react";
import { Navigate, Route, Routes, useNavigate } from "react-router-dom";

import Loader from "@sms/components/Loader";
import pageRoutes from "@sms/route/PageRoutes";
import { useAuthentication, useLogoutMutation } from "@sms/services/sms-auth";
import ForgetPassword from "../ForgetPassword";
import LayoutWrapper from "../LayoutWrapper";

const Login = React.lazy(() => import("@sms/pages/Login"));
const UserList = React.lazy(() => import("@sms/pages/User/UserList"));
const UserDetail = React.lazy(() => import("@sms/pages/User/UserDetail"));
const User = React.lazy(() => import("@sms/pages/User/UserAdd"));
const Plans = React.lazy(() => import("@sms/pages/Plans/index"));
const Dashboard = React.lazy(() => import("@sms/pages/Dashboard/index"));
const EmailTemplate = React.lazy(
  () => import("../EmailTemplate/EmailTemplateForm")
);
const EmailTemplateList = React.lazy(
  () => import("../EmailTemplate/EmailTemplateList")
);
const Billing = React.lazy(() => import("../Billing/index"));
const EmailTemplateDetail = React.lazy(
  () => import("@sms/pages/EmailTemplate/EmailTemplateDetail")
);
const GoogleToken = React.lazy(() => import("@sms/pages/Mailbox/googleToken"));
const YahooToken = React.lazy(() => import("@sms/pages/Mailbox/yahooToken"));

const Register = React.lazy(() => import("@sms/pages/Register"));
const Report = React.lazy(() => import("@sms/pages/Report/index"));

const signInRoutes = [
  { path: pageRoutes.login, component: <Login /> },
  { path: pageRoutes.register, component: <Register /> },
  { path: pageRoutes.forgetPassword, component: <ForgetPassword /> }
];

const protectedRoutes = [
  //dashboard
  { path: pageRoutes.dashboard, component: <Dashboard /> },

  //emailTemplate
  { path: pageRoutes.add_email_template, component: <EmailTemplate /> },
  { path: pageRoutes.email_template, component: <EmailTemplateList /> },
  { path: pageRoutes.edit_email_template, component: <EmailTemplate /> },
  { path: pageRoutes.view_email_template, component: <EmailTemplateDetail /> },

  //user-management
  { path: pageRoutes.add_user, component: <User /> },
  { path: pageRoutes.user, component: <UserList /> },
  { path: pageRoutes.edit_user, component: <User /> },
  { path: pageRoutes.view_user, component: <UserDetail /> },

  //plans
  { path: pageRoutes.plans, component: <Plans /> },

  //google & yahoo
  { path: pageRoutes.google_token, component: <GoogleToken /> },
  { path: pageRoutes.yahoo_token, component: <YahooToken /> },

  //report
  { path: pageRoutes.report, component: <Report /> },

  //billing
  { path: pageRoutes.billing, component: <Billing /> }
];

function AppRoutes() {
  const navigate = useNavigate();

  const { data: isAuthenticated, isLoading } = useAuthentication();
  const { mutate: logoutUser } = useLogoutMutation();

  useEffect(() => {
    if (typeof isAuthenticated === "boolean" && !isAuthenticated) {
      logoutUser();
      navigate("/");
    }
  }, [isAuthenticated]);

  if (isLoading) {
    return <Loader />;
  }
  return (
    <Routes>
      <Route
        path={pageRoutes.login}
        element={
          isAuthenticated ? <Navigate to={pageRoutes.dashboard} /> : <Login />
        }
      />

      {signInRoutes.map((route) => (
        <Route key={route.path} path={route.path} element={route.component} />
      ))}

      {protectedRoutes.map((route) => (
        <Route
          key={route.path}
          path={route.path}
          element={
            isAuthenticated ? (
              <>
                <LayoutWrapper>{route.component}</LayoutWrapper>
              </>
            ) : (
              <Navigate to={pageRoutes.login} />
            )
          }
        />
      ))}
    </Routes>
  );
}
export default AppRoutes;
