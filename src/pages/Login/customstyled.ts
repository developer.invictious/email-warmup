import LoadingButton from "@mui/lab/LoadingButton";
import FormControlLabel from "@mui/material/FormControlLabel";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import { styled } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import TextInput from "@sms/components/Form/Input";

const CustomGrid = styled(Grid)({
  width: "100%",
  height: "100vh !important",
  position: "relative",
  "& .MuiGrid-container": {
    position: "absolute",
    top: "50%",
    width: "100%",
    left: "15%",
    transform: "translate(0,-50%)"
  }
});
const CustomLink = styled(Link)({
  position: "absolute",
  left: "68px",
  top: "52px",
  width: "185px"
});
const HeadingTypography = styled(Typography)({
  fontWeight: "bold",
  fontSize: "28px",
  lineHeight: "34.13px",
  color: "#000000"
});
const CustomTypography = styled(Typography)({
  fontWeight: "400",
  color: "#6E6B7b",
  fontSize: "14px",
  lineHeight: "17px",
  marginBottom: "8px"
});

const CustomTextField = styled(TextInput)({
  width: "100%",
  paddingBottom: "15px",
  margin: 0,
  fontSize: "14px"
});
const CustomButton = styled(LoadingButton)({
  width: "100%",
  margin: "5px 0",
  padding: 10,
  backgroundColor: "#024B8D",
  color: "#ffffff"
});
const CustomFormLabel = styled(FormControlLabel)({
  "& span.MuiTypography-root": {
    fontWeight: "400",
    fontSize: "14px",
    color: "#6E6B7B"
  }
});

export {
  CustomButton,
  CustomGrid,
  CustomFormLabel,
  CustomTypography,
  CustomTextField,
  HeadingTypography,
  CustomLink
};
