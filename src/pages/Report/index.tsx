import {
  Box,
  Card,
  Divider,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Typography
} from "@mui/material";
import { images } from "@sms/assets/images";
import { useFetchMailbox } from "@sms/services/warmup-email";
import { useFetchByDate } from "@sms/services/warmup-stats";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsExportData from "highcharts/modules/export-data";
import HighchartsExporting from "highcharts/modules/exporting";
import { useState } from "react";
import { useParams } from "react-router-dom";
import {
  CustomStatsCard,
  StatsTitleTypography
} from "../PopupForm/mailbox.style";

const Report = () => {
  const { statsId } = useParams();
  const [type, setType] = useState("daily");
  const [value, setValue] = useState("7");

  //for filter
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const handleDropdown = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const openDrodown = Boolean(anchorEl);
  const filterId = openDrodown ? "simple-popper" : undefined;
  const handleClose = () => {
    setAnchorEl(null);
  };

  const { data: mailboxList = { result: [], pages: 1 }, isFetching } =
    useFetchMailbox({ page: 1, limit: 5, status: "" } ?? "");

  const [id, setId] = useState(statsId || mailboxList.result[0].id);

  const { data: statsList = {}, isFetching: statsFetching } = useFetchByDate(
    { type, value, id } ?? ""
  );

  const data = Object.values(statsList);
  const mail_send: any = [];
  const mail_replied: any = [];
  const mail_opened: any = [];
  const mail_spam: any = [];
  data.forEach((x: any) => {
    mail_send.push(x.total_message_sent);
    mail_replied.push(x.total_message_replied);
    mail_opened.push(x.total_inbox_read);
    mail_spam.push(x.total_spam_converted);
  });
  if (typeof Highcharts === "object") {
    HighchartsExporting(Highcharts);
    HighchartsExportData(Highcharts);
  }

  const options = {
    chart: {
      type: "column"
    },
    title: {
      text: "Email Details"
    },

    xAxis: {
      categories: Object.keys(statsList),
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: "Value"
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
      footerFormat: "</table>",
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    legend: {
      enabled: true,
      align: "right",
      verticalAlign: "top"
    },
    credits: {
      enabled: false
    },
    exporting: {
      buttons: {
        contextButton: {
          menuItems: [
            "printChart",
            "separator",
            "downloadPNG",
            "downloadJPEG",
            "downloadPDF",
            "downloadSVG",
            "separator",
            "downloadCSV",
            "downloadXLS",
            // "viewData",
            "openInCloud"
          ]
        }
      }
    },
    series: [
      {
        name: "Total Email Sent",
        data: mail_send,
        color: "#4F539E"
      },
      {
        name: "Total Email Replied",
        data: mail_replied,
        color: "#C1C2DD"
      },
      {
        name: "Total Email Opened",
        data: mail_opened,
        color: "#B2DF8A"
      }
    ]
  };

  return (
    <Box sx={{ mt: 7, mb: 2, paddingX: 3 }}>
      <Typography fontSize={20} fontWeight={400} sx={{ pb: 8 }}>
        Report
      </Typography>

      <Grid container columnSpacing={3}>
        <Grid item md={3}>
          <CustomStatsCard>
            <img style={{ marginRight: 5 }} src={images.mailSent} />
            <StatsTitleTypography>Total Email Send</StatsTitleTypography>
            <Typography sx={{ marginLeft: "auto" }}>
              {mail_send.reduce((a: number, b: number) => a + b, 0)}
            </Typography>
          </CustomStatsCard>
        </Grid>
        <Grid item md={3}>
          <CustomStatsCard>
            <img style={{ marginRight: 5 }} src={images.incomingMail} />
            <StatsTitleTypography>Total Incoming Mail</StatsTitleTypography>
            <Typography sx={{ marginLeft: "auto" }}>
              {mail_opened.reduce((a: number, b: number) => a + b, 0)}
            </Typography>
          </CustomStatsCard>
        </Grid>
        <Grid item md={3}>
          <CustomStatsCard>
            <img style={{ marginRight: 5 }} src={images.replyMail} />
            <StatsTitleTypography>Total Email Replied</StatsTitleTypography>
            <Typography sx={{ marginLeft: "auto" }}>
              {mail_opened.reduce((a: number, b: number) => a + b, 0)}
            </Typography>
          </CustomStatsCard>
        </Grid>
        <Grid item md={3}>
          <CustomStatsCard>
            <img style={{ marginRight: 5 }} src={images.spamMail} />
            <StatsTitleTypography>Total Spam Mail</StatsTitleTypography>
            <Typography sx={{ marginLeft: "auto" }}>
              {mail_spam.reduce((a: number, b: number) => a + b, 0)}
            </Typography>
          </CustomStatsCard>
        </Grid>
      </Grid>
      <Divider sx={{ my: 5 }} />
      <Card sx={{ boxShadow: "none" }}>
        <IconButton
          onClick={handleDropdown}
          aria-describedby={filterId}
          sx={{
            backgroundColor: "#4F539E",
            margin: 2,
            paddingY: 1.5,
            borderRadius: "8px",
            "&:hover": {
              backgroundColor: "#4F539E"
            }
          }}
        >
          <img src={images.filterList} />
        </IconButton>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={openDrodown}
          onClose={() => setAnchorEl(null)}
          MenuListProps={{
            "aria-labelledby": "basic-button"
          }}
          sx={{ top: 20 }}
        >
          <MenuItem
            selected={type === "daily" && value === "3" ? true : false}
            sx={{
              fontSize: 15,
              p: "5px 30px",
              m: "10px",
              borderRadius: "12px",
              "&:hover": { background: "#4F539E", color: "#fff" },
              "&.Mui-selected": { background: "#4F539E", color: "#fff" }
            }}
            onClick={() => {
              setType("daily");
              setValue("3");
              handleClose();
            }}
          >
            Last Three Days
          </MenuItem>
          <MenuItem
            selected={type === "daily" && value === "7" ? true : false}
            sx={{
              fontSize: 15,
              p: "5px 30px",
              m: "10px",
              borderRadius: "12px",
              "&:hover": { background: "#4F539E", color: "#fff" },
              "&.Mui-selected": { background: "#4F539E", color: "#fff" }
            }}
            onClick={() => {
              setType("daily");
              setValue("7");
              handleClose();
            }}
          >
            Last Week
          </MenuItem>
          <MenuItem
            selected={type === "daily" && value === "30" ? true : false}
            sx={{
              fontSize: 15,
              p: "5px 30px",
              m: "10px",
              borderRadius: "12px",
              "&.Mui-selected": { background: "#4F539E", color: "#fff" },
              "&:hover": { background: "#4F539E", color: "#fff" }
            }}
            onClick={() => {
              setType("daily");
              setValue("30");
              handleClose();
            }}
          >
            Last Month
          </MenuItem>
          <MenuItem
            selected={type === "monthly" && value === "90" ? true : false}
            sx={{
              fontSize: 15,
              p: "5px 30px",
              m: "10px",
              borderRadius: "12px",
              "&:hover": { background: "#4F539E", color: "#fff" },
              "&.Mui-selected": { background: "#4F539E", color: "#fff" }
            }}
            onClick={() => {
              setType("monthly");
              setValue("90");
              handleClose();
            }}
          >
            Last Three Month
          </MenuItem>
          <MenuItem
            selected={type === "monthly" && value === "180" ? true : false}
            sx={{
              fontSize: 15,
              p: "5px 30px",
              m: "10px",
              borderRadius: "12px",
              "&:hover": { background: "#4F539E", color: "#fff" },
              "&.Mui-selected": { background: "#4F539E", color: "#fff" }
            }}
            onClick={() => {
              setType("monthly");
              setValue("180");
              handleClose();
            }}
          >
            Last Six Month
          </MenuItem>
        </Menu>
        <HighchartsReact highcharts={Highcharts} options={options} />
      </Card>
    </Box>
  );
};

export default Report;
