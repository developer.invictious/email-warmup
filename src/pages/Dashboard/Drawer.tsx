import { yupResolver } from "@hookform/resolvers/yup";
import InfoIcon from "@mui/icons-material/Info";
import { Divider, Grid, Stack, Tooltip, Typography } from "@mui/material";
import MuiDrawer from "@mui/material/Drawer";
import { styled } from "@mui/material/styles";
import { Box } from "@mui/system";
import { images } from "@sms/assets/images";
import {
  useEditMailConfig,
  useMailById
} from "@sms/services/warmup-domain-checkup";
import { useFetchStatsId } from "@sms/services/warmup-stats";
import { useFetchAllTemps } from "@sms/services/warmup-template";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import {
  CustomPrimaryButton,
  CustomSecondaryButton,
  CustomSelect,
  CustomStatsCard,
  CustomTextField,
  CustomTypography,
  StatsTitleTypography
} from "../PopupForm/mailbox.style";

const CustomInfoIcon = styled(InfoIcon)({
  fontSize: "24px",
  marginLeft: "5px",
  color: "#E6E7F1",
  "&:hover": {
    cursor: "pointer"
  }
});

const schema = Yup.object().shape({
  send_limit: Yup.string().required("Sending limit is required"),
  reading_time: Yup.string().required("Reading time is required"),
  templates: Yup.array().required("Template is required"),
  reply_rate: Yup.string().required("Replay rate is required"),
  ramp_up: Yup.string().required("Ramp_up percentage is required"),
  max_limit: Yup.string().required("Maximum limit is required")
});

const defaultValues = {
  id: "",
  send_limit: "",
  reading_time: "",
  templates: [],
  ramp_up: "",
  reply_rate: "",
  max_limit: ""
};

const Drawer = ({ open, handleClose, settingId }: IDrawer) => {
  const navigate = useNavigate();

  const { data: mailDetail } = useMailById(settingId ?? "");

  const { data: statsDetail } = useFetchStatsId(settingId ?? "");

  const { data: tempList = { result: [], pages: 1 }, isFetching } =
    useFetchAllTemps({ page: 1, limit: 1000 } ?? "");

  const { mutateAsync: updateMail, isLoading: isUpdatingMail } =
    useEditMailConfig();
  const { control, handleSubmit, reset, setValue } = useForm({
    defaultValues: defaultValues,
    resolver: yupResolver(schema)
  });

  const onSettingHandler = async (mailingDetails: typeof defaultValues) => {
    const requestBody = {
      sending_limit: parseFloat(mailingDetails.send_limit),
      reply_rate: parseFloat(mailingDetails.reply_rate),
      reading_time: parseFloat(mailingDetails.reading_time),
      ramp_up: parseFloat(mailingDetails.ramp_up),
      max_limit: parseFloat(mailingDetails.max_limit),
      templates: mailingDetails.templates
    };

    try {
      await updateMail({ ...requestBody, id: mailingDetails.id });
      handleClose();
      navigate("/dashboard");
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    if (mailDetail) {
      reset({
        ...defaultValues,
        id: mailDetail.id.toString(),
        send_limit: mailDetail.sending_limit,
        reading_time: mailDetail.reading_time,
        reply_rate: mailDetail.reply_rate,
        max_limit: mailDetail.max_limit,
        ramp_up: mailDetail.reply_rate,
        // mailBox: mailDetail.mailBox,
        templates: mailDetail.templates?.map((x: any) => x.id)
      });
    }
  }, [mailDetail]);
  return (
    <MuiDrawer
      PaperProps={{
        sx: { width: "70%" }
      }}
      elevation={999}
      anchor="right"
      open={open}
      onClose={handleClose}
    >
      <Stack
        flexDirection={"row"}
        justifyContent="space-between"
        alignItems="center"
        sx={{ background: "#E6E7F1", px: 7, py: 3 }}
      >
        <Typography sx={{ fontSize: 18 }}>Setting</Typography>
        <Typography
          onClick={handleClose}
          sx={{ cursor: "pointer", fontWeight: "700" }}
        >
          X
        </Typography>
      </Stack>
      <Box sx={{ p: 7 }}>
        <Typography sx={{ fontSize: 18, fontWeight: "600", mb: "30px" }}>
          Warmup stats
        </Typography>
        <Grid container columnSpacing={3}>
          <Grid item md={3}>
            <CustomStatsCard>
              <img style={{ marginRight: 5 }} src={images.replyMail} />
              <StatsTitleTypography>Total Email Replied</StatsTitleTypography>
              <Typography sx={{ marginLeft: "auto" }}>
                {statsDetail?.total_message_replied}
              </Typography>
            </CustomStatsCard>
          </Grid>
          <Grid item md={3}>
            <CustomStatsCard>
              <img style={{ marginRight: 5 }} src={images.incomingMail} />
              <StatsTitleTypography>Total Incoming Mail</StatsTitleTypography>
              <Typography sx={{ marginLeft: "auto" }}>
                {statsDetail?.total_inbox_read}
              </Typography>
            </CustomStatsCard>
          </Grid>
          <Grid item md={3}>
            <CustomStatsCard>
              <img style={{ marginRight: 5 }} src={images.replyMail} />
              <StatsTitleTypography>Total Email Replied</StatsTitleTypography>
              <Typography sx={{ marginLeft: "auto" }}>
                {statsDetail?.total_message_replied}
              </Typography>
            </CustomStatsCard>
          </Grid>
          <Grid item md={3}>
            <CustomStatsCard>
              <img style={{ marginRight: 5 }} src={images.spamMail} />
              <StatsTitleTypography>Total Spam Mail</StatsTitleTypography>
              <Typography sx={{ marginLeft: "auto" }}>
                {statsDetail?.total_spam_converted}
              </Typography>
            </CustomStatsCard>
          </Grid>
        </Grid>
        <Divider sx={{ my: 5 }} />
        <Typography sx={{ fontSize: 18, fontWeight: "600", mb: "30px" }}>
          Mail Configuration
        </Typography>
        <form onSubmit={handleSubmit(onSettingHandler)}>
          <Grid container rowSpacing={2} columnSpacing={4}>
            <Grid item md={6}>
              <CustomTypography
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                Set Sending Limit{" "}
                <Tooltip title="Sending Limit" placement="top" arrow>
                  <CustomInfoIcon />
                </Tooltip>
              </CustomTypography>
              <CustomTextField
                control={control}
                type="text"
                id="outlined-basic"
                name="send_limit"
                placeholder="No. of mails per day"
                variant="outlined"
              />
            </Grid>
            <Grid item md={6}>
              <CustomTypography
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                Set Reading Limit
                <Tooltip title="Reading Limit" placement="top" arrow>
                  <CustomInfoIcon />
                </Tooltip>
              </CustomTypography>
              <CustomTextField
                control={control}
                type="text"
                id="outlined-basic"
                name="reading_time"
                placeholder="Duration to read email"
                variant="outlined"
              />
            </Grid>
            <Grid item md={6}>
              <CustomTypography
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                Set Reply Rate{" "}
                <Tooltip title="Reply Rate" placement="top" arrow>
                  <CustomInfoIcon />
                </Tooltip>
              </CustomTypography>
              <CustomTextField
                control={control}
                type="text"
                id="outlined-basic"
                name="reply_rate"
                placeholder="percentage to reply"
                variant="outlined"
              />
            </Grid>
            <Grid item md={6}>
              <CustomTypography
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                Set Ramp-up Percentage{" "}
                <Tooltip title="Ramp-up Percentage" placement="top" arrow>
                  <CustomInfoIcon />
                </Tooltip>
              </CustomTypography>
              <CustomTextField
                control={control}
                type="text"
                id="outlined-basic"
                name="ramp_up"
                placeholder="rampup pergentage"
                variant="outlined"
              />
            </Grid>
            <Grid item md={6}>
              <CustomTypography
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                Set Maximum Limit{" "}
                <Tooltip title="Maximum Limit" placement="top" arrow>
                  <CustomInfoIcon />
                </Tooltip>
              </CustomTypography>
              <CustomTextField
                control={control}
                type="text"
                id="outlined-basic"
                name="max_limit"
                placeholder="Maximum limit"
                variant="outlined"
              />
            </Grid>
            <Grid item md={6}>
              <CustomTypography
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                Templates{" "}
                <Tooltip title="Choose Template" placement="top" arrow>
                  <CustomInfoIcon />
                </Tooltip>
              </CustomTypography>
              <CustomSelect
                searchable={true}
                multiple={true}
                name="templates"
                options={tempList.result.map((x: any) => {
                  return {
                    label: x.title,
                    value: x.id
                  };
                })}
                control={control}
              />
            </Grid>
            {/* <Grid item md={6}>
              <CustomTypography>Monitor Blacklists</CustomTypography>
              <CustomSwitch1
                defaultChecked={false}
                // onChange={() => {

                // }}
              />
            </Grid> */}
            <Grid item md={12}>
              <Stack
                flexDirection={"row"}
                justifyContent="flex-end"
                sx={{ paddingTop: "50px" }}
              >
                <CustomPrimaryButton type="submit" sx={{ mr: 2 }}>
                  Save
                </CustomPrimaryButton>
                <CustomSecondaryButton onClick={(_) => reset(defaultValues)}>
                  Clear
                </CustomSecondaryButton>
              </Stack>
            </Grid>
          </Grid>
        </form>
      </Box>
    </MuiDrawer>
  );
};

interface IDrawer {
  open: boolean;
  handleClose: () => void;
  settingId: string;
}
export default Drawer;
