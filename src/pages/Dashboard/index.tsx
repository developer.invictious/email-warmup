import { Box, Button, Divider, Stack, Typography } from "@mui/material";
import { ReactComponent as DisplayIcon } from "@sms/assets/svg/displayicon.svg";
import { user } from "@sms/hooks/useIsFirstLogin";
import MailboxPopUp from "@sms/pages/PopupForm/MailboxPopup";
import { useFetchMailbox } from "@sms/services/warmup-email";
import { DigiHubColor } from "@sms/shared/colors";
import { useEffect, useState } from "react";
import Joyride, { CallBackProps, STATUS } from "react-joyride";
import DashboardList from "./DashboardList";

const Dashboard = () => {
  const [openMail, setOpenMail] = useState(false);
  const [run, setRun] = useState(false);

  const handlePopUp = (popUp: boolean) => {
    setOpenMail(popUp);
  };

  const handleClose = () => {
    setOpenMail(false);
  };

  //react-joyride code
  useEffect(() => {
    setRun(true);
  }, []);

  const handleJoyrideCallback = (data: CallBackProps) => {
    const { status, type } = data;
    const finishedStatuses: string[] = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      setRun(false);
    }
  };

  const steps: any = [
    {
      title: (
        <strong style={{ fontFamily: "Montserrat" }}>
          Add New Configuration
          <Divider sx={{ pt: "20px" }} />
        </strong>
      ),
      content: (
        <h4 style={{ fontFamily: "Montserrat" }}>
          Here you can add new mailing configurations
        </h4>
      ),
      floaterProps: {
        disableAnimation: true
      },
      //   spotlightPadding: 25,
      placement: "bottom",
      target: "#start",
      disableBeacon: true
    }
  ];

  const { data: mailboxList = { result: [], pages: 1 } } = useFetchMailbox(
    { page: 1, limit: 5, status: "" } ?? ""
  );

  return (
    <Box>
      {user.isFirstTime <= 1 ? (
        <>
          <Joyride
            callback={handleJoyrideCallback}
            // continuous={true}
            // getHelpers={getHelpers}
            disableScrolling={true}
            run={run}
            showProgress={true}
            showSkipButton={true}
            steps={steps}
            styles={{
              options: {
                zIndex: 10000
              },
              buttonNext: {
                background: "#4F539E",
                color: "#fff",
                border: "none !important"
              }
            }}
          />
        </>
      ) : (
        <></>
      )}
      {mailboxList.result.length <= 0 ? (
        <>
          <Box
            justifyContent="center"
            display="flex"
            flexDirection="column"
            alignItems="center"
            sx={{
              height: "600px",
              alignItems: "center"
            }}
          >
            <DisplayIcon height="50%" width="50%" />
            <Typography
              fontSize={32}
              fontWeight={600}
              textTransform="none"
              sx={{ margin: "10px" }}
            >
              Welcome
            </Typography>
            <Typography
              fontSize={16}
              fontWeight={300}
              align="center"
              textTransform="none"
              sx={{ width: "30%" }}
            >
              Make your email account more efficient, improve email
              deliverability, and increase your open rate.
            </Typography>
            <Button
              onClick={() => setOpenMail(true)}
              variant="contained"
              color="primary"
              size="large"
              sx={{ mt: "15px" }}
              id="start"
            >
              <Typography fontSize={14} fontWeight={500} textTransform="none">
                Add New Configuration
              </Typography>
            </Button>
          </Box>
        </>
      ) : (
        <>
          <Stack
            direction="row"
            justifyContent="space-between"
            color={DigiHubColor.lightBlack}
            mt={7}
            px={3}
            mb={2}
          >
            <Typography fontSize={20} fontWeight={400}>
              Mailbox Configuration
            </Typography>
            <Button
              onClick={() => setOpenMail(true)}
              variant="contained"
              color="primary"
              size="large"
              sx={{ mb: 2 }}
            >
              <Typography fontSize={14} fontWeight={500} textTransform="none">
                Add New Configuration
              </Typography>
            </Button>
          </Stack>
          <DashboardList />
        </>
      )}
      <MailboxPopUp
        open={openMail}
        handleClose={handleClose}
        handlePopUp={handlePopUp}
      />
    </Box>
  );
};

export default Dashboard;
