import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  Switch,
  SwitchProps,
  Tooltip
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { images } from "@sms/assets/images";
import { ReactComponent as Delete } from "@sms/assets/svg/delete.svg";
// import { ReactComponent as Edit } from "@sms/assets/svg/edit.svg";
import AssessmentOutlinedIcon from "@mui/icons-material/AssessmentOutlined";
import { ReactComponent as Setting } from "@sms/assets/svg/settinglogo.svg";
import Datatable from "@sms/components/Datatable";
import {
  useDeleteMailbox,
  useFetchMailbox,
  useToggle
} from "@sms/services/warmup-email";
import moment from "moment";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Cell } from "react-table";
import SettingDrawer from "./Drawer";
// import EditDrawer from "./EditDrawer";

export const CustomSwitch1 = styled((props: SwitchProps) => (
  <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
  width: 55,
  height: 26,
  padding: 0,

  "& .MuiSwitch-switchBase": {
    padding: 0,
    margin: 2,
    transitionDuration: "300ms",
    color: "#CC0303",
    "&.Mui-checked": {
      transform: "translateX(29px)",
      color: "#308D44",
      "& + .MuiSwitch-track": {
        backgroundColor:
          theme.palette.mode === "dark" ? "#2ECA45" : "#308d441a",
        opacity: 1,
        border: 0
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: 0.5
      },
      "& .MuiSwitch-thumb": {
        boxSizing: "border-box",
        width: 20,
        height: 20,
        marginTop: 1,
        marginLeft: 1,
        background: "#308D44"
      }
    },
    "&.Mui-focusVisible .MuiSwitch-thumb": {
      color: "#33cf4d",
      border: "6px solid #fff"
    },
    "&.Mui-disabled .MuiSwitch-thumb": {
      color:
        theme.palette.mode === "light"
          ? theme.palette.grey[100]
          : theme.palette.grey[600]
    },
    "&.Mui-disabled + .MuiSwitch-track": {
      opacity: theme.palette.mode === "light" ? 0.7 : 0.3
    }
  },
  "& .MuiSwitch-thumb": {
    boxSizing: "border-box",
    width: 20,
    height: 20,
    marginTop: 1,
    marginLeft: 1,
    background: "#E70000"
  },
  "& .MuiSwitch-track": {
    borderRadius: 36 / 2,
    backgroundColor: theme.palette.mode === "light" ? "#FAE6E6" : "#39393D",
    opacity: 1,
    transition: theme.transitions.create(["background-color"], {
      duration: 500
    })
  }
}));

const DashboardList = () => {
  const navigate = useNavigate();
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(1);
  const [open, setOpen] = useState(false);
  const [showDrawer, setShowDrawer] = useState(false);
  //   const [showEditDrawer, setEditDrawer] = useState(false);
  const [warmActive, setWarnActive] = useState("");
  const [settingId, setSettingId] = useState("");
  //   const [editId, setEditId] = useState("");
  const [openDial, setOpenDial] = useState(false);
  const [deleteMailbox, setDeleteMailbox] = useState("");
  const [toggleId, setToggleId] = useState("");
  //   const [stt, setStt] = useState(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const handleDropdown = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const openDrodown = Boolean(anchorEl);
  const id = openDrodown ? "simple-popper" : undefined;

  const { mutateAsync: deleteMail, isLoading } = useDeleteMailbox();

  const handleClose = () => {
    setOpen(false);
    setOpenDial(false);
    setAnchorEl(null);
  };

  const handleClickOpen = (data: string) => {
    setOpenDial(true);
    setDeleteMailbox(data);
  };

  const { data: mailboxList = { result: [], pages: 1 }, isFetching } =
    useFetchMailbox({ page, limit, status: warmActive } ?? "");

  const totalPages = JSON.stringify(mailboxList);

  const handleLimit = (limit: number): void => {
    setLimit(limit);
  };

  const handlePage = (page: number): void => {
    setPage(page + 1);
  };

  const { mutateAsync } = useToggle();
  const item: any = mailboxList?.result.find((x) => x.id === toggleId);
  const columns = [
    {
      Header: "Email",
      accessor: "email"
    },
    {
      Header: "Created Date",
      Cell: (cell: Cell<any>) => (
        <Stack direction="row">
          <Stack direction="row" mr={4}>
            <img
              width="20"
              height="20"
              src={images.datepicker}
              style={{ marginRight: 10 }}
            />
            {moment(cell.row.original.createdAt).format("DD MMM, YYYY")}
          </Stack>
          <Tooltip title="Total Mail Sent" placement="top" arrow={true}>
            <Stack direction="row" mr={2}>
              <img
                width="20"
                height="20"
                src={images.mailSent}
                style={{ marginRight: 5 }}
              />
              {cell.row.original.stats.total_message_sent}
            </Stack>
          </Tooltip>
          <Tooltip title="Total Incoming Mail" placement="top" arrow={true}>
            <Stack direction="row" mr={2}>
              <img
                width="20"
                height="20"
                src={images.incomingMail}
                style={{ marginRight: 5 }}
              />
              {cell.row.original.stats.total_inbox_read}
            </Stack>
          </Tooltip>
          <Tooltip title="Total Email Replied" placement="top" arrow={true}>
            <Stack direction="row" mr={2}>
              <img
                width="20"
                height="20"
                src={images.replyMail}
                style={{ marginRight: 5 }}
              />
              {cell.row.original.stats.total_message_replied}
            </Stack>
          </Tooltip>
          <Tooltip title="Total Spam Mail" placement="top" arrow={true}>
            <Stack direction="row" mr={2}>
              <img
                width="20"
                height="20"
                src={images.spamMail}
                style={{ marginRight: 5 }}
              />
              {cell.row.original.stats.total_spam_converted}
            </Stack>
          </Tooltip>
        </Stack>
      )
    },

    {
      Header: "Status",
      Cell: (cell: Cell<any>) => (
        <>
          <IconButton
            sx={{
              "&:hover": {
                backgroundColor: "transparent"
              }
            }}
          >
            <CustomSwitch1
              defaultChecked={cell.row.original.status}
              onChange={() => {
                setOpen(true);
                setToggleId(cell.row.original?.id);
              }}
            />
          </IconButton>
        </>
      )
    },
    {
      Header: "Actions",
      Cell: (cell: Cell<any>) => (
        <Stack direction="row" justifyContent="flex-end">
          <IconButton
            aria-label="setting"
            // onClick={() => navigate(`/mailconfig/edit/${cell.row.original.id}`)}
            onClick={() => {
              setSettingId(cell.row.original.id);
              setShowDrawer(true);
            }}
          >
            <Setting />
          </IconButton>
          <IconButton
            aria-label="edit"
            onClick={() => navigate(`/report/${cell.row.original.id}`)}
          >
            <AssessmentOutlinedIcon sx={{ color: "#4F539E" }} />
          </IconButton>

          <IconButton
            aria-label="delete"
            onClick={() => {
              handleClickOpen(cell.row.original?.id);
            }}
          >
            <Delete />
          </IconButton>
        </Stack>
      )
    }
  ];
  return (
    <>
      <Datatable
        HeaderChildren={
          <IconButton
            onClick={handleDropdown}
            aria-describedby={id}
            sx={{
              backgroundColor: "#4F539E",
              ml: 2,
              borderRadius: "8px",
              "&:hover": {
                backgroundColor: "#4F539E"
              }
            }}
          >
            <img src={images.filterList} />
          </IconButton>
        }
        showLimit
        searchable
        data={mailboxList.result}
        columns={columns}
        loading={isFetching}
        onPageSizeChange={handleLimit}
        pageSize={JSON.parse(totalPages).pages}
        isBackendPagination={true}
        paginateParams={{
          gotoPage: handlePage,
          pageIndex: page - 1,
          canNextPage: page + 1 <= JSON.parse(totalPages).pages ? true : false,
          canPreviousPage:
            page + 1 > JSON.parse(totalPages).pages ? true : false
        }}
      />

      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={openDrodown}
        onClose={() => setAnchorEl(null)}
        MenuListProps={{
          "aria-labelledby": "basic-button"
        }}
        sx={{ top: 20 }}
      >
        <MenuItem
          selected={warmActive === "true" ? true : false}
          sx={{
            fontSize: 15,
            p: "5px 30px",
            m: "10px",
            borderRadius: "12px",
            "&:hover": { background: "#4F539E", color: "#fff" },
            "&.Mui-selected": { background: "#4F539E", color: "#fff" }
          }}
          onClick={() => {
            setWarnActive("true");
            handleClose();
          }}
        >
          Warmup Active
        </MenuItem>
        <MenuItem
          selected={warmActive === "false" ? true : false}
          sx={{
            fontSize: 15,
            p: "5px 30px",
            m: "10px",
            borderRadius: "12px",

            "&:hover": { background: "#4F539E", color: "#fff" },
            "&.Mui-selected": { background: "#4F539E", color: "#fff" }
          }}
          onClick={() => {
            setWarnActive("false");
            handleClose();
          }}
        >
          Warmup Paused
        </MenuItem>
        <MenuItem
          selected={warmActive === "" ? true : false}
          sx={{
            fontSize: 15,
            p: "5px 30px",
            m: "10px",
            borderRadius: "12px",
            "&:hover": { background: "#4F539E", color: "#fff" },
            "&.Mui-selected": { background: "#4F539E", color: "#fff" }
          }}
          onClick={() => {
            setWarnActive("");
            handleClose();
          }}
        >
          Show All
        </MenuItem>
      </Menu>

      <Dialog open={openDial} onClose={handleClose}>
        <DialogTitle>Alert!!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you really want to delete this mailbox?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            disableElevation
            variant="contained"
            onClick={async () => {
              await deleteMail(deleteMailbox);
              setOpenDial(false);
            }}
          >
            Yes
          </Button>
          <Button
            disableElevation
            variant="contained"
            color="error"
            onClick={handleClose}
          >
            No
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Alert!!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {item?.status
              ? "Do you want to turn off the warm up?"
              : "Do you want to turn on the warm up?"}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            disableElevation
            onClick={() => {
              mutateAsync(toggleId);
              setOpen(false);
            }}
          >
            Yes
          </Button>
          <Button
            disableElevation
            variant="contained"
            color="error"
            onClick={handleClose}
          >
            No
          </Button>
        </DialogActions>
      </Dialog>
      <SettingDrawer
        open={showDrawer}
        handleClose={() => setShowDrawer(false)}
        settingId={settingId.toString()}
      />
      {/* <EditDrawer
        open={showEditDrawer}
        handleClose={() => setEditDrawer(false)}
        editId={editId}
      /> */}
    </>
  );
};

export default DashboardList;
