import { yupResolver } from "@hookform/resolvers/yup";
import { Checkbox, Grid, Stack, Typography } from "@mui/material";
import MuiDrawer from "@mui/material/Drawer";
import { Box } from "@mui/system";
import {
  useFetchMailBoxById,
  useUpdateMailbox
} from "@sms/services/warmup-email";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import {
  CustomFormLabel,
  CustomPrimaryButton,
  CustomSecondaryButton,
  CustomTextField,
  CustomTypography
} from "../PopupForm/mailbox.style";

const defaultValues = {
  id: "",
  smtp_host: "",
  imap_host: "",
  smtp_port: "",
  imap_port: "",
  email: "",
  encryption: ""
};
const schema = Yup.object().shape({
  // server: Yup.string().required("Provider name is required"),
  // name: Yup.string().required("Name is required"),
  smtp_host: Yup.string().required("SMTP host is required"),
  imap_host: Yup.string().required("IMAP host is required"),
  // password: Yup.string()
  //   .min(6, "Password should be at least six character")
  //   .required("Enter password"),
  smtp_port: Yup.string().required("SMTP port is required"),
  imap_port: Yup.string().required("IMAP port is required"),
  email: Yup.string().required("Email is required"),
  encryption: Yup.string()
});

const EditDrawer = ({ open, handleClose, editId }: IEditDrawer) => {
  const navigate = useNavigate();
  const { data: MailboxDetail } = useFetchMailBoxById(editId ?? "");
  const { mutateAsync: updateMailbox, isLoading } = useUpdateMailbox();
  const handleCheck = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked === false) {
      setValue("encryption", "unencrypted");
    } else {
      setValue("encryption", "SSL/TLS");
    }
  };
  const { control, handleSubmit, reset, setValue } = useForm({
    defaultValues: defaultValues,
    resolver: yupResolver(schema)
  });
  const onEditHandler = async (editDetails: typeof defaultValues) => {
    const requestBody = {
      smtp_host: editDetails.smtp_host,
      imap_host: editDetails.imap_host,
      smtp_port: editDetails.smtp_port,
      imap_port: editDetails.imap_port,
      email: editDetails.email,
      encryption: editDetails.encryption
    };
    try {
      await updateMailbox({ ...requestBody, id: editDetails.id });
      navigate("/dashboard");
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    if (MailboxDetail) {
      reset({
        ...defaultValues,
        id: MailboxDetail.id.toString(),

        smtp_host: MailboxDetail.smtp_host,
        imap_host: MailboxDetail.imap_host,

        smtp_port: MailboxDetail.smtp_port,
        imap_port: MailboxDetail.imap_port,
        email: MailboxDetail.email
      });
    }
  }, [MailboxDetail]);
  return (
    <MuiDrawer
      PaperProps={{
        sx: { width: "70%" }
      }}
      elevation={999}
      anchor="right"
      open={open}
      onClose={handleClose}
    >
      <Stack
        flexDirection={"row"}
        justifyContent="space-between"
        alignItems="center"
        sx={{ background: "#E6E7F1", px: 7, py: 3 }}
      >
        <Typography sx={{ fontSize: 18 }}>Edit Mailbox</Typography>
        <Typography
          onClick={handleClose}
          sx={{ cursor: "pointer", fontWeight: "700" }}
        >
          X
        </Typography>
      </Stack>
      <Box sx={{ p: 7 }}>
        <Typography sx={{ fontSize: 18, fontWeight: "600", mb: "30px" }}>
          Mailbox Configuration
        </Typography>
        <form onSubmit={handleSubmit(onEditHandler)}>
          <Grid container rowSpacing={2} columnSpacing={4}>
            <Grid item md={6}>
              <CustomTypography>Email Adddress</CustomTypography>
              <CustomTextField
                control={control}
                name="email"
                type="email"
                placeholder="eg. something@gmail.com"
              />
            </Grid>
            <Grid item md={6}></Grid>
            <Grid item xs={12} md={6}>
              <Typography fontWeight="bold" sx={{ color: "#4F539E" }}>
                SMTP Server
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}></Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Host</CustomTypography>
              <CustomTextField
                control={control}
                name="smtp_host"
                placeholder="eg:smtp.xyz.com"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Port Number</CustomTypography>
              <CustomTextField
                control={control}
                name="smtp_port"
                placeholder="eg:25"
                type="number"
              />
            </Grid>
            {/* <Grid item xs={12} md={12}>
                    <CustomFormLabel
                      control={<Checkbox  />}
                      label="Do you want to use SSL/TSL?"
                    />
                  </Grid> */}
            <Grid item xs={12} md={6}>
              <Typography fontWeight="bold" sx={{ color: "#4F539E" }}>
                IMAP Server
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}></Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Host</CustomTypography>
              <CustomTextField
                control={control}
                name="imap_host"
                placeholder="eg:imap.xyz.com"
                type="text"
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <CustomTypography>Port Number</CustomTypography>
              <CustomTextField
                control={control}
                name="imap_port"
                placeholder="eg:143"
                type="number"
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <CustomFormLabel
                control={
                  <Checkbox onChange={handleCheck} defaultChecked={true} />
                }
                label="Do you want to use SSL/TLS?"
              />
            </Grid>
            <Grid item md={12}>
              <Stack flexDirection={"row"} justifyContent="flex-end">
                <CustomPrimaryButton type="submit" sx={{ mr: 2 }}>
                  Save
                </CustomPrimaryButton>
                <CustomSecondaryButton onClick={(_) => reset(defaultValues)}>
                  Clear
                </CustomSecondaryButton>
              </Stack>
            </Grid>
          </Grid>
        </form>
      </Box>
    </MuiDrawer>
  );
};
interface IEditDrawer {
  open: boolean;
  handleClose: () => void;
  editId: string;
}
export default EditDrawer;
