import { Link, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Datatable from "@sms/components/Datatable";
import { user } from "@sms/hooks/useIsFirstLogin";
import { useGetPaymentDetail } from "@sms/services/warmup-stats";

import { getLoggedDetail } from "@sms/services/warmup-stripe";
import moment from "moment";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Cell } from "react-table";

const Billing = () => {
  const navigate = useNavigate();
  const { mutateAsync } = getLoggedDetail();
  const [customerId, setCustomerId] = useState("");
  useEffect(() => {
    mutateAsync(user.id).then(async (x: any) => {
      setCustomerId(x.data.stripeId);
    });
  }, []);

  const { data: paymentList = [], isFetching } = useGetPaymentDetail(
    customerId ?? ""
  );
  const columns = [
    {
      Header: "Package",
      accessor: "description"
    },
    {
      Header: "Paid Amount",
      Cell: (cell: Cell<any>) => <>{cell.row.original.amount / 100}$</>
    },
    {
      Header: "Currency",
      accessor: "currency"
    },
    {
      Header: "Paid At",
      Cell: (cell: Cell<any>) => (
        <>{moment.unix(cell.row.original.created).format("DD MMM, YYYY")}</>
      )
      //   accessor: "created"
    },
    {
      Header: "Receipt",
      Cell: (cell: Cell<any>) => (
        <>
          <Link target="_blank" href={cell.row.original.receipt_url}>
            view
          </Link>
        </>
      )
      //   accessor: "available_on"
    }
  ];
  return (
    <Box>
      <Stack
        direction="row"
        justifyContent="space-between"
        mt={7}
        px={3}
        mb={2}
      >
        <Typography fontSize={20} fontWeight={400}>
          Billing Details
        </Typography>
      </Stack>

      <Datatable
        showTableHeader
        searchable
        columns={columns}
        data={paymentList}
        loading={isFetching}
      />
    </Box>
  );
};

export default Billing;
