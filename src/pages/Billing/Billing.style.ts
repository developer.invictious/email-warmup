import LoadingButton from "@mui/lab/LoadingButton";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import TextInput from "@sms/components/Form/Input";
import SelectComponent from "@sms/components/Form/Select";

const CustomPrimaryButton = styled(LoadingButton)({
  background: "#4F539E;",
  borderRadius: "6px;",
  height: "42px",
  //   width: "94px",
  color: "#fff",
  padding: "12px 24px;",
  fontWeight: "500",
  textTransform: "none",
  "&:hover": {
    background: "rgb(98, 101, 162)"
  }
});
const CustomSecondaryButton = styled(Button)({
  background: "#fff;",
  border: "1px solid #024B8D",
  borderRadius: "6px;",
  height: "42px",
  //   width: "94px",
  color: "#024B8D",
  padding: "12px 24px;",
  fontWeight: "500",
  textTransform: "none"
});
const CustomTypography = styled(Typography)({
  fontWeight: "400",
  color: "#6E6B7b",
  fontSize: "14px",
  marginBottom: "8px"
});

const CustomCard = styled(Card)({
  width: "100%",
  padding: "40px",
  boxShadow: "none"
});
const CustomTitle = styled(Typography)({
  fontWeight: "400",
  fontSize: "14px"
});
const CustomTextField = styled(TextInput)({
  width: "100%",
  //   paddingBottom: "15px",
  margin: 0,
  fontSize: "14px"
});

const CustomHeaderTitle = styled(Typography)({
  fontWeight: "500",
  fontSize: "21px",
  color: "#4F539E",
  marginBottom: "16px"
});
const MainTitle = styled(Typography)({
  fontWeight: "400",
  fontSize: "20px"
});
const CustomSelect = styled(SelectComponent)({
  borderColor: "none",
  "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
    border: "1px solid grey"
  }
});

export {
  CustomSelect,
  CustomCard,
  CustomHeaderTitle,
  CustomPrimaryButton,
  CustomSecondaryButton,
  CustomTextField,
  CustomTitle,
  CustomTypography,
  MainTitle
};
