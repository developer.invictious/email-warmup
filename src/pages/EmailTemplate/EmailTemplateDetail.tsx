import { useParams } from "react-router-dom";

import { Box, Grid, LinearProgress, Stack, Typography } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import BreadcrumbsComponent from "@sms/components/BreadCrumb";
import { useFetchTempById } from "@sms/services/warmup-template";
import parse from "html-react-parser";
function EmailTemplateDetail() {
  const { templateId } = useParams();
  const { data: tempDetail, isLoading } = useFetchTempById(templateId ?? "");

  return (
    <Box>
      <Typography fontSize={20} fontWeight={400}>
        Email Template
      </Typography>

      <BreadcrumbsComponent />

      <Stack direction="row" justifyContent="center" sx={{ m: 5 }}>
        <Card sx={{ width: "60%", borderRadius: 3, boxShadow: "none" }}>
          <CardContent>
            {isLoading && <LinearProgress color="info" />}
            <Typography
              fontWeight="bold"
              sx={{ fontSize: 16, my: 2 }}
              gutterBottom
            >
              Details
            </Typography>

            <Grid container>
              <Grid item sm={6}>
                <Grid container>
                  <Grid item xs={12}>
                    <Stack direction="row">
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Subject:
                      </Typography>
                      <Typography
                        sx={{ fontSize: 16, pl: 1 }}
                        gutterBottom
                        color="text.secondary"
                      >
                        {tempDetail?.title}
                      </Typography>
                    </Stack>
                  </Grid>

                  <Grid item xs={12}>
                    <Stack direction="row">
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Language:
                      </Typography>
                      <Typography
                        sx={{ fontSize: 16, pl: 1 }}
                        gutterBottom
                        color="text.secondary"
                      >
                        {tempDetail?.language}
                      </Typography>
                    </Stack>
                  </Grid>

                  <Grid item xs={12}>
                    <Stack direction="row">
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Description:
                      </Typography>
                      <Typography
                        sx={{ fontSize: 16, pl: 1 }}
                        gutterBottom
                        color="text.secondary"
                      >
                        {tempDetail && parse(tempDetail.description)}
                      </Typography>
                    </Stack>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item sm={6}></Grid>
            </Grid>
          </CardContent>
        </Card>
      </Stack>
    </Box>
  );
}
export default EmailTemplateDetail;
