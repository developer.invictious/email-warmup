import { yupResolver } from "@hookform/resolvers/yup";
import InfoIcon from "@mui/icons-material/Info";
import { Box, Divider, Grid, Tooltip, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import Editor from "@sms/components/Form/Editor";
import {
  useCreateTemp,
  useFetchTempById,
  useLang,
  useUpdateTemp
} from "@sms/services/warmup-template";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import { CustomTextField } from "../Login/customstyled";
import {
  CustomCard,
  CustomHeaderTitle,
  CustomPrimaryButton,
  CustomSecondaryButton,
  CustomSelect,
  CustomTitle,
  CustomTypography,
  MainTitle
} from "./EmailTemplateForm.style";

const CustomInfoIcon = styled(InfoIcon)({
  fontSize: "24px",
  marginLeft: "5px",
  color: "#E6E7F1",
  "&:hover": {
    cursor: "pointer"
  }
});

const defaultValues = {
  id: null as number | null,
  language: "",
  title: "",
  description: ""
};
const EmailTemplateForm = () => {
  const navigate = useNavigate();
  const { templateId } = useParams();
  const schema = Yup.object().shape({
    title: Yup.string().required("Title is required"),
    language: Yup.string().required("Language is required"),
    description: Yup.string().required("Description is required")
  });
  const { data: langList = [], isFetching } = useLang();
  const { data: tempDetail } = useFetchTempById(templateId ?? "");

  const { mutateAsync: addTemp, isLoading } = useCreateTemp();
  const { mutateAsync: updateTemp, isLoading: isUpdatingTemp } =
    useUpdateTemp();
  const { control, handleSubmit, reset, getValues } = useForm({
    defaultValues: defaultValues,
    resolver: yupResolver(schema)
  });

  const onTempAddHandler = async (tempDetails: typeof defaultValues) => {
    const requestBody = {
      title: tempDetails.title,
      language: tempDetails.language,
      description: tempDetails.description
    };

    try {
      if (tempDetails.id) {
        await updateTemp({
          description: tempDetails.description,
          title: tempDetails.title,
          language: tempDetails.language,
          id: tempDetails.id
        });
      } else {
        await addTemp({
          ...requestBody
        });
      }
      navigate("/email-template");
    } catch (e) {
      console.log(e);
    }
  };

  React.useEffect(() => {
    if (tempDetail) {
      reset({
        ...defaultValues,
        id: tempDetail.id,
        title: tempDetail.title,
        language: tempDetail.language,
        description: tempDetail.description
      });
    }
  }, [tempDetail]);
  return (
    <Box sx={{ paddingTop: "20px", mt: 7 }}>
      <MainTitle sx={{ mb: 2 }}>Email Template Form</MainTitle>
      <Grid container>
        <Grid item xs={12}>
          <CustomCard sx={{ marginTop: 2.5 }}>
            <CustomHeaderTitle>Email Template Form</CustomHeaderTitle>
            <Divider />
            <CustomTitle sx={{ marginTop: 1.5 }}>
              Please kindly fill up the form*
            </CustomTitle>

            <Grid
              container
              sx={{ paddingTop: "33px", display: "flex", alignItems: "center" }}
            >
              <form
                style={{ width: "100%" }}
                onSubmit={handleSubmit(onTempAddHandler)}
              >
                <Grid
                  container
                  rowSpacing={2}
                  columnSpacing={6}
                  alignItems="center"
                >
                  <Grid item xs={12} md={6}>
                    <CustomTypography
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      Language Type{" "}
                      <Tooltip title="Language Type" placement="top" arrow>
                        <CustomInfoIcon />
                      </Tooltip>
                    </CustomTypography>
                    <CustomSelect
                      control={control}
                      multiple={false}
                      name="language"
                      options={langList.map((x) => {
                        return {
                          label: x,
                          value: x
                        };
                      })}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <CustomTypography
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      Subject{" "}
                      <Tooltip title="Email Subject" placement="top" arrow>
                        <CustomInfoIcon />
                      </Tooltip>
                    </CustomTypography>
                    <CustomTextField
                      sx={{ paddingBottom: "0px" }}
                      control={control}
                      type="text"
                      id="outlined-basic"
                      name="title"
                      placeholder="Subject"
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12}>
                    <CustomTypography
                      sx={{
                        paddingTop: "15px",
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      Email description{" "}
                      <Tooltip title="Description" placement="top" arrow>
                        <CustomInfoIcon />
                      </Tooltip>
                    </CustomTypography>
                    {/* <CustomTextField
                      rows={4}
                      multiline
                      control={control}
                      type="text"
                      id="outlined-basic"
                      name="description"
                      placeholder="Description"
                      variant="outlined"
                    /> */}
                    <Controller
                      name="description"
                      control={control}
                      render={({
                        field: { onChange, value },
                        fieldState: { error }
                      }) => {
                        return (
                          <>
                            <Editor
                              onChange={onChange}
                              data={getValues("description")}
                              //   value={value}
                              // error={error}
                            />
                            {error && (
                              <Typography
                                fontWeight={400}
                                fontSize={12}
                                color={"#d32f2f"}
                              >
                                {error?.message}
                              </Typography>
                            )}
                          </>
                        );
                      }}
                    />
                    {/* <Editor /> */}
                  </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ mt: 2 }}>
                  <Grid item xs={0}></Grid>
                  <Grid
                    item
                    xs={12}
                    sx={{
                      display: "flex",
                      justifyContent: "end"
                    }}
                  >
                    <CustomPrimaryButton
                      type="submit"
                      //   loading={isLoading}
                      sx={{ marginRight: "16px" }}
                    >
                      {!templateId ? "Submit" : "Update"}
                    </CustomPrimaryButton>
                    <CustomSecondaryButton
                      onClick={(_) => reset(defaultValues)}
                    >
                      Discard
                    </CustomSecondaryButton>
                  </Grid>
                </Grid>
              </form>
            </Grid>
          </CustomCard>
        </Grid>
      </Grid>
    </Box>
  );
};

export default EmailTemplateForm;
