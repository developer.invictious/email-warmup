import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Stack,
  Typography
} from "@mui/material";
import { Box } from "@mui/system";
import Datatable from "@sms/components/Datatable";
import pageRoutes from "@sms/route/PageRoutes";
import { useDeleteTemp, useFetchAllTemps } from "@sms/services/warmup-template";
import parse from "html-react-parser";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Cell } from "react-table";
import { ReactComponent as Delete } from "../../assets/svg/delete.svg";
import { ReactComponent as Edit } from "../../assets/svg/edit.svg";
import { ReactComponent as View } from "../../assets/svg/view.svg";

const EmailTemplateList = () => {
  const navigate = useNavigate();
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(1);
  const [open, setOpen] = useState(false);
  const [deleteTemplate, setDeleteTemplate] = useState("");
  const { mutateAsync: deleteTemp, isLoading: deleteLoading } = useDeleteTemp();
  const { data: tempList = { result: [], pages: 1 }, isFetching } =
    useFetchAllTemps({ page, limit } ?? "");

  const totalPages = JSON.stringify(tempList);

  const handleLimit = (limit: number): void => {
    setLimit(limit);
  };

  const handlePage = (page: number): void => {
    setPage(page + 1);
  };

  const handleClickOpen = (data: string) => {
    setOpen(true);
    setDeleteTemplate(data);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const columns = [
    {
      Header: "Subject",
      accessor: "title"
    },
    {
      Header: "Description",
      accessor: "description",
      width: "50%",
      Cell: (cell: Cell<any>) => parse(cell.row.original.description)
    },
    {
      Header: "Language Type",
      accessor: "language"
      // width: "20%"
    },
    {
      Header: "Actions",
      // width: "10%",
      Cell: (cell: Cell<any>) => (
        <Stack direction="row" justifyContent="flex-end">
          <IconButton
            aria-label="view"
            onClick={() =>
              navigate(`/email-template/view/${cell.row.original.id}`)
            }
          >
            <View />
          </IconButton>
          <IconButton
            aria-label="edit"
            onClick={() =>
              navigate(`/email-template/edit/${cell.row.original.id}`)
            }
          >
            <Edit />
          </IconButton>

          <IconButton
            aria-label="delete"
            onClick={() => {
              handleClickOpen(cell.row.original?.id);
            }}
          >
            <Delete />
          </IconButton>
        </Stack>
      )
    }
  ];
  return (
    <Box>
      <Stack
        direction="row"
        justifyContent="space-between"
        mt={7}
        px={3}
        mb={2}
      >
        <Typography fontSize={20} fontWeight={400}>
          Email Template
        </Typography>
        <Button
          onClick={() => navigate(pageRoutes.add_email_template)}
          variant="contained"
          color="primary"
        >
          <Typography fontSize={14} fontWeight={500} textTransform="none">
            Add New Template
          </Typography>
        </Button>
      </Stack>

      <Datatable
        showLimit
        searchable
        columns={columns}
        data={tempList.result}
        loading={isFetching}
        onPageSizeChange={handleLimit}
        pageSize={JSON.parse(totalPages).pages}
        isBackendPagination={true}
        paginateParams={{
          gotoPage: handlePage,
          pageIndex: page - 1,
          canNextPage: page + 1 <= JSON.parse(totalPages).pages ? true : false,
          canPreviousPage:
            page + 1 > JSON.parse(totalPages).pages ? true : false
        }}
      />

      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Alert!!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you really want to delete this template?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            disableElevation
            onClick={async () => {
              await deleteTemp(deleteTemplate);
              setOpen(false);
            }}
          >
            Yes
          </Button>
          <Button
            disableElevation
            variant="contained"
            color="error"
            onClick={handleClose}
          >
            No
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

export default EmailTemplateList;
