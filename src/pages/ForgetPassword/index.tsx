import { yupResolver } from "@hookform/resolvers/yup";
import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { ReactComponent as Login_pic } from "../../assets/login-page.svg";
import { LoginDetails, useLoginMutation } from "../../services/sms-auth";

import {
  CustomButton,
  CustomGrid,
  CustomTextField,
  CustomTypography,
  HeadingTypography
} from "./customstyled";

const loginDefaultValues = {
  email: "",
  password: ""
};

const schema = Yup.object().shape({
  email: Yup.string().required("Email Address is Required"),
  password: Yup.string().required("Password is Required")
});

const ForgetPassword = () => {
  const { mutateAsync: initLogin, isLoading } = useLoginMutation();
  const { control, handleSubmit } = useForm({
    defaultValues: loginDefaultValues,
    resolver: yupResolver(schema)
  });
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const onLoginHandler = async (loginDetails: LoginDetails) => {
    try {
      await initLogin(loginDetails);
      navigate("/Dashboard");
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Box
      sx={{
        flexGrow: 1,
        backgroundColor: "white",
        overflow: "hidden"
      }}
    >
      <Grid container>
        <Grid item xs={12} md={7.5} sx={{ backgroundColor: "#F4F3F3" }}>
          <Login_pic width={"100%"} height={"100%"}></Login_pic>
        </Grid>
        <CustomGrid
          item
          xs={12}
          md={3.5}
          sx={{ height: "100vh", backgroundColor: "white" }}
        >
          <Grid container rowSpacing={2}>
            <HeadingTypography>Forget Password ? </HeadingTypography>
            <Typography
              sx={{
                width: "388px",
                height: "54px",
                fontWeight: "400",
                fontSize: "16px",
                lineHeight: "27px",
                marginBottom: "20px",
                color: "#4D4D4D",
                marginTop: "6px"
              }}
            >
              Please kindly fill up form *
            </Typography>
            <form
              style={{ width: "100%", margin: "auto" }}
              onSubmit={handleSubmit(onLoginHandler)}
            >
              <Grid item xs={8} md={12}>
                <CustomTypography>Email</CustomTypography>
                <CustomTextField
                  size="small"
                  control={control}
                  type="text"
                  id="outlined-basic"
                  name="email"
                  placeholder="Email"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={8} md={12}>
                <CustomButton
                  loading={isLoading}
                  variant="contained"
                  type="submit"
                >
                  Reset Password
                </CustomButton>
              </Grid>
              <Grid
                item
                xs={8}
                md={12}
                sx={{
                  marginTop: "16px"
                }}
              >
                <CustomTypography
                  sx={{
                    textAlign: "center"
                  }}
                >
                  Haven’t Created an account?
                  <Link
                    style={{
                      fontWeight: "bold",
                      marginLeft: "10px"
                    }}
                    to="/register"
                  >
                    Register
                  </Link>
                </CustomTypography>
              </Grid>
            </form>
          </Grid>
        </CustomGrid>
      </Grid>
    </Box>
  );
};

export default ForgetPassword;
