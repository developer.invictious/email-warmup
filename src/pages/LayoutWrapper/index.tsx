import { useState } from "react";
import { Link } from "react-router-dom";
import ErrorBoundary from "src/components/ErrorBoundry";
import Header from "src/components/Header";
import Sidebar from "src/components/Sidebar";

import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import MuiDrawer from "@mui/material/Drawer";
import { CSSObject, styled, Theme } from "@mui/material/styles";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/system/Box";

import { DigiHubColor } from "@sms/shared/colors";
import { ReactComponent as Logo } from "../../assets/logo-1.svg";
import { ReactComponent as Logo2 } from "../../assets/logo-2.svg";

interface ILayoutWrapper {
  children: React.ReactNode;
}

const drawerWidth = 256;

const openedMixin = (theme: Theme): CSSObject => ({
  width: drawerWidth,
  borderRight: "none",
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen
  }),
  overflowX: "hidden"
});

const closedMixin = (theme: Theme): CSSObject => ({
  borderRight: "none",
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(10.25)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(11.25)} + 1px)`
  }
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar
}));

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open"
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  })
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open"
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme)
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme)
  })
}));

const LayoutWrapper: React.FC<ILayoutWrapper> = ({ children }) => {
  const [open, setOpen] = useState(true);

  const handleDrawerToggle = () => {
    setOpen((oldState) => !oldState);
  };

  return (
    <ErrorBoundary>
      <Box sx={{ display: "flex", height: "130%" }}>
        <AppBar
          position="fixed"
          open={open}
          color="inherit"
          sx={{ boxShadow: "none" }}
        >
          <Toolbar disableGutters>
            <Header
              handleDrawerToggle={handleDrawerToggle}
              isDrawerOpen={open}
            />
          </Toolbar>
        </AppBar>

        <Drawer variant="permanent" open={open}>
          <DrawerHeader sx={{ justifyContent: "center", height: 80 }}>
            <Box>
              <Link to="/">
                {!open && (
                  <div
                    style={{
                      position: "fixed",
                      left: "23px",
                      top: "23px"
                      //   zIndex: "1"
                    }}
                  >
                    <Logo2 />
                  </div>
                )}
                <Logo style={{ opacity: `${!open ? "0" : ""} ` }} />
              </Link>
            </Box>
          </DrawerHeader>

          <Sidebar isDrawerOpen={open} />
        </Drawer>

        <Box
          component="main"
          sx={{
            flexGrow: 1,
            p: 5,
            backgroundColor: DigiHubColor.ghostWhite
          }}
        >
          <DrawerHeader sx={{ mb: 1 }} />
          <Box sx={{ pb: 3 }}>{children}</Box>
        </Box>
      </Box>
    </ErrorBoundary>
  );
};

export default LayoutWrapper;
