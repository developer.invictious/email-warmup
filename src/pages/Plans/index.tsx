import {
  Box,
  Card,
  Grid,
  Link,
  Stack,
  Switch,
  Typography
} from "@mui/material";
import StripeContainer from "@sms/components/Stripe/index";
import { useGetPrice, useGetProduct } from "@sms/services/warmup-plans";
import React, { useState } from "react";
import { ReactComponent as Plans } from "../../assets/svg/plans.svg";

const Mailbox = () => {
  const [data, setData] = useState({ price: "", description: "" });
  const [checked, setChecked] = useState(false);

  const [showItem, setShowItem] = useState(false);

  const { data: productList = [], isFetching } = useGetProduct();
  const { data: priceList = [], isFetching: fetchingPrice } = useGetPrice();
  const monthlyPayments: any = [];
  const yearlyPayments: any = [];

  priceList.forEach((data: any) => {
    if (data.interval === "month") {
      monthlyPayments.push({
        id: data.id,
        product: data.product,
        amount: data.amount
      });
    }
    if (data.interval === "year") {
      yearlyPayments.push({
        id: data.id,
        product: data.product,
        amount: data.amount
      });
    }
  });

  productList.forEach((product: any) => {
    monthlyPayments.forEach((data: any) => {
      if (data.product === product.id) {
        data.name = product.name;
        (data.p1 = "All essential features"),
          (data.p2 = "Some basic integrations"),
          (data.p3 = "Advanced Security"),
          (data.p4 = "All integration"),
          (data.p5 = "VIP Support");
        product.name === "Standard"
          ? (data.description =
              "The essentials to get your team up and running ")
          : product.name === "Enterprise"
          ? (data.description =
              "Powerful tools to transform the way your team works")
          : (data.description =
              "All of our features with advanced security, control and support");
      }
    });
    yearlyPayments.forEach((data: any) => {
      if (data.product === product.id) {
        data.name = product.name;
        (data.p1 = "All essential features"),
          (data.p2 = "Some basic integrations"),
          (data.p3 = "Advanced Security"),
          (data.p4 = "All integration"),
          (data.p5 = "VIP Support");
        product.name === "Standard"
          ? (data.description =
              "The essentials to get your team up and running ")
          : product.name === "Enterprise"
          ? (data.description =
              "Powerful tools to transform the way your team works")
          : (data.description =
              "All of our features with advanced security, control and support");
      }
    });
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  return (
    <Box>
      <Card
        sx={{
          mt: 7,
          mb: 2,
          paddingX: 3,
          borderRadius: "8px",
          boxShadow: "none",
          bgcolor: "#FAF8FC"
        }}
      >
        <Typography fontSize={20} fontWeight={400}>
          Plan Detail
        </Typography>
        <Typography
          color="text.secondary"
          variant="h6"
          fontWeight="bold"
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            paddingTop: "45px"
          }}
        >
          Choose your plan according to your needs
        </Typography>
        <Stack
          direction="row"
          spacing={2}
          marginY="20px"
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Typography color={checked ? "black" : "#4F539E"}>Monthly</Typography>

          <Switch checked={checked} onChange={handleChange} />

          <Typography color={!checked ? "black" : "#4F539E"}>Yearly</Typography>
        </Stack>

        <Grid container spacing={4}>
          {(checked ? yearlyPayments : monthlyPayments).map((list: any) => (
            <Grid item md={4} key={list.name}>
              <Link
                onClick={() => {
                  setShowItem(true);
                  setData({
                    price: list.amount,
                    description: list.name
                  });
                }}
                sx={{
                  textDecoration: "none",
                  cursor: "pointer",

                  color: "white !important",
                  "&:hover": {
                    "& .MuiTypography-body1": {
                      color: "white"
                    }
                  }
                }}
              >
                <Box
                  justifyContent="center"
                  alignItems="center"
                  display="flex"
                  flexDirection="column"
                  sx={{
                    height: 350,
                    backgroundColor: "#fff",
                    "&:hover": {
                      "&svg": {
                        fill: "black"
                      },
                      backgroundColor: "#4F539E",

                      opacity: [0.9, 0.8, 0.7]
                    },
                    borderRadius: 2
                  }}
                >
                  <Plans />
                  <Typography
                    color="#4F539E"
                    fontWeight="bold"
                    fontSize={18}
                    sx={{
                      pb: 1,
                      pt: 3
                    }}
                  >
                    {list.name}
                  </Typography>
                  <Typography
                    color="#4F539E"
                    fontWeight="bold"
                    fontSize={18}
                    sx={{
                      py: 1
                    }}
                  >
                    {list.amount / 100}$&nbsp;&nbsp;
                    <span style={{ fontSize: "10pt" }}>
                      {checked ? "/ Year" : "/ Month"}
                    </span>
                  </Typography>
                  <Typography
                    color="text.secondary"
                    fontSize={14}
                    sx={{
                      py: 1
                    }}
                  >
                    <h4 style={{ textAlign: "center" }}>{list.description}</h4>
                  </Typography>

                  <Typography color="text.secondary" fontSize={14}>
                    <h5>{list.p1}</h5>{" "}
                  </Typography>
                  <Typography color="text.secondary" fontSize={14}>
                    <h5>{list.p2}</h5>{" "}
                  </Typography>
                  <Typography
                    color="text.secondary"
                    fontSize={14}
                    sx={{
                      textDecoration:
                        `${list.name}` === "Standard" ? "line-through" : "none"
                    }}
                  >
                    <h5>{list.p3}</h5>{" "}
                  </Typography>
                  <Typography
                    color="text.secondary"
                    fontSize={14}
                    sx={{
                      textDecoration:
                        `${list.name}` === "Standard" ? "line-through" : "none"
                    }}
                  >
                    <h5>{list.p4}</h5>{" "}
                  </Typography>
                  <Typography
                    color="text.secondary"
                    fontSize={14}
                    sx={{
                      textDecoration:
                        `${list.name}` === "Enterprise" ||
                        `${list.name}` === "Standard"
                          ? "line-through"
                          : "none"
                    }}
                  >
                    <h5>{list.p5}</h5>
                  </Typography>
                </Box>
              </Link>
            </Grid>
          ))}
        </Grid>
        <Box>
          {showItem ? (
            <Box>
              <StripeContainer
                price={data.price}
                description={data.description}
                open={showItem}
                handleClose={() => {
                  setShowItem(false);
                }}
              />
            </Box>
          ) : (
            <></>
          )}
        </Box>
      </Card>
    </Box>
  );
};
export default Mailbox;
