import { useParams } from "react-router-dom";

import { Box, Grid, LinearProgress, Stack, Typography } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import BreadcrumbsComponent from "@sms/components/BreadCrumb";
import { useFetchUserById } from "@sms/services/warmup-user";

function UserDetail() {
  const { userId } = useParams();
  const { data: userDetail, isLoading } = useFetchUserById(userId ?? "");

  return (
    <Box>
      <Typography fontSize={20} fontWeight={400}>
        User Management
      </Typography>

      <BreadcrumbsComponent />

      <Stack direction="row" justifyContent="center" sx={{ m: 5 }}>
        <Card sx={{ width: "60%", borderRadius: 3, boxShadow: "none" }}>
          <CardContent>
            {isLoading && <LinearProgress color="info" />}
            <Typography
              fontWeight="bold"
              sx={{ fontSize: 16, my: 2 }}
              gutterBottom
            >
              Details
            </Typography>

            <Grid container>
              <Grid item sm={6}>
                <Grid container>
                  <Grid item xs={12}>
                    <Stack direction="row">
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        First Name:
                      </Typography>
                      <Typography
                        sx={{ fontSize: 16, pl: 1 }}
                        gutterBottom
                        color="text.secondary"
                      >
                        {userDetail?.firstName}
                      </Typography>
                    </Stack>
                  </Grid>

                  <Grid item xs={12}>
                    <Stack direction="row">
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Last Name:
                      </Typography>
                      <Typography
                        sx={{ fontSize: 16, pl: 1 }}
                        gutterBottom
                        color="text.secondary"
                      >
                        {userDetail?.lastName}
                      </Typography>
                    </Stack>
                  </Grid>

                  <Grid item xs={12}>
                    <Stack direction="row">
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Email:
                      </Typography>
                      <Typography
                        sx={{ fontSize: 16, pl: 1 }}
                        gutterBottom
                        color="text.secondary"
                      >
                        {userDetail?.email}
                      </Typography>
                    </Stack>
                  </Grid>

                  <Grid item xs={12}>
                    <Stack direction="row">
                      <Typography sx={{ fontSize: 16 }} gutterBottom>
                        Role:
                      </Typography>
                      <Typography
                        sx={{ fontSize: 16, pl: 1 }}
                        gutterBottom
                        color="text.secondary"
                      >
                        {userDetail?.roles?.[0].name}
                      </Typography>
                    </Stack>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item sm={6}></Grid>
            </Grid>
          </CardContent>
        </Card>
      </Stack>
    </Box>
  );
}
export default UserDetail;
