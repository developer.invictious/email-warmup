import { useNavigate } from "react-router-dom";
import { Cell } from "react-table";

import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Stack,
  Typography
} from "@mui/material";
import Datatable from "@sms/components/Datatable";
import pageRoutes from "@sms/route/PageRoutes";
import {
  IUser,
  useDeleteUser,
  useFetchAllUsers
} from "@sms/services/warmup-user";

import { useState } from "react";
import { ReactComponent as Delete } from "../../assets/svg/delete.svg";
import { ReactComponent as Edit } from "../../assets/svg/edit.svg";
import { ReactComponent as View } from "../../assets/svg/view.svg";

const style = {
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4
};

const UserList = () => {
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(1);
  const [deleteData, setDeleteData] = useState("");
  const { data: userList = { result: [], page: 1 }, isFetching } =
    useFetchAllUsers({ page, limit } ?? "");
  const { mutateAsync: deleteUser, isLoading } = useDeleteUser();

  const totalPages = JSON.stringify(userList);

  const handleLimit = (limit: number): void => {
    setLimit(limit);
  };

  const handlePage = (page: number): void => {
    setPage(page + 1);
  };

  const handleClickOpen = (data: string) => {
    setOpen(true);
    setDeleteData(data);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const columns = [
    {
      Header: "First Name",
      accessor: "firstName"
    },
    {
      Header: "Last Name",
      accessor: "lastName"
    },
    {
      Header: "Role",
      Cell: (cell: Cell<IUser>) => <>{cell.row.original?.roles?.[0].name}</>
    },
    {
      Header: "Email Address",
      accessor: "email"
    },
    {
      Header: "Date",
      accessor: "createdAt"
    },
    {
      Header: "Actions    ",
      Cell: (cell: Cell<IUser>) => (
        <Stack direction="row" justifyContent="flex-end">
          <IconButton
            aria-label="view"
            onClick={() =>
              navigate(`/user-management/view/${cell.row.original.id}`)
            }
          >
            <View />
          </IconButton>

          <IconButton
            aria-label="edit"
            onClick={() =>
              navigate(`/user-management/edit/${cell.row.original.id}`)
            }
          >
            <Edit />
          </IconButton>
          <IconButton
            aria-label="delete"
            onClick={() => {
              handleClickOpen(cell.row.original?.id?.toString());
            }}
          >
            <Delete />
          </IconButton>
        </Stack>
      )
    }
  ];

  return (
    <Box>
      {/* <BreadcrumbsComponent /> */}

      <Stack
        direction="row"
        justifyContent="space-between"
        mt={7}
        px={3}
        mb={2}
      >
        <Typography fontSize={20} fontWeight={400}>
          User Management
        </Typography>
        <Button
          onClick={() => navigate(pageRoutes.add_user)}
          variant="contained"
          color="primary"
          size="large"
          sx={{ mb: 2 }}
        >
          <Typography fontSize={14} fontWeight={500} textTransform="none">
            Add New User
          </Typography>
        </Button>
      </Stack>

      <Datatable
        showLimit
        searchable
        columns={columns}
        data={userList.result}
        loading={isFetching}
        onPageSizeChange={handleLimit}
        pageSize={JSON.parse(totalPages).pages}
        isBackendPagination={true}
        paginateParams={{
          gotoPage: handlePage,
          pageIndex: page - 1,
          canNextPage: page + 1 <= JSON.parse(totalPages).pages ? true : false,
          canPreviousPage:
            page + 1 > JSON.parse(totalPages).pages ? true : false
        }}
      />

      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Alert!!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you really want to delete this user?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            disableElevation
            variant="contained"
            onClick={async () => {
              await deleteUser(deleteData);
              setOpen(false);
            }}
          >
            Yes
          </Button>
          <Button
            disableElevation
            variant="contained"
            color="error"
            onClick={handleClose}
          >
            No
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

export default UserList;
