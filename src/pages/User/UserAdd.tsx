import { yupResolver } from "@hookform/resolvers/yup";
import InfoIcon from "@mui/icons-material/Info";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { styled } from "@mui/material/styles";
import Tooltip from "@mui/material/Tooltip";
import React from "react";
import { useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";

import {
  useCreateUser,
  useFetchRole,
  useFetchUserById,
  useUpdateUser
} from "@sms/services/warmup-user";

import { Divider } from "@mui/material";

import {
  CustomCard,
  CustomHeaderTitle,
  CustomPrimaryButton,
  CustomSecondaryButton,
  CustomSelect,
  CustomTextField,
  CustomTitle,
  CustomTypography,
  MainTitle
} from "./user.style";

const CustomInfoIcon = styled(InfoIcon)({
  fontSize: "24px",
  marginLeft: "5px",
  color: "#E6E7F1",
  "&:hover": {
    cursor: "pointer"
  }
});

const defaultValues = {
  id: null as string | null,
  firstname: "",
  lastname: "",
  email: "",
  roles: []
};

const UserForm = () => {
  const navigate = useNavigate();
  const { userId } = useParams();

  const schema = Yup.object().shape({
    firstname: Yup.string().required("Firstname is required"),
    lastname: Yup.string().required("Lastname is required"),
    email: Yup.string().required("Email is required"),
    roles: Yup.array().required("User roles is required")
  });

  const { data: useDetail } = useFetchUserById(userId ?? "");

  const { data: roleList = [], isFetching } = useFetchRole();

  const { mutateAsync: addUser, isLoading } = useCreateUser();
  const { mutateAsync: updateUser, isLoading: isUpdatingUser } =
    useUpdateUser();
  const { control, handleSubmit, reset } = useForm({
    defaultValues: defaultValues,
    resolver: yupResolver(schema)
  });

  const onUserAddHandler = async (userDetails: typeof defaultValues) => {
    const requestBody = {
      firstName: userDetails.firstname,
      lastName: userDetails.lastname,
      email: userDetails.email,

      roles: userDetails.roles
    };

    try {
      if (userDetails.id) {
        await updateUser({
          email: userDetails.email,
          firstName: userDetails.firstname,
          lastName: userDetails.lastname,
          id: userDetails.id,
          roles: userDetails.roles
        });
      } else {
        await addUser({
          ...requestBody
        });
      }
      navigate("/user-management");
    } catch (e) {
      console.log(e);
    }
  };

  React.useEffect(() => {
    if (useDetail) {
      reset({
        ...defaultValues,
        id: useDetail.id?.toString(),
        firstname: useDetail.firstName,
        lastname: useDetail.lastName,
        email: useDetail.email,
        roles: useDetail.roles?.map((x: any) => x.id)
      });
    }
  }, [useDetail]);

  return (
    <Box sx={{ paddingTop: "20px", mt: 7 }}>
      <MainTitle sx={{ mb: 2 }}>User Management</MainTitle>

      {/* <BreadcrumbsComponent /> */}
      <Grid container>
        <Grid item xs={12}>
          <CustomCard sx={{ marginTop: 2.5 }}>
            <CustomHeaderTitle>User Form</CustomHeaderTitle>
            <Divider />
            <CustomTitle sx={{ marginTop: 1.5 }}>
              Please kindly fill up the form*
            </CustomTitle>
            <Grid
              container
              sx={{ paddingTop: "33px", display: "flex", alignItems: "center" }}
            >
              <form
                style={{ width: "100%" }}
                onSubmit={handleSubmit(onUserAddHandler)}
              >
                <Grid
                  container
                  columnSpacing={6}
                  // rowSpacing={2}
                  // sx={{ paddingTop: "10px" }}
                  alignItems="center"
                >
                  <Grid item xs={12} md={6}>
                    <CustomTypography
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      First Name{" "}
                      <Tooltip title="First Name" placement="top" arrow>
                        <CustomInfoIcon />
                      </Tooltip>
                    </CustomTypography>
                    <CustomTextField
                      control={control}
                      name="firstname"
                      placeholder="eg. John"
                      type="text"
                    />
                  </Grid>

                  <Grid item xs={12} md={6}>
                    <CustomTypography
                      sx={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      Last Name{" "}
                      <Tooltip title="Last Name" placement="top" arrow>
                        <CustomInfoIcon />
                      </Tooltip>
                    </CustomTypography>
                    <CustomTextField
                      control={control}
                      name="lastname"
                      placeholder="eg. Doe"
                      type="text"
                    />
                  </Grid>

                  <Grid item xs={12} md={6}>
                    <CustomTypography
                      sx={{
                        paddingTop: "15px",
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      Email{" "}
                      <Tooltip title="Email Address" placement="top" arrow>
                        <CustomInfoIcon />
                      </Tooltip>
                    </CustomTypography>
                    <CustomTextField
                      //   sx={{ paddingBottom: "0px" }}
                      control={control}
                      name="email"
                      placeholder="eg. something@gmail.com"
                      type="email"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <CustomTypography
                      sx={{
                        paddingTop: "15px",
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      User Role{" "}
                      <Tooltip title="User Role" placement="top" arrow>
                        <CustomInfoIcon />
                      </Tooltip>
                    </CustomTypography>
                    <CustomSelect
                      multiple={true}
                      name="roles"
                      options={roleList.map((x) => {
                        return {
                          label: x.name,
                          value: x.id
                        };
                      })}
                      control={control}
                    />
                  </Grid>

                  <Grid container spacing={2} sx={{ mt: 2 }}>
                    <Grid item xs={0}></Grid>
                    <Grid
                      item
                      xs={12}
                      sx={{
                        display: "flex",
                        justifyContent: "end"
                      }}
                    >
                      <CustomPrimaryButton
                        type="submit"
                        loading={isLoading}
                        sx={{ marginRight: "16px" }}
                      >
                        {!userId ? "Submit" : "Update"}
                      </CustomPrimaryButton>
                      <CustomSecondaryButton
                        onClick={(_) => reset(defaultValues)}
                      >
                        Discard
                      </CustomSecondaryButton>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            </Grid>
          </CustomCard>
        </Grid>
      </Grid>
    </Box>
  );
};

export default UserForm;
