import { DateTime, Duration, DurationObjectUnits } from "luxon";

/**
 * Get date difference between two dates
 * @param fromDate From Date
 * @param toDate To Date
 */
function getDateDifference(fromDate: Date, toDate: Date) {
  return Math.floor(
    DateTime.fromJSDate(fromDate).diff(DateTime.fromJSDate(toDate), "days").days
  );
}

/**
 * Formats date to locale string
 * @param datatime Date
 * @returns
 */
function getLocaleDate(datatime: Date) {
  return DateTime.fromJSDate(datatime).toLocaleString(
    DateTime.DATETIME_MED_WITH_SECONDS
  );
}
/**
 * Formats date to locale date
 * @param datatime Date
 * @returns
 */
function getLocaleDateOnly(datatime: Date) {
  return DateTime.fromJSDate(datatime).toLocaleString(
    DateTime.DATE_MED_WITH_WEEKDAY
  );
}

/**
 * Get date
 * @param datetime Date
 */
function getDateTime(datetime: Date) {
  return DateTime.fromJSDate(datetime);
}

const getValidDurations = (durations: DurationObjectUnits) => {
  return Object.entries(durations).reduce(
    (acc, [durationKey, durationValue]) => {
      if (durationValue) {
        acc[durationKey as keyof DurationObjectUnits] = durationValue;
      }

      return acc;
    },
    {} as Record<keyof DurationObjectUnits, number>
  );
};

/**
 * Format seconds duration to human readable format
 * @param durationInSeconds Duration in seconds
 * @returns Human readable duration
 */

function toHumanDuration(durationInSeconds: number): string {
  const durationShift = Duration.fromDurationLike({
    seconds: durationInSeconds
  }).shiftTo("days", "hours", "minutes", "seconds", "milliseconds");

  const validDurations = getValidDurations({
    days: durationShift.days,
    hours: durationShift.hours,
    minutes: durationShift.minutes,
    seconds: durationShift.seconds,
    milliseconds: durationShift.milliseconds
  });

  return Duration.fromDurationLike(validDurations).toHuman({
    unitDisplay: "short"
  });
}

export {
  getDateDifference,
  getLocaleDate,
  getLocaleDateOnly,
  getDateTime,
  toHumanDuration
};
