const creditUnitEng = 120;
const creditUnitNep = 60;

const english = /\w$/;

const phonePattern = [
  { pattern: new RegExp(/(980|981|982)\d{7}$/), type: "Ncell" },
  { pattern: new RegExp(/(984|986)\d{7}$/), type: "NTC Prepaid" },
  { pattern: new RegExp(/(985)\d{7}$/), type: "NTC Postpaid" },
  {
    pattern: new RegExp(/[9][6][1|2]{1}[0-9]{7}|[9][8][8][0-9]{7}$/),
    type: "Smartcell"
  },
  { pattern: new RegExp(/(974)\d{7}$/), type: "CDMA Prepaid" },
  { pattern: new RegExp(/(975)\d{7}$/), type: "CDMA PostPaid" }
];
const calculateCredit = (textMessage: string, isNepali = false) => {
  const creditUnit = isNepali ? creditUnitNep : creditUnitEng;
  const charecterCount = textMessage.length;
  const credit = Math.ceil(charecterCount / creditUnit);
  return credit;
};

const networkDeterminer = (number: string) => {
  return (
    phonePattern.find((p) => {
      return p.pattern.test(number);
    })?.type || ""
  );
};

const checkValidNumber = (number: string) => {
  return phonePattern.some((pattern) => pattern.pattern.test(number));
};

const languageDeterminer = (message: string) => {
  return english.test(message) ? "English" : "Nepali";
};

export {
  languageDeterminer,
  networkDeterminer,
  calculateCredit,
  checkValidNumber
};
