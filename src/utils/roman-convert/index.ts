import * as core from "./core";

export function convertRomanNepali(raw: string, smartConvert = true) {
  const charactersUnicode = core.translate(raw, smartConvert).split("#");
  let convertedCharacters = "";
  charactersUnicode.forEach(
    (element) =>
      (convertedCharacters += String.fromCharCode(
        Number(element.replace("¬", ""))
      ))
  );
  return convertedCharacters;
}
