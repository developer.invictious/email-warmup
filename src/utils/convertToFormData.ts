export interface IConvertFormValueProps {
  [key: string]: any;
}

const convertToFormData = (values: IConvertFormValueProps) => {
  const formdata = new FormData();
  for (const [key, value] of Object.entries(values)) {
    value && formdata.append(key, value);
  }
  return formdata;
};

export { convertToFormData };
