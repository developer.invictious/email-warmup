import { createTheme } from "@mui/material/styles";
import type {} from "@mui/x-date-pickers/themeAugmentation";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#4F539E"
    },
    secondary: {
      main: "#edf2ff"
    },
    error: {
      main: "#d32f2f"
    },
    // warning: {
    //   main: "#ed6c02"
    // },
    // success: {
    //   main: "#ed6c02",
    //   dark: "#004A89"
    // },
    text: {
      primary: "#000000"
    },
    background: {
      paper: "#fff",
      default: "#fff"
    },
    grey: {
      50: "#9CA3AF",
      100: "#828894"
    }
  },
  typography: {
    fontFamily: `"Montserrat", "Arial", sans-serif`,
    button: {
      textTransform: "capitalize"
    }
  },
  components: {
    MuiDatePicker: {
      styleOverrides: {
        root: {
          borderColor: "#9CA3AF"
        }
      }
    }
  }
});
export type AppTheme = typeof theme;
