import { css, Global } from "@emotion/react";
import Montserrat from "@sms/assets/fonts/Montserrat-Regular.ttf";

export const GlobalStyles = (
  <Global
    styles={() => css`
      @font-face {
        font-family: "Montserrat";
        font-style: normal;
        font-weight: regular;
        src: url(${Montserrat}) format("truetype");
      }
      html {
        height: 100vh;
      }
      html,
      body {
        margin: 0;
        padding: 0;
        font-family: "IBM Plex Sans";
        scroll-behavior: smooth;
        background: #f6f9ff;
      }
      body {
        -moz-osx-font-smoothing: grayscale;
        -webkit-text-size-adjust: 100%;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        padding-top: 0px;
        margin: 0px;
        height: 80%;
        font-family: "IBM Plex Sans";
      }
      * {
        box-sizing: border-box;
        &:before,
        &:after {
          box-sizing: border-box;
        }
      }
      img {
        max-width: 100%;
      }
      li {
        list-style: none;
      }
      h1,
      h2,
      h3,
      h4,
      h5,
      ul,
      li,
      h6,
      p,
      img,
      figure {
        margin: 0px;
        padding: 0px;
      }
      a {
        text-decoration: none;
      }
      input:-webkit-autofill,
      input:-webkit-autofill:hover,
      input:-webkit-autofill:focus,
      input:-webkit-autofill:active {
        box-shadow: 0 0 0 30px white inset !important;
        -webkit-box-shadow: 0 0 0 30px white inset !important;
      }
      #digihub-app {
        height: inherit;
      }
    `}
  />
);
